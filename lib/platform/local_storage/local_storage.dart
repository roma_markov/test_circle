import 'package:rxdart/rxdart.dart';

abstract class LocalStorage {
    // ignore: close_sinks
    final storageLoaded = BehaviorSubject<bool>();

    setToken(String token);

    String token();

    setAuthId(int id);

    int authId();

    setAuthEmail(String email);

    String authEmail();

    setAuthUsername(String username);

    String authUsername();

    setFirstName(String username);

    String firstName();

    setLastName(String username);

    String lastName();

    clear();
}