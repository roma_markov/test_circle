import 'package:shared_preferences/shared_preferences.dart';

import 'local_storage.dart';

class SharedStorage extends LocalStorage {
  SharedPreferences _prefs;

  SharedStorage() {
    SharedPreferences.getInstance().then((prefs) {
      _prefs = prefs;
      storageLoaded.add(true);
    });
  }

  @override
  setToken(String token) => _prefs.setString('token', token);

  @override
  String token() => _prefs == null ? null : _prefs.get('token');

  @override
  setAuthId(int id) => _prefs.setInt('auth_id', id);

  @override
  int authId() => _prefs == null ? null : _prefs.get('auth_id');

  @override
  setAuthEmail(String email) => _prefs.setString('auth_email', email);

  @override
  String authEmail() => _prefs.get('auth_email');

  @override
  setAuthUsername(String username) => _prefs.setString('auth_username', username);

  @override
  String authUsername() => _prefs.get('auth_username');

  @override
  setFirstName(String username) => _prefs.setString('first_name', username);

  @override
  String firstName() => _prefs.get('first_name');

  @override
  setLastName(String username) => _prefs.setString('last_name', username);

  @override
  String lastName() => _prefs.get('last_name');

  @override
  clear() {
    _prefs.clear();
  }
}
