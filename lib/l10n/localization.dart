import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'messages_all.dart';

/*
*  Localization generating steps:
*  1. flutter pub run intl_translation:extract_to_arb --output-dir=lib/l10n lib/l10n/localization.dart
*  2. flutter pub run intl_translation:generate_from_arb --output-dir=lib/l10n --no-use-deferred-loading lib/l10n/localization.dart lib/l10n/intl_*.arb
* */

class AppLocalizations {
  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static Future<AppLocalizations> load(Locale locale) {
    final String name = locale.countryCode == null ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      return new AppLocalizations();
    });
  }

  //PLURALS

  String hours(int count) => Intl.message('$count ${Intl.plural(count, one: hour, other: hourOther)}');

  String weeks(int count) => Intl.message('$count ${Intl.plural(count, one: week, other: weeksOther)}');

  String players(int count) => Intl.message('$count ${Intl.plural(count, one: player, other: playersOther)}');

  String playersCount(int count) => Intl.message('${Intl.plural(count, one: player, other: playersOther)} $count');

  String students(int count) => Intl.message('$count ${Intl.plural(count, one: student, other: studentsOther)}');

  String lessons(int count) => Intl.message('$count ${Intl.plural(count, one: lesson, other: lessonsOther)}');

  //COMMON
  String get goal => Intl.message('Goal', name: 'goal');

  String get note => Intl.message('Note', name: 'note');

  String get equipment => Intl.message('Equipment', name: 'equipment');

  String get tags => Intl.message('Tags', name: 'tags');

  String get duration => Intl.message('Duration', name: 'duration');

  String get saveChanges => Intl.message('SAVE CHANGES', name: 'saveChanges');

  String get edit => Intl.message('Edit', name: 'edit');

  String get capacity => Intl.message('Capacity', name: 'capacity');

  String get location => Intl.message('Location', name: 'location');

  String get lesson => Intl.message('Lesson', name: 'lesson');

  String get lessonsOther => Intl.message('Lessons', name: 'lessonsOther');

  String get student => Intl.message('Student', name: 'student');

  String get studentsOther => Intl.message('Students', name: 'studentsOther');

  String get hour => Intl.message('Hour', name: 'hour');

  String get hourOther => Intl.message('Hours', name: 'hourOther');

  String get week => Intl.message('Week', name: 'week');

  String get weeksOther => Intl.message('Weeks', name: 'weeksOther');

  String get player => Intl.message('Player', name: 'player');

  String get playersOther => Intl.message('Players', name: 'playersOther');

  //LESSON

  String get lessonSearchHint => Intl.message('Search for a lesson', name: 'lessonSearchHint');

  String get lessonEditTitle => Intl.message('EDIT LESSON INFO', name: 'lessonEditTitle');

  String get lessonCreateTitle => Intl.message('CREATE NEW LESSON', name: 'lessonCreateTitle');

  String get lessonNameHint => Intl.message('Lesson name', name: 'lessonNameHint');

  String get lessonDurationHint => Intl.message('Duration in hours', name: 'lessonDurationHint');

  String get lessonCreate => Intl.message('CREATE LESSON', name: 'lessonCreate');

  String get lessonInfo => Intl.message('LESSON INFO', name: 'lessonInfo');

  String get lessonSchedulePrivate => Intl.message('SCHEDULE PRIVATE LESSON', name: 'lessonSchedulePrivate');

  String get lessonScheduleThis => Intl.message('SCHEDULE THIS LESSON', name: 'lessonScheduleThis');

  String get lessonAddPrivate => Intl.message('ADD THIS LESSON', name: 'lessonAddPrivate');

  String get lessonEdit => Intl.message('EDIT LESSON', name: 'lessonEdit');

  String get lessonDelete => Intl.message('DELETE LESSON', name: 'lessonDelete');

  String get lessonSimilar => Intl.message('CREATE NEW SIMILAR', name: 'lessonSimilar');

  String get lessonPrivate => Intl.message('Private lesson', name: 'lessonPrivate');

  //STUDENT
  String get studentSearchHint => Intl.message('Search for player', name: 'studentSearchHint');
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return ['en'].contains(locale.languageCode);
  }

  @override
  Future<AppLocalizations> load(Locale locale) {
    return AppLocalizations.load(locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalizations> old) => false;
}
