// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a messages locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// ignore_for_file: unnecessary_brace_in_string_interps

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'messages';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "capacity" : MessageLookupByLibrary.simpleMessage("Capacity"),
    "duration" : MessageLookupByLibrary.simpleMessage("Duration"),
    "edit" : MessageLookupByLibrary.simpleMessage("Edit"),
    "equipment" : MessageLookupByLibrary.simpleMessage("Equipment"),
    "goal" : MessageLookupByLibrary.simpleMessage("Goal"),
    "hour" : MessageLookupByLibrary.simpleMessage("Hour"),
    "hourOther" : MessageLookupByLibrary.simpleMessage("Hours"),
    "lesson" : MessageLookupByLibrary.simpleMessage("Lesson"),
    "lessonAddPrivate" : MessageLookupByLibrary.simpleMessage("ADD THIS LESSON"),
    "lessonCreate" : MessageLookupByLibrary.simpleMessage("CREATE LESSON"),
    "lessonCreateTitle" : MessageLookupByLibrary.simpleMessage("CREATE NEW LESSON"),
    "lessonDelete" : MessageLookupByLibrary.simpleMessage("DELETE LESSON"),
    "lessonDurationHint" : MessageLookupByLibrary.simpleMessage("Duration in hours"),
    "lessonEdit" : MessageLookupByLibrary.simpleMessage("EDIT LESSON"),
    "lessonEditTitle" : MessageLookupByLibrary.simpleMessage("EDIT LESSON INFO"),
    "lessonInfo" : MessageLookupByLibrary.simpleMessage("LESSON INFO"),
    "lessonNameHint" : MessageLookupByLibrary.simpleMessage("Lesson name"),
    "lessonPrivate" : MessageLookupByLibrary.simpleMessage("Private lesson"),
    "lessonSchedulePrivate" : MessageLookupByLibrary.simpleMessage("SCHEDULE PRIVATE LESSON"),
    "lessonScheduleThis" : MessageLookupByLibrary.simpleMessage("SCHEDULE THIS LESSON"),
    "lessonSearchHint" : MessageLookupByLibrary.simpleMessage("Search for a lesson"),
    "lessonSimilar" : MessageLookupByLibrary.simpleMessage("CREATE NEW SIMILAR"),
    "lessonsOther" : MessageLookupByLibrary.simpleMessage("Lessons"),
    "note" : MessageLookupByLibrary.simpleMessage("Note"),
    "player" : MessageLookupByLibrary.simpleMessage("Player"),
    "playersOther" : MessageLookupByLibrary.simpleMessage("Players"),
    "saveChanges" : MessageLookupByLibrary.simpleMessage("SAVE CHANGES"),
    "student" : MessageLookupByLibrary.simpleMessage("Student"),
    "studentsOther" : MessageLookupByLibrary.simpleMessage("Students"),
    "week" : MessageLookupByLibrary.simpleMessage("Week"),
    "weeksOther" : MessageLookupByLibrary.simpleMessage("Weeks")
  };
}
