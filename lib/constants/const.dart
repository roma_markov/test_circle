final year = 365;
final dayInWeek = 7;
final weeks = List.generate(4, (index) => (index + 1) * dayInWeek)..add(year);
