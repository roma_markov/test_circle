//import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:lesson_scheduling_pga/constants/index.dart' as Const;
import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/platform/index.dart';
import 'package:lesson_scheduling_pga/routes.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:overlay_support/overlay_support.dart';

import 'l10n/localization.dart';

class App extends StatelessWidget {
  final BLoC _bloc;
  final _navigatorObserver = NavigatorObserver();

  App([BLoC bloc]) : this._bloc = bloc;

  @override
  Widget build(BuildContext context) {
    _handleTokenExpired();
    return BlocProvider<BLoC>(
      child: PortraitMode(
        app: OverlaySupport(
          child: MaterialApp(
            theme: ThemeData(
              scaffoldBackgroundColor: appBackgroundColor,
              canvasColor: appBackgroundColor,
              primaryColor: appNavigationColor,
              backgroundColor: appBackgroundColor,
            ),
//            localizationsDelegates: [GlobalMaterialLocalizations.delegate, const AppLocalizationsDelegate()],
            supportedLocales: [const Locale('en')],
            navigatorObservers: [_navigatorObserver],
            title: Const.Title,
            onGenerateRoute: buildRoute,
          ),
        ),
      ),
      builder: (_, bloc) => bloc ?? this._bloc,
      onDispose: (_, bloc) => bloc.dispose(),
    );
  }

  _handleTokenExpired() async {
    tokenExpiredSubject.throttleTime(Duration(seconds: 10)).where((value) => value).listen(
          (_) => _bloc.logout(() {
            _navigatorObserver.navigator.pushReplacementNamed(LoginScreen.route);
          }),
        );
  }
}
