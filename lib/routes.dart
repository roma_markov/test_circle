import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:lesson_scheduling_pga/ui/screen/add_event/schedule_lesson_list/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/add_event/schedule_private_lesson/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/add_event/schedule_program_list/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/add_event/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/home/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/private_details/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/scheduled_details/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/login/create_user.dart';
import 'package:lesson_scheduling_pga/ui/screen/place/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/edit/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/edit_program_info/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/program_occurs/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/quit_scheduling/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/schedule_lesson/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_1/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_2/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_3/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_4/add_player/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_4/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_5/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/details/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/schedule/choose_date/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/schedule/choose_time/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/student/create/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/student/details/screen.dart';

import 'ui/screen/lesson/create/screen.dart';
import 'ui/screen/lesson/details/screen.dart';
import 'ui/screen/program/create/step_3/add_lessons/screen.dart';
import 'ui/shared/transition/slide_up_transition.dart';

Route buildRoute(RouteSettings settings) {
  final Map<String, dynamic> arguments = settings.arguments;
  switch (settings.name) {
    case SplashScreen.route:
      return FadeRoute(settings, SplashScreen());

    //Auth
    case LoginScreen.route:
      return FadeRoute(settings, LoginScreen());
    case CreateUserScreen.route:
      return SlideUpRoute(settings, CreateUserScreen.fromArgs());

    //Home
    case HomeScreen.route:
      return SlideUpRoute(settings, HomeScreen());

    //Lessons
    case LessonDetailsScreen.route:
      return SlideUpRoute(settings, LessonDetailsScreen.fromArgs(arguments));
    case LessonCreateScreen.route:
      return SlideUpRoute(settings, LessonCreateScreen.fromArgs(arguments));

    //Students
    case StudentDetailsScreen.route:
      return SlideUpRoute(settings, StudentDetailsScreen.fromArgs(arguments));
    case StudentCreateScreen.route:
      return SlideUpRoute(settings, StudentCreateScreen.fromArgs(arguments));

    //Programs
    case ProgramDetailsScreen.route:
      return SlideUpRoute(settings, ProgramDetailsScreen.fromArgs(arguments));
    case EditProgramInfoScreen.route:
      return SlideUpRoute(settings, EditProgramInfoScreen.fromArgs(arguments));
    case ProgramEditSummaryScreen.route:
      return SlideUpRoute(settings, ProgramEditSummaryScreen.fromArgs(arguments));

    //Create program
    case ProgramCreateInfoScreen.route:
      return SlideUpRoute(settings, ProgramCreateInfoScreen.fromArgs(arguments));
    case ProgramCreateScheduleStartScreen.route:
      return FadeRoute(settings, ProgramCreateScheduleStartScreen.fromArgs(arguments));
    case ChooseProgramDatesScreen.route:
      return SlideUpRoute(settings, ChooseProgramDatesScreen.fromArgs(arguments));
    case ProgramCreateAddLessonsScreen.route:
      return FadeRoute(settings, ProgramCreateAddLessonsScreen.fromArgs(arguments));
    case ProgramCreateSummaryScreen.route:
      return FadeRoute(settings, ProgramCreateSummaryScreen.fromArgs(arguments));
    case ProgramCreateAddPlayersScreen.route:
      return FadeRoute(settings, ProgramCreateAddPlayersScreen.fromArgs(arguments));
    case ScheduleProgramLessonScreen.route:
      return SlideUpRoute(settings, ScheduleProgramLessonScreen.fromArgs(arguments));
    case QuitSchedulingScreen.route:
      return MaterialPageRoute(settings: settings, builder: (_) => QuitSchedulingScreen.fromArgs(arguments));

    case SelectLessonsScreen.route:
      return SlideUpRoute(settings, SelectLessonsScreen.fromArgs(arguments));
    case SelectPlayerScreen.route:
      return SlideUpRoute(settings, SelectPlayerScreen.fromArgs(arguments));

    case AddEventScreen.route:
      return SlideUpRoute(settings, AddEventScreen.fromArgs(arguments));
    case ScheduleLessonListScreen.route:
      return SlideUpRoute(settings, ScheduleLessonListScreen.fromArgs(arguments));
    case ScheduleProgramListScreen.route:
      return SlideUpRoute(settings, ScheduleProgramListScreen.fromArgs(arguments));
    case SchedulePrivateLessonScreen.route:
      return SlideUpRoute(settings, SchedulePrivateLessonScreen.fromArgs(arguments));
    case ChooseDateScreen.route:
      return SlideUpRoute(settings, ChooseDateScreen.fromArgs(arguments));
    case ScheduledLessonDetails.route:
      return SlideUpRoute(settings, ScheduledLessonDetails.fromArgs(arguments));
    case ScheduledPrivateLessonDetails.route:
      return SlideUpRoute(settings, ScheduledPrivateLessonDetails.fromArgs(arguments));
    case ChooseTimeScreen.route:
      return SlideUpRoute(settings, ChooseTimeScreen.fromArgs(arguments));
    case PlacesScreen.route:
      return SlideUpRoute(settings, PlacesScreen.fromArgs());
    default:
      throw "Undefined route: ${settings.name}";
  }
}
