import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_crashlytics/flutter_crashlytics.dart';

import 'app.dart';
import 'logic/index.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  _fabricWrapper(App(BLoC.dev()));
}

_fabricWrapper(StatelessWidget app) async {
  bool isInDebugMode = true;

  FlutterError.onError = (FlutterErrorDetails details) {
    if (isInDebugMode) {
      FlutterError.dumpErrorToConsole(details);
    } else {
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };
  await FlutterCrashlytics().initialize();
  runZoned<Future<Null>>(() async {
    runApp(app);
  }, onError: (error, stackTrace) async {
    print(error);
    //await FlutterCrashlytics().reportCrash(error, stackTrace, forceCrash: false);
  });
}
