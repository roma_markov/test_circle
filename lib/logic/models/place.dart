class Place {
  final int id;
  final String name;

  Place(this.id, this.name);

  Place.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['message'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': name,
      };

  @override
  int get hashCode => id.hashCode;

  @override
  bool operator ==(other) => other is Place && other.hashCode == hashCode;

  static List<Place> fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return [];
    return List<Place>.from(jsonList.map((json) => Place.fromJson(json)));
  }

  static List<dynamic> toJsonList(List<Place> list) {
    if (list == null) return [];
    return List.from(list.map((item) => item.toJson()));
  }
}
