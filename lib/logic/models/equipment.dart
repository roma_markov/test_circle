class Equipment {
  final int id;
  final String name;

  Equipment(this.id, this.name);

  Equipment.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['title'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': name,
      };

  @override
  int get hashCode => id.hashCode;

  @override
  bool operator ==(other) => other is Equipment && other.hashCode == hashCode;

  static List<Equipment> fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return [];
    return List<Equipment>.from(jsonList.map((json) => Equipment.fromJson(json)));
  }

  static List<dynamic> toJsonList(List<Equipment> list) {
    if (list == null) return [];
    return List.from(list.map((item) => item.toJson()));
  }
}
