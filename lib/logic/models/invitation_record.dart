class InvitationRecord {
  final String status;
  final int playerId;

  InvitationRecord(this.status, this.playerId);

  factory InvitationRecord.fromPlayerId(int playerId) {
    return InvitationRecord('none', playerId);
  }

  bool get isConfirmed => status == 'confirmed';

  InvitationRecord.fromJson(Map<String, dynamic> json)
      : playerId = json['playerId'],
        status = json['invitationRecord'] == null ? '' : json['invitationRecord']['status'];
}
