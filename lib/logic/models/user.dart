class User {
  int id;
  final String email;
  final String password;
  final String role;
  final String firstName;
  final String lastName;

  User(this.firstName, this.lastName, this.id, this.email, this.role, this.password);

  User.fromJson(Map<String, dynamic> json)
      : firstName = json['firstName'],
        lastName = json['lastName'],
        email = json['email'],
        password = '',
        role = json['role'] {
    if (role == 'coach') {
      id = json['coach']['id'];
    }
  }

  Map<String, dynamic> toJson() => {
        'firstName': firstName,
        'lastName': lastName,
        'email': email,
        'username': email,
        'password': password,
        'role': role,
        'middleName': '',
        'address': '',
        'tel': '',
        'age': 20,
        'skillLevel': '',
        'personalNote': '',
        'gender': 'male',
        'message': '',
      };
}
