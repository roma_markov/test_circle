import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/logic/models/place.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';

import 'invitation_record.dart';
import 'lesson.dart';

class Program {
  int id = -1;
  bool isPublish = false;
  String name;
  String goal;
  int durationDays;
  int capacity = 0;
  int placeId = 0;
  List<InvitationRecord> invitationRecords;
  Map<int, List<ScheduleTask>> lessonsScheduleMap = Map();
  List<ProgramEvent> events;

  Program(
    this.name,
    this.goal,
    this.durationDays,
    this.capacity,
    this.players,
    this.place,
  ) {
    lessonsSchedule = Map();
  }

  Map<Lesson, List<Schedule>> lessonsSchedule;
  List<Student> players;
  List<DateTime> startDates;
  Place place;

  Program.fromJson(Map<String, dynamic> json)
      : id = json['id'] ?? -1,
        name = json['nickName'],
        goal = json['goal'],
        durationDays = json['duration'] ~/ (24 * 60 * 60),
        capacity = json['capacity'],
        isPublish = json['isPublish'],
        events = (json['events'] != null)
            ? List<ProgramEvent>.from(json['events'].map((item) => ProgramEvent.fromJson(item)))
            : <ProgramEvent>[],
        invitationRecords = (json['playerList'] != null)
            ? List<InvitationRecord>.from(json['playerList'].map((item) => InvitationRecord.fromJson(item)))
            : <InvitationRecord>[] {
    startDates = events.map((event) => event.startDate).toList();
    placeId = json['location'] != null ? json['location']['id'] : null;
    lessonsScheduleMap = (json['lessonList'] != null)
        ? Map<int, List<ScheduleTask>>.fromIterable(
            json['lessonList'],
            key: (item) => item['lessonId'],
            value: (item) => List<ScheduleTask>.from(item['scheduleTasks'].map((task) => ScheduleTask.fromJson(task))),
          )
        : Map();
  }

  Map<String, dynamic> toJson() => {
        'nickName': name,
        'goal': goal,
        'isPublish': isPublish,
        'duration': durationDays * 24 * 60 * 60,
        'capacity': capacity,
        'location_id': place?.id,
        'players': List<int>.from(invitationRecords.map((record) => record.playerId)),
        'event_times': startDates == null
            ? <int>[]
            : List<int>.from(startDates.map((item) => item.millisecondsSinceEpoch ~/ 1000)),
        'lessons': lessonsSchedule.keys.map((lesson) {
          return {
            'lesson_id': lesson.id,
            'scheduleTaskTimes': List<int>.from(
              lessonsSchedule[lesson].map((item) => item.start.millisecondsSinceEpoch ~/ 1000),
            ),
          };
        }).toList(),
      };

  factory Program.from(
    Program program, [
    String name,
    String goal,
    int durationDays,
    int capacity,
    Place place,
  ]) {
    final newProgram = Program(
      name ?? program.name,
      goal ?? program.goal,
      durationDays ?? program.durationDays,
      capacity ?? program.capacity,
      program.players,
      place,
    );
    newProgram.lessonsSchedule = Map.fromIterable(
      program.lessonsSchedule.keys,
      key: (lesson) => lesson,
      value: (lesson) => [],
    );
    newProgram.startDates?.clear();
    return newProgram;
  }

  @override
  int get hashCode => id.hashCode;

  @override
  bool operator ==(other) => other is Program && other.hashCode == hashCode;
}

class ScheduleTask {
  final int lessonId;
  final int eventId;
  final DateTime startDate;

  ScheduleTask(this.lessonId, this.eventId, this.startDate);

  ScheduleTask.fromJson(Map<String, dynamic> json)
      : lessonId = json['lesson']['id'],
        eventId = json['event']['id'],
        startDate = DateTime.fromMillisecondsSinceEpoch(json['startDate'] * 1000);
}

class Schedule {
  Program program;
  final Lesson lesson;
  final DateTime date;
  final TimeOfDay time;

  DateTime get start => DateTime(date.year, date.month, date.day, time.hour, time.minute);

  DateTime get end => start.add(Duration(hours: lesson.durationHours));

  Schedule(this.date, this.time, this.lesson);
}

class ProgramEvent {
  DateTime startDate;
  int id;

  ProgramEvent();

  factory ProgramEvent.fromDate(DateTime date) {
    return ProgramEvent()..startDate = date;
  }

  ProgramEvent.fromJson(Map<String, dynamic> json)
      : id = json['id'] ?? -1,
        startDate = DateTime.fromMillisecondsSinceEpoch(json['startDate'] * 1000);
}
