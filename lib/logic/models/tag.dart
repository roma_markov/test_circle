class Tag {
  final int id;
  final String name;

  Tag(this.id, this.name);

  Tag.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['title'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': name,
      };

  @override
  int get hashCode => id.hashCode;

  @override
  bool operator ==(other) => other is Tag && other.hashCode == hashCode;

  static List<Tag> fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return [];
    return List<Tag>.from(jsonList.map((json) => Tag.fromJson(json)));
  }

  static List<dynamic> toJsonList(List<Tag> list) {
    if (list == null) return [];
    return List.from(list.map((item) => item.toJson()));
  }
}
