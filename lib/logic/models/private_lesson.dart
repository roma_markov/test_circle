import 'package:lesson_scheduling_pga/logic/models/place.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';

import 'invitation_record.dart';
import 'lesson.dart';

class PrivateLesson {
  int id = -1;
  DateTime start;
  bool fromProgram = false;
  Lesson lesson;
  List<InvitationRecord> invitationRecords;
  List<Student> players = [];
  ScheduleTask scheduleTask;
  ProgramEvent event;
  int placeId;

  PrivateLesson();

  Place place;

  PrivateLesson.fromJson(Map<String, dynamic> json)
      : id = json['id'] ?? -1,
        event = (json['event'] != null) ? ProgramEvent.fromJson(json['event']) : null,
        invitationRecords = (json['playerList'] != null)
            ? List<InvitationRecord>.from(json['playerList'].map((item) => InvitationRecord.fromJson(item)))
            : <InvitationRecord>[] {
    start = event.startDate;
    placeId = json['location'] != null ? json['location']['id'] : null;
    scheduleTask = json['lessonList'] != null
        ? json['lessonList'][0]['scheduleTasks'].map((task) => ScheduleTask.fromJson(task))?.first
        : Map();
  }

  Map<String, dynamic> toJson() => {
        'isPublish': true,
        'players': List<int>.from(invitationRecords.map((record) => record.playerId)),
        'event_time': [start.millisecondsSinceEpoch ~/ 1000],
        'location_id': place?.id,
        'lesson': [
          {
            'lesson_id': lesson.id,
            'scheduleTaskTimes': [start.millisecondsSinceEpoch ~/ 1000],
          }
        ],
      };

  DateTime get end => start.add(Duration(hours: lesson.durationHours));

  factory PrivateLesson.withStartDate(DateTime start) {
    return PrivateLesson()..start = start;
  }

  @override
  int get hashCode => id.hashCode;

  @override
  bool operator ==(other) => other is PrivateLesson && other.hashCode == hashCode;
}
