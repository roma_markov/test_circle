class Student {
  int id = -1;
  final String firstName;
  final String lastName;
  final String email;
  final int age;
  final String level;
  final int gender;
  final String notes;

  Student(
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.age,
    this.level,
    this.gender,
    this.notes,
  );

  factory Student.fromJson(Map<String, dynamic> json) {
    final userJson = json['user'];
    if (userJson != null) {
      return Student(
        json['id'],
        userJson['firstName'],
        userJson['lastName'],
        userJson['email'],
        userJson['age'] ?? 28,
        userJson['skillLevel'] ?? '',
        userJson['gender'] == 'male' ? 0 : 1,
        userJson['personalNote'] ?? '',
      );
    }
    return null;
  }

  Map<String, dynamic> toJson() => {
        'firstName': firstName,
        'lastName': lastName,
        'email': email,
        'age': age ?? 20,
        'skillLevel': level ?? '',
        'personalNote': notes ?? '',
        'gender': gender == 0 ? 'male' : 'female',
        'role': 'player',
        'middleName': '',
        'address': '',
        'tel': '',
        'message': '',
      };
}
