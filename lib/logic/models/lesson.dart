import 'package:lesson_scheduling_pga/logic/models/tag.dart';

import 'equipment.dart';

class Lesson {
  int id = -1;
  final String name;
  final String goal;
  final int durationHours;
  final String note;
  final List<int> equipmentIds;
  final List<int> tagIds;

  Lesson(this.name, this.goal, this.durationHours, this.note, this.equipmentIds, this.tagIds);

  List<Equipment> equipment;
  List<Tag> tags;

  Lesson.fromJson(Map<String, dynamic> json)
      : id = json['id'] ?? -1,
        name = json['nickName'],
        goal = json['goal'],
        durationHours = json['duration'] ~/ (60 * 60),
        note = json['note'],
        equipmentIds = List<int>.from(json['equipments'] ?? []),
        tagIds = List<int>.from(json['tags'] ?? []);

  Map<String, dynamic> toJson() => {
        'nickName': name,
        'goal': goal,
        'duration': durationHours * 60 * 60,
        'note': note,
        'equipments': equipmentIds,
        'tags': tagIds,
        //TODO Remove
        'startDate': 0,
        'endDate': 0,
        'id': id,
      };
}
