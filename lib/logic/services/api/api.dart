import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:lesson_scheduling_pga/logic/bloc/result.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/private_lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';
import 'package:lesson_scheduling_pga/logic/models/user.dart';

abstract class Api {
  //Auth

  Future<ResponseStatus> auth(String email, String password);

  Future<ResponseStatus> profile();

  Future<ResponseStatus> createUser(User user);

  logout();

  //Lessons

  Future<ResponseStatus> lessons();

  Future<ResponseStatus> createLesson(Lesson lesson);

  Future<ResponseStatus> editLesson(int id, Lesson lesson);

  Future<ResponseStatus> deleteLesson(int id);

  Future<ResponseStatus> equipments();

  Future<ResponseStatus> tags();

  //Students

  Future<ResponseStatus> students();

  Future<ResponseStatus> createStudent(Student student);

  Future<ResponseStatus> editStudent(int id, Student student);

  //Programs

  Future<ResponseStatus> programs();

  Future<ResponseStatus> createProgram(Program program);

  Future<ResponseStatus> editProgram(int id, Program program);

  Future<ResponseStatus> deleteProgram(int id);

  //Private lessons

  Future<ResponseStatus> privateLessons();

  Future<ResponseStatus> createPrivateLessons(PrivateLesson privateLesson);

  Future<ResponseStatus> deletePrivateLessons(int id);

  //Places

  Future<ResponseStatus> places();

  Future<ResponseStatus> createPlace(String text);

  Future<ResponseStatus> deletePlace(int id);
}

parseAndDecode(String response) {
  return jsonDecode(response);
}

parseJson(String text) {
  return compute(parseAndDecode, text);
}
