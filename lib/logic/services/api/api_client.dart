import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:lesson_scheduling_pga/logic/bloc/result.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/private_lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';
import 'package:lesson_scheduling_pga/logic/models/user.dart';
import 'package:lesson_scheduling_pga/logic/services/api/api.dart';
import 'package:lesson_scheduling_pga/platform/index.dart';
import 'package:lesson_scheduling_pga/platform/local_storage/local_storage.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:rxdart/rxdart.dart';

String authPath() => '/schedule/auth/token';

String usersPath({int id}) => '/schedule/users${id == null ? '' : '/$id'}';

String profilePath() => '/schedule/profile';

String lessonsPath({int id}) => '/schedule/lesson${id == null ? '' : '/$id'}';

String locationPath({int id}) => '/schedule/location${id == null ? '' : '/$id'}';

String equipmentsPath({int id}) => '/schedule/equipment${id == null ? '' : '/$id'}';

String tagsPath({int id}) => '/schedule/tag${id == null ? '' : '/$id'}';

String playersPath({int id}) => '/schedule/players${id == null ? '' : '/$id'}';

String programPath({int id}) => '/schedule/program${id == null ? '' : '/$id'}';

String privateLessonPath({int id}) => '/schedule/private_lesson${id == null ? '' : '/$id'}';

const List<int> statusCodesSuccess = [200, 201];

class ApiClient extends Api {
  static const baseUrlDev = 'http://192.168.9.5:8888/';
  static const baseUrlProd = 'http://192.168.9.5:8888/';

  PublishSubject tokenExpiredSubject;
  PublishSubject generalErrorsSubject = PublishSubject();
  final LocalStorage storage;
  final String baseUrl;
  final Dio _dio;

  ApiClient({@required this.baseUrl, @required this.storage})
      : this._dio = Dio(BaseOptions(baseUrl: '$baseUrl', connectTimeout: 10000)) {
    (_dio.transformer as DefaultTransformer).jsonDecodeCallback = parseJson;
    _dio.interceptors.add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      final token = storage.token();
      if (token != null) {
        options..headers['Authorization'] = 'Bearer $token';
      }
      return options;
    }));
    _dio.interceptors.add(LogInterceptor(requestBody: true, responseBody: true));
    generalErrorsSubject.throttleTime(const Duration(seconds: 5)).listen((data) {
      if (data != null && data is String) {
        _showGeneralError(data);
      }
    });
  }

  @override
  logout() {
    storage.clear();
  }

  @override
  Future<ResponseStatus> auth(String email, String password) async {
    return await _post(
      authPath(),
      params: {
        'grant_type': 'password',
        'username': email,
        'password': password,
        'scope': 'coach',
      },
      options: Options(
        headers: {
          'Authorization': 'Basic Y29tLnBnYV9sZXNzb25zLmNvYWNoSUQ6MTIz',
          'charset': 'utf-8',
        },
        contentType: 'application/x-www-form-urlencoded',
      ),
    );
  }

  @override
  Future<ResponseStatus> profile() async {
    return await _get(profilePath());
  }

  @override
  Future<ResponseStatus> createUser(User user) async {
    return await _post(usersPath(), params: user.toJson());
  }

  @override
  Future<ResponseStatus> lessons() async {
    return await _get(lessonsPath());
  }

  @override
  Future<ResponseStatus> createLesson(Lesson lesson) async {
    return await _post(lessonsPath(), params: lesson.toJson());
  }

  @override
  Future<ResponseStatus> deleteLesson(int id) async {
    return await _delete(lessonsPath(id: id));
  }

  @override
  Future<ResponseStatus> editLesson(int id, Lesson lesson) async {
    return await _put(lessonsPath(id: id), params: lesson.toJson());
  }

  @override
  Future<ResponseStatus> equipments() async {
    return await _get(equipmentsPath());
  }

  @override
  Future<ResponseStatus> tags() async {
    return await _get(tagsPath());
  }

  @override
  Future<ResponseStatus> students() async {
    return await _get(playersPath());
  }

  @override
  Future<ResponseStatus> createStudent(Student student) async {
    return await _post(usersPath(), params: student.toJson());
  }

  @override
  Future<ResponseStatus> editStudent(int id, Student student) async {
    return await _post(usersPath(id: id), params: student.toJson());
  }

  @override
  Future<ResponseStatus> createProgram(Program program) async {
    return await _post(programPath(), params: program.toJson());
  }

  @override
  Future<ResponseStatus> programs() async {
    return await _get(programPath());
  }

  @override
  Future<ResponseStatus> deleteProgram(int id) async {
    return await _delete(programPath(id: id));
  }

  @override
  Future<ResponseStatus> editProgram(int id, Program program) async {
    return await _put(programPath(id: id), params: program.toJson());
  }

  @override
  Future<ResponseStatus> createPrivateLessons(PrivateLesson privateLesson) async {
    return await _post(privateLessonPath(), params: privateLesson.toJson());
  }

  @override
  Future<ResponseStatus> deletePrivateLessons(int id) async {
    return await _delete(privateLessonPath(id: id));
  }

  @override
  Future<ResponseStatus> privateLessons() async {
    return await _get(privateLessonPath());
  }

  @override
  Future<ResponseStatus> places() async {
    return await _get(locationPath());
  }

  @override
  Future<ResponseStatus> createPlace(String text) async {
    return await _post(locationPath(), params: {'message': text});
  }

  @override
  Future<ResponseStatus> deletePlace(int id) async {
    return await _delete(locationPath(id: id));
  }

  //Private

  Future<Response> _callRequest(Future<Response> request) async {
    try {
      final result = await InternetAddress.lookup('google.com');
      final connected = result.isNotEmpty && result[0].rawAddress.isNotEmpty;
      if (!connected) {
        generalErrorsSubject.add("Please check your internet connection");
        return Future.value(Response(
          data: 'no_internet_connection',
          statusCode: 503,
        ));
      }
      var response = await request;
      return Future.value(response);
    } on DioError catch (e) {
      if (e.response == null) {
        generalErrorsSubject.add("Something went wrong");
        return Future.value(Response(data: "server_error", statusCode: -1));
      } else if (e.response.statusCode == 401) {
        tokenExpiredSubject.add(true);
      }
      return Future.value(e.response);
    } on SocketException catch (_) {
      generalErrorsSubject.add("Please check your internet connection");
      return Future.value(Response(
        data: 'no_internet_connection',
        statusCode: 503,
      ));
    }
  }

  Future<ResponseStatus> _get(String url, {Map<String, dynamic> params}) async {
    return _handleResponse(await _callRequest(_dio.get(url, queryParameters: params)));
  }

  Future<ResponseStatus> _post(url, {Map<String, dynamic> params, Options options}) async {
    return _handleResponse(await _callRequest(_dio.post(url, data: params, options: options)));
  }

  Future<ResponseStatus> _put(url, {Map<String, dynamic> params}) async {
    return _handleResponse(await _callRequest(_dio.put(url, data: params)));
  }

  Future<ResponseStatus> _delete(url, {Map<String, dynamic> params}) async {
    return _handleResponse(await _callRequest(_dio.delete(url, queryParameters: params)));
  }

  ResponseStatus _handleResponse(Response response) {
    if (statusCodesSuccess.contains(response.statusCode)) {
      return ResponseSuccess(response.data);
    } else {
      final data = response?.data;
      if (data == 'no_internet_connection' || data == 'server_error') {
        return ServerError();
      }
      var message = 'Something went wrong';
      return ClientError.withMessage(message);
    }
  }

  Future dismissed;

  _showGeneralError(String text) {
    if (dismissed != null) return;
    final overlay = showSimpleNotification(
        Text(
          text,
          style: buttonTextStyle,
        ),
        background: appWarningColor);
    dismissed = overlay.dismissed.whenComplete(() {
      dismissed = null;
    });
  }
}
