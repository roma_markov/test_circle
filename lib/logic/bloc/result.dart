class Result<D> {
  D data;

  Result({this.data});

  factory Result.success(D data) {
    return Result(data: data);
  }
}

abstract class ResponseStatus {
  String message;
  bool isSuccess;

  ResponseStatus();

  ResponseStatus.withMessage(this.message);

  bool get isError => !(this is ResponseSuccess);
}

class ResponseSuccess<T> extends ResponseStatus {
  final T data;

  ResponseSuccess(this.data) : super();

  ResponseSuccess.ok() : this(null);

  ResponseSuccess.withMessage(message, this.data) : super.withMessage(message);

  List<R> asList<R>(R map(dynamic raw)) {
    final iterable = data;
    if (iterable is Iterable) {
      final list = iterable.map(map).toList();
      list.removeWhere((item) => item == null);
      return List<R>.from(list);
    }
    return null;
  }
}

class ClientError extends ResponseStatus {
  ClientError() : super();

  ClientError.withMessage(message) : super.withMessage(message);
}

class ServerError extends ResponseStatus {
  ServerError() : super();

  ServerError.withMessage(message) : super.withMessage(message);
}

class Loading extends ResponseStatus {
  Loading() : super();

  Loading.withMessage(message) : super.withMessage(message);
}
