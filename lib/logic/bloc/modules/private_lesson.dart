import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/private_lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/logic/repositories/base.dart';
import 'package:lesson_scheduling_pga/logic/repositories/lesson.dart';
import 'package:lesson_scheduling_pga/logic/repositories/place.dart';
import 'package:lesson_scheduling_pga/logic/repositories/player.dart';
import 'package:lesson_scheduling_pga/logic/repositories/private_lesson.dart';
import 'package:lesson_scheduling_pga/platform/index.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/event_list.dart';
import 'package:rxdart/rxdart.dart';

class PrivateLessonBLoC extends BaseBLoC {
  final LessonRepository lessonRepository;
  final PlayerRepository playerRepository;
  final PlaceRepository placeRepository;
  final PrivateLessonRepository privateLessonRepository;

  final _progressSubject = BehaviorSubject<bool>();
  final _eventsSubject = BehaviorSubject<List<PrivateLesson>>();

  Stream<bool> get progressStream => _progressSubject.stream;

  Stream<List<PrivateLesson>> get eventsStream => _eventsSubject.stream;

  PrivateLessonBLoC(
    this.lessonRepository,
    this.playerRepository,
    this.privateLessonRepository,
    this.placeRepository,
  );

  Future<Status> requestPrivateLessons({bool force = false}) async {
    _progressSubject.add(!force);
    return await _requestPrivateLessons(force: force).whenComplete(() {
      _progressSubject.add(false);
    });
  }

  Future<Status> _requestPrivateLessons({bool force = false}) async {
    final responsePrograms = await privateLessonRepository.privateLessons(force: force);
    final responseLessons = await lessonRepository.lessons();
    final responsePlayers = await playerRepository.students();
    final responsePlaces = await placeRepository.places();
    if (responsePrograms.isSuccess &&
        responseLessons.isSuccess &&
        responsePlayers.isSuccess &&
        responsePlaces.isSuccess) {
      final privateLessons = responsePrograms.data;
      PrivateLessonRepository.fillPlayers(privateLessons, responsePlayers.data);
      PrivateLessonRepository.fillLessons(privateLessons, responseLessons.data);
      PrivateLessonRepository.fillPlaces(privateLessons, responsePlaces.data);
      _fillPrivateLessonDays(privateLessons);
    }
    return responsePrograms;
  }

  Future<Status> createPrivateLesson(PrivateLesson privateLesson) async {
    _progressSubject.add(true);
    final result = await privateLessonRepository.create(privateLesson);
    if (result.isSuccess) await _requestPrivateLessons(force: true);
    _progressSubject.add(false);
    return result;
  }

  Future<Status> deletePrivateLesson(int id) async {
    _progressSubject.add(true);
    final result = await privateLessonRepository.delete(id);
    if (result.isSuccess) await _requestPrivateLessons(force: true);
    _progressSubject.add(false);
    return result;
  }

  _fillPrivateLessonDays(List<PrivateLesson> privateLessons) {
    _eventsSubject.add(privateLessons);
  }

  @override
  void dispose() {
    _progressSubject.close();
    _eventsSubject.close();
  }

  static List<dynamic> dayEvents(EventList eventList, DateTime date) {
    final thisDayEvents = List<dynamic>();
    thisDayEvents.addAll(eventList.getEvents(date).whereType<PrivateLesson>().toList());
    final thisMonthProgramEvents = eventList.getProgramsForMonth(date);
    thisMonthProgramEvents.forEach((event) {
      event.lessonsSchedule.forEach((lesson, schedules) {
        final schedule = schedules.firstWhere((item) => item.date == date, orElse: () => null);
        if (schedule != null) {
          schedule.program = event;
          thisDayEvents.add(schedule);
        }
      });
    });
    return thisDayEvents;
  }

  static bool validateDateTime(EventList eventList, int programId, Lesson lesson, DateTime date, TimeOfDay time) {
    final dateTimeStart = DateTime(date.year, date.month, date.day, time.hour, time.minute);
    final dateTimeEnd = DateTime(date.year, date.month, date.day, time.hour + lesson.durationHours, time.minute);
    final eventsForDay = dayEvents(eventList, date);
    List<StartEndPair> lessonRanges = [];
    eventsForDay.forEach((event) {
      if (event is PrivateLesson) {
        lessonRanges.add(StartEndPair(event.start, event.end));
      } else if (event is Schedule) {
        if (!(event.program.id == programId && event.lesson.id == lesson.id)) {
          lessonRanges.add(StartEndPair(event.start, event.end));
        }
      }
    });
    _combineRanges(lessonRanges, lesson.durationHours);
    final intersected = lessonRanges.firstWhere(
      (pair) => pair.isIntersects(dateTimeStart, dateTimeEnd),
      orElse: () => null,
    );
    return intersected == null;
  }

  static _combineRanges(List<StartEndPair> lessonRanges, int itemDuration) {
    if (lessonRanges.isEmpty) return;

    lessonRanges.sort((left, right) => left.start.compareTo(right.start));
    final first = lessonRanges.first.start;
    if (first.hour < itemDuration) {
      lessonRanges.first = StartEndPair(DateTime(first.year, first.month, first.day, 0, 0), lessonRanges.first.end);
    }
    final last = lessonRanges.last.end;
    if (last.hour > 24 - itemDuration) {
      lessonRanges.last = StartEndPair(lessonRanges.last.start, DateTime(last.year, last.month, last.day, 24, 60));
    }
    StartEndPair pair = lessonRanges.first;
    for (int index = 1; index < lessonRanges.length; index++) {
      final item = lessonRanges[index];
      if (item.start.hour - pair.end.hour < itemDuration) {
        pair.end = item.end;
        lessonRanges.removeAt(index);
      } else {
        pair = item;
      }
    }
  }

  @override
  void clear() {
    privateLessonRepository.clearData();
  }
}

class StartEndPair {
  StartEndPair(this.start, this.end);

  DateTime start;
  DateTime end;

  bool isIntersects(DateTime itemStart, DateTime itemEnd) {
    return (itemEnd.millisecondsSinceEpoch > start.millisecondsSinceEpoch &&
            itemEnd.millisecondsSinceEpoch < end.millisecondsSinceEpoch ||
        (itemStart.millisecondsSinceEpoch >= start.millisecondsSinceEpoch &&
            itemStart.millisecondsSinceEpoch < end.millisecondsSinceEpoch));
  }
}
