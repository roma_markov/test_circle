import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/place.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/logic/repositories/base.dart';
import 'package:lesson_scheduling_pga/logic/repositories/lesson.dart';
import 'package:lesson_scheduling_pga/logic/repositories/place.dart';
import 'package:lesson_scheduling_pga/logic/repositories/player.dart';
import 'package:lesson_scheduling_pga/logic/repositories/program.dart';
import 'package:lesson_scheduling_pga/utils/date_utils.dart';
import 'package:rxdart/rxdart.dart';

class ProgramBLoC extends BaseBLoC {
  final LessonRepository lessonRepository;
  final PlayerRepository playerRepository;
  final PlaceRepository placeRepository;
  final ProgramRepository programRepository;

  ProgramBLoC(
    this.lessonRepository,
    this.playerRepository,
    this.programRepository,
    this.placeRepository,
  );

  final _progressSubject = BehaviorSubject<bool>();
  final _programsSubject = BehaviorSubject<List<Program>>();
  final _programDetailsSubject = BehaviorSubject<Program>();

  final _eventsSubject = BehaviorSubject<List<Program>>();

  Stream<List<Program>> get programsStream => _programsSubject.stream;

  Stream<Program> get programDetailsStream => _programDetailsSubject.stream;

  Stream<bool> get progressStream => _progressSubject.stream;

  Stream<List<Program>> get eventsStream => _eventsSubject.stream;

  String _searchTerm = '';

  Future<Status> requestPrograms({bool force = false}) async {
    _progressSubject.add(force);
    final result = await _requestPrograms(force: force);
    _progressSubject.add(false);
    return result;
  }

  Future<Status> _requestPrograms({bool force = false}) async {
    final responsePrograms = await programRepository.programs(force: force);
    final responseLessons = await lessonRepository.lessons();
    final responsePlayers = await playerRepository.students();
    final responsePlaces = await placeRepository.places();
    if (responsePrograms.isSuccess &&
        responseLessons.isSuccess &&
        responsePlayers.isSuccess &&
        responsePlaces.isSuccess) {
      final programs = responsePrograms.data;
      ProgramRepository.fillPlayers(programs, responsePlayers.data);
      ProgramRepository.fillLessons(programs, responseLessons.data);
      ProgramRepository.fillPlaces(programs, responsePlaces.data);
      _programsSubject.add(programs);
      _fillProgramDays(programs);
      _applySearch();
    }
    return responsePrograms;
  }

  program(int programId) {
    _programDetailsSubject.add(
      _programsSubject.value?.firstWhere((program) => program.id == programId, orElse: () => null),
    );
  }

  Future<Status> createProgram(Program newProgram, bool isPublish) async {
    _progressSubject.add(true);
    final result = await programRepository.create(newProgram, isPublish);
    if (result.isSuccess) await _requestPrograms(force: true);
    _progressSubject.add(false);
    return result;
  }

  Future<Status> editProgram(Program program) async {
    _progressSubject.add(true);
    final result = await programRepository.edit(program);
    if (result.isSuccess) await _requestPrograms(force: true);
    _progressSubject.add(false);
    return result;
  }

  Future<Status> deleteProgram(int programId) async {
    _progressSubject.add(true);
    final result = await programRepository.delete(programId);
    if (result.isSuccess) await _requestPrograms(force: true);
    _progressSubject.add(false);
    return result;
  }

  Future<ResponseStatus> editProgramInfo(
    int programId,
    String name,
    String goal,
    int duration,
    int capacity,
    Place place,
  ) async {
    final program = _programsSubject.value.firstWhere((program) => program.id == programId);
    final index = _programsSubject.value.indexOf(program);
    program.name = name;
    program.goal = goal;
    program.startDates.clear();
    program.lessonsSchedule.clear();
    program.durationDays = duration;
    program.capacity = capacity;
    program.place = place;

    _programsSubject.value.removeAt(index);
    _programsSubject.value.insert(index, program);
    return ResponseSuccess.ok();
  }

  Future<Program> updateScheduledLessons(Program program) async {
    List<DatePair> availableRanges = [];
    program.startDates.forEach((start) {
      availableRanges.add(DatePair(start, start.add(Duration(days: program.durationDays - 1))));
    });
    Map<Lesson, List<Schedule>> result = Map();
    program.lessonsSchedule.forEach((lesson, list) {
      List<Schedule> validSchedules =
          list.where((schedule) => DateUtils.isInRanges(availableRanges, schedule.date)).toList();
      if (validSchedules != null) {
        result[lesson] = validSchedules;
      }
    });
    program.lessonsSchedule = result;
    return program;
  }

  changeSearchFilter(String searchString) {
    _searchTerm = searchString ?? '';
    _applySearch();
  }

  _applySearch() async {
    final resultPrograms = await programRepository.programs(force: false);
    List<Program> result = resultPrograms.data;
    if (result == null) {
      _programsSubject.add([]);
      return;
    }

    if (result != null && _searchTerm.isNotEmpty) {
      final searchString = _searchTerm.toLowerCase();
      result = result.where((item) => item.name.toLowerCase().contains(searchString)).toList();
    }
    _programsSubject.add(result ?? result);
  }

  _fillProgramDays(List<Program> programs) {
    _eventsSubject.add(programs);
  }

  @override
  void clear() {
    programRepository.clearData();
    _searchTerm = '';
  }

  @override
  void dispose() {
    _eventsSubject.close();
    _progressSubject.close();
    _programsSubject.close();
    _programDetailsSubject.close();
  }
}
