import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/place.dart';
import 'package:lesson_scheduling_pga/logic/repositories/base.dart';
import 'package:lesson_scheduling_pga/logic/repositories/place.dart';
import 'package:rxdart/rxdart.dart';

class PlaceBLoC extends BaseBLoC {
  final PlaceRepository placeRepository;

  final _progressSubject = BehaviorSubject<bool>();
  final _placesSubject = BehaviorSubject<List<Place>>.seeded([]);
  final _placeDetailsSubject = BehaviorSubject<Place>();

  Stream<List<Place>> get placesStream => _placesSubject.stream;

  Stream<Place> get placeDetailsStream => _placeDetailsSubject.stream;

  Stream<bool> get progressStream => _progressSubject.stream;

  String _searchTerm = '';

  PlaceBLoC(this.placeRepository) : super();

  Future<Status> requestPlaces({bool force = false}) async {
    _progressSubject.add(true);
    final result = await _requestPlaces(force: force);
    _progressSubject.add(false);
    return result;
  }

  Future<Status> place(int id) async {
    final result = await placeRepository.place(id);
    if (result.isSuccess) _placeDetailsSubject.add(result.data);
    return result;
  }

  Future<Place> placeByMessage(String text) async {
    final result = await placeRepository.places();
    if (result.isSuccess) {
      return result.data.singleWhere(
        (place) => place.name.toLowerCase() == text.toLowerCase(),
        orElse: () => null,
      );
    }
    return null;
  }

  Future<Status<Place>> createPlace(String text) async {
    _progressSubject.add(true);
    final result = await placeRepository.create(text);
    if (result.isSuccess) await _requestPlaces(force: true);
    _progressSubject.add(false);
    return result;
  }

  Future<Status> deletePlace(int id) async {
    final result = await placeRepository.delete(id);
    if (result.isSuccess) await requestPlaces(force: true);
    return result;
  }

  changeSearchFilter(String searchString) {
    _searchTerm = searchString ?? '';
    _applySearch();
  }

  _applySearch() async {
    final resultPlaces = await placeRepository.places(force: false);
    List<Place> result = resultPlaces.data;
    if (result == null) {
      _placesSubject.add([]);
      return;
    }
    if (_searchTerm.isNotEmpty) {
      final searchString = _searchTerm.toLowerCase();
      result = result.where((item) => item.name.toLowerCase().contains(searchString)).toList();
    }
    _placesSubject.add(result ?? []);
  }

  Future<Status> _requestPlaces({bool force = false}) async {
    final result = await placeRepository.places(force: force);
    if (result.isSuccess) _applySearch();
    return result;
  }

  @override
  void dispose() {
    _progressSubject.close();
    _placesSubject.close();
    _placeDetailsSubject.close();
  }

  @override
  void clear() {
    placeRepository.clearData();
    _searchTerm = '';
  }
}
