import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/equipment.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/tag.dart';
import 'package:lesson_scheduling_pga/logic/repositories/base.dart';
import 'package:lesson_scheduling_pga/logic/repositories/lesson.dart';
import 'package:rxdart/rxdart.dart';

class LessonBLoC extends BaseBLoC {
  final LessonRepository lessonRepository;

  final _progressSubject = BehaviorSubject<bool>();
  final _lessonsSubject = BehaviorSubject<List<Lesson>>();
  final _lessonDetailsSubject = BehaviorSubject<Lesson>();
  final _equipmentSubject = BehaviorSubject<List<Equipment>>();
  final _tagsSubject = BehaviorSubject<List<Tag>>.seeded([]);

  Stream<List<Lesson>> get lessonsStream => _lessonsSubject.stream;

  Stream<Lesson> get lessonDetailsStream => _lessonDetailsSubject.stream;

  Stream<List<Equipment>> get equipmentStream => _equipmentSubject.stream;

  Stream<List<Tag>> get tagsStream => _tagsSubject.stream;

  Stream<bool> get progressStream => _progressSubject.stream;

  String _searchTerm = '';
  Set<Tag> _selectedTags;

  LessonBLoC(this.lessonRepository) : super();

  Future<Status> requestLessons({bool force = false}) async {
    _progressSubject.add(true);
    await _requestAdditional();
    final result = await lessonRepository.lessons(force: force);
    if (result.isSuccess) _applySearch();
    _progressSubject.add(false);
    return result;
  }

  Future<Status> lesson(int lessonId) async {
    final result = await lessonRepository.lesson(lessonId);
    if (result.isSuccess) _lessonDetailsSubject.add(result.data);
    return result;
  }

  Future<Status> createLesson(
    String name,
    String goal,
    int duration,
    String note,
    List<Equipment> equipment,
    List<Tag> tags,
  ) async {
    _progressSubject.add(true);
    final result = await lessonRepository.create(name, goal, duration, note, equipment, tags);
    if (result.isSuccess) await requestLessons(force: true);
    _progressSubject.add(false);
    return result;
  }

  Future<Status> editLesson(
    int lessonId,
    String name,
    String goal,
    int duration,
    String note,
    List<Equipment> equipment,
    List<Tag> tags,
  ) async {
    _progressSubject.add(true);
    final result = await lessonRepository.edit(lessonId, name, goal, duration, note, equipment, tags);
    if (result.isSuccess) await requestLessons(force: true);
    _progressSubject.add(false);
    return result;
  }

  Future<Status> deleteLesson(int lessonId) async {
    final result = await lessonRepository.delete(lessonId);
    if (result.isSuccess) await requestLessons(force: true);
    return result;
  }

  changeSearchFilter(String searchString) {
    _searchTerm = searchString ?? '';
    _applySearch();
  }

  changeTagsFilter(Set<Tag> tags) {
    _selectedTags = tags;
    _applySearch();
  }

  _requestAdditional() async {
    final result = await lessonRepository.equipments();
    if (result.isSuccess) {
      _equipmentSubject.add(result.data);
    }
    final resultTags = await lessonRepository.tags();
    if (resultTags.isSuccess) {
      _tagsSubject.add(resultTags.data);
    }
  }

  _applySearch() async {
    final resultLessons = await lessonRepository.lessons(force: false);
    List<Lesson> result = resultLessons.data;
    if (result == null) {
      _lessonsSubject.add([]);
      return;
    }

    if (_selectedTags != null && _selectedTags.isNotEmpty) {
      result = result.where((item) => item.tags.any((tag) => _selectedTags.contains(tag))).toList();
    }
    if (_searchTerm.isNotEmpty) {
      final searchString = _searchTerm.toLowerCase();
      result = result.where((item) => item.name.toLowerCase().contains(searchString)).toList();
    }
    _lessonsSubject.add(result ?? []);
  }

  @override
  void dispose() {
    _progressSubject.close();
    _lessonsSubject.close();
    _lessonDetailsSubject.close();
    _equipmentSubject.close();
    _tagsSubject.close();
  }

  @override
  void clear() {
    lessonRepository.clearData();
    _searchTerm = '';
    _selectedTags = null;
  }
}
