import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/user.dart';
import 'package:lesson_scheduling_pga/logic/repositories/base.dart';
import 'package:lesson_scheduling_pga/logic/services/api/api.dart';
import 'package:lesson_scheduling_pga/platform/index.dart';
import 'package:lesson_scheduling_pga/platform/local_storage/local_storage.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

class AuthBLoC extends BaseBLoC {
  final _progressSubject = BehaviorSubject<bool>();

  Stream<bool> get progressStream => _progressSubject.stream;

  final Api api;
  final LocalStorage storage;

  AuthBLoC(this.api, this.storage) : super();

  Future<Status> login({
    @required String email,
    @required String password,
  }) async {
    _progressSubject.add(true);
    final response = await api.auth(email, password);
    if (response is ResponseSuccess) {
      storage.setToken(response.data['access_token']);
      final profileResponse = await api.profile();
      if (profileResponse is ResponseSuccess) {
        _saveUser(User.fromJson(profileResponse.data));
      }
      _progressSubject.add(false);
      return Status.success(true);
    }
    _progressSubject.add(false);
    if (response is ServerError) {
      return Status.error();
    }
    return Status.error(message: 'Wrong email or password');
  }

  Future<Status> createCoach({
    @required String email,
    @required String password,
    @required String firstName,
    @required String lastName,
  }) async {
    _progressSubject.add(true);
    final response = await api.createUser(User(firstName, lastName, -1, email, 'coach', password));
    if (response is ResponseSuccess) {
      _progressSubject.add(false);
      return Status.success(true);
    }
    _progressSubject.add(false);
    return Status.error(message: 'Something went wrong');
  }

  _saveUser(User user) {
    storage.setAuthId(user.id);
    storage.setAuthEmail(user.email);
    storage.setLastName(user.lastName);
    storage.setFirstName(user.firstName);
  }

  logout() {
    api.logout();
  }

  @override
  void dispose() {
    _progressSubject.close();
  }

  @override
  void clear() {}
}
