import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';
import 'package:lesson_scheduling_pga/logic/repositories/base.dart';
import 'package:lesson_scheduling_pga/logic/repositories/player.dart';
import 'package:rxdart/rxdart.dart';

class StudentBLoC extends BaseBLoC {
  final PlayerRepository playerRepository;

  final _progressSubject = BehaviorSubject<bool>();
  final _studentsSubject = BehaviorSubject<List<Student>>();

  Stream<List<Student>> get studentsStream => _studentsSubject.stream;

  Stream<bool> get progressStream => _progressSubject.stream;

  String _searchTerm = '';

  StudentBLoC(this.playerRepository) : super();

  Future<Status> requestStudents({bool force = false}) async {
    _progressSubject.add(!force);
    final response = await _requestStudents(force: force);
    if (response.isSuccess) _applySearch();
    _progressSubject.add(false);
    return response;
  }

  Future<Status> _requestStudents({bool force = false}) async {
    return await playerRepository.students(force: force);
  }

  Future<Status> createStudent(Student student) async {
    _progressSubject.add(true);
    final result = await playerRepository.create(student);
    if (result.isSuccess) await _requestStudents(force: true);
    _progressSubject.add(false);
    return result;
  }

  Future<Status> editStudent(Student student) async {
    _progressSubject.add(true);
    final result = await playerRepository.edit(student.id, student);
    if (result.isSuccess) await _requestStudents(force: true);
    _progressSubject.add(false);
    return result;
  }

  changeSearchFilter(String searchString) {
    _searchTerm = searchString ?? '';
    _applySearch();
  }

  _applySearch() async {
    final response = await playerRepository.students();
    List<Student> result = response.data;
    if (result != null && _searchTerm.isNotEmpty) {
      final searchString = _searchTerm.toLowerCase();
      result = List<Student>.from(result.where((item) => _searchFunction(item, searchString)));
    }
    _studentsSubject.add(result ?? []);
  }

  bool _searchFunction(Student element, String searchString) {
    return element.firstName.toLowerCase().contains(searchString) ||
        element.lastName.toLowerCase().contains(searchString) ||
        element.email.toLowerCase().contains(searchString);
  }

  @override
  void clear() {
    playerRepository.clearData();
    _searchTerm = '';
  }

  @override
  void dispose() {
    _progressSubject.close();
    _studentsSubject.close();
  }
}
