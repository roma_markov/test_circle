import 'package:dio/dio.dart';
import 'package:lesson_scheduling_pga/logic/models/private_lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/logic/repositories/lesson.dart';
import 'package:lesson_scheduling_pga/logic/repositories/place.dart';
import 'package:lesson_scheduling_pga/logic/repositories/player.dart';
import 'package:lesson_scheduling_pga/logic/repositories/private_lesson.dart';
import 'package:lesson_scheduling_pga/logic/repositories/program.dart';
import 'package:lesson_scheduling_pga/logic/services/api/api.dart';
import 'package:lesson_scheduling_pga/logic/services/api/api_client.dart';
import 'package:lesson_scheduling_pga/platform/local_storage/local_storage.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/event_list.dart';
import 'package:rxdart/rxdart.dart';

import '../../platform/index.dart';
import 'modules/auth.dart';
import 'modules/lesson.dart';
import 'modules/place.dart';
import 'modules/private_lesson.dart';
import 'modules/program.dart';
import 'modules/student.dart';

final tokenExpiredSubject = PublishSubject<bool>();

class BLoC {
  LocalStorage storage;
  AuthBLoC auth;
  LessonBLoC lesson;
  StudentBLoC student;
  ProgramBLoC program;
  PrivateLessonBLoC privateLesson;
  PlaceBLoC place;

  BLoC._private();

  BLoC._internal({
    @required this.storage,
    @required this.auth,
    @required this.lesson,
    @required this.program,
    @required this.student,
    @required this.privateLesson,
    @required this.place,
  });

  factory BLoC.dev() => BLoC._buildBloc(ApiClient.baseUrlDev);

  factory BLoC.prod() => BLoC._buildBloc(ApiClient.baseUrlProd);

  factory BLoC.mock() => BLoC._buildBloc(ApiClient.baseUrlProd);

  factory BLoC._buildBloc(String baseUrl) {
    final storage = SharedStorage();
    final api = _buildApi(baseUrl, storage);

    final lessonRepository = LessonRepository(api);
    final playerRepository = PlayerRepository(api);
    final programRepository = ProgramRepository(api);
    final privateLessonsRepository = PrivateLessonRepository(api);
    final placeRepository = PlaceRepository(api);

    return BLoC._internal(
      storage: storage,
      auth: AuthBLoC(api, storage),
      lesson: LessonBLoC(lessonRepository),
      student: StudentBLoC(playerRepository),
      program: ProgramBLoC(
        lessonRepository,
        playerRepository,
        programRepository,
        placeRepository,
      ),
      privateLesson: PrivateLessonBLoC(
        lessonRepository,
        playerRepository,
        privateLessonsRepository,
        placeRepository,
      ),
      place: PlaceBLoC(placeRepository),
    );
  }

  logout(VoidCallback callback) {
    auth.logout();
    auth.clear();
    lesson.clear();
    student.clear();
    program.clear();
    privateLesson.clear();
    place.clear();
    callback();
  }

  Stream<EventList> eventsStream() {
    return ZipStream.zip2(
      program.eventsStream,
      privateLesson.eventsStream,
      (List<Program> programs, List<PrivateLesson> privateLessons) {
        EventList result = EventList();
        result.addPrograms(programs);
        result.addPrivateLessons(privateLessons);
        return result;
      },
    );
  }

  static Api _buildApi(String baseUrl, LocalStorage storage) =>
      ApiClient(baseUrl: baseUrl, storage: storage)..tokenExpiredSubject = tokenExpiredSubject;

  dispose() {
    auth.dispose();
  }
}
