import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/place.dart';
import 'package:lesson_scheduling_pga/logic/services/api/api.dart';

import 'base.dart';

class PlaceRepository extends BaseRepository {
  PlaceRepository(Api api) : super(api);

  List<Place> _places;

  Future<Status<List<Place>>> places({bool force = false}) async {
    if (_places != null && !force) return Status.success(_places);

    final response = await api.places();
    if (response is ResponseSuccess) {
      _places = response.asList((raw) => Place.fromJson(raw));
      return Status.success(_places);
    }
    return Status.error();
  }

  Future<Status<Place>> place(int id) async {
    final result = await places();
    if (result.isSuccess) {
      final place = result.data.firstWhere((item) => item.id == id, orElse: () => null);
      return place != null ? Status.success(place) : Status.error();
    }
    return Status.error();
  }

  Future<Status<Place>> create(String text) async {
    final response = await api.createPlace(text);
    if (response is ResponseSuccess) {
      return Status.success(Place.fromJson(response.data));
    }
    return Status.error();
  }

  Future<Status> delete(int id) async {
    final response = await api.deletePlace(id);
    if (response is ResponseSuccess) {
      return Status.success(true);
    }
    return Status.error();
  }

  @override
  void clearData() {
    _places = null;
  }
}
