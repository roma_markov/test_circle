import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/equipment.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/tag.dart';
import 'package:lesson_scheduling_pga/logic/services/api/api.dart';

import 'base.dart';

class LessonRepository extends BaseRepository {
  LessonRepository(Api api) : super(api);

  List<Lesson> _lessons;
  List<Equipment> _equipment;
  List<Tag> _tags;

  Future<Status<List<Lesson>>> lessons({bool force = false}) async {
    final resultLessons = await _requestLessons(force: force);
    final resultEquipments = await equipments();
    final resultTags = await tags();
    if (resultLessons.isSuccess && resultEquipments.isSuccess && resultTags.isSuccess) {
      final equipments = resultEquipments.data;
      final tags = resultTags.data;
      final lessons = resultLessons.data;
      LessonRepository._fillLessonWithEquipment(lessons, equipments);
      LessonRepository._fillLessonWithTags(lessons, tags);
      return Status.success(lessons);
    }
    return Status.error();
  }

  Future<Status<Lesson>> lesson(int lessonId) async {
    final result = await lessons();
    if (result.isSuccess) {
      final lesson = result.data.firstWhere((lesson) => lesson.id == lessonId, orElse: () => null);
      return lesson != null ? Status.success(lesson) : Status.error();
    }
    return Status.error();
  }

  Future<Status<bool>> create(
    String name,
    String goal,
    int duration,
    String note,
    List<Equipment> equipment,
    List<Tag> tags,
  ) async {
    final newLesson = Lesson(
      name,
      goal,
      duration,
      note,
      equipment.map((item) => item.id).toList(),
      tags.map((item) => item.id).toList(),
    );
    final response = await api.createLesson(newLesson);
    if (response is ResponseSuccess) {
      return Status.success(true);
    }
    return Status.error();
  }

  Future<Status<bool>> edit(
    int lessonId,
    String name,
    String goal,
    int duration,
    String note,
    List<Equipment> equipment,
    List<Tag> tags,
  ) async {
    final lesson = Lesson(
      name,
      goal,
      duration,
      note,
      equipment.map((item) => item.id).toList(),
      tags.map((item) => item.id).toList(),
    );
    final response = await api.editLesson(lessonId, lesson);
    if (response is ResponseSuccess) {
      return Status.success(true);
    }
    return Status.error();
  }

  Future<Status> delete(int lessonId) async {
    final response = await api.deleteLesson(lessonId);
    if (response is ResponseSuccess) {
      return Status.success(true);
    }
    return Status.error();
  }

  Future<Status<List<Equipment>>> equipments() async {
    if (_equipment != null) {
      return Status.success(_equipment);
    }
    final response = await api.equipments();
    if (response is ResponseSuccess) {
      _equipment = response.asList((raw) => Equipment.fromJson(raw));
      return Status.success(_equipment);
    }
    return Status.error();
  }

  Future<Status<List<Tag>>> tags() async {
    if (_tags != null) {
      return Status.success(_tags);
    }
    final response = await api.tags();
    if (response is ResponseSuccess) {
      _tags = response.asList((raw) => Tag.fromJson(raw));
      return Status.success(_tags);
    }
    return Status.error();
  }

  Future<Status<List<Lesson>>> _requestLessons({bool force = false}) async {
    if (_lessons != null && !force) return Status.success(_lessons);

    final response = await api.lessons();
    if (response is ResponseSuccess) {
      _lessons = response.asList((raw) => Lesson.fromJson(raw));
      return Status.success(_lessons);
    }
    return Status.error();
  }

  static _fillLessonWithEquipment(List<Lesson> lessons, List<Equipment> equipment) {
    lessons.forEach((lesson) {
      lesson.equipment = lesson.equipmentIds.map((id) => equipment.singleWhere((item) => item.id == id)).toList();
    });
  }

  static _fillLessonWithTags(List<Lesson> lessons, List<Tag> tags) {
    lessons.forEach((lesson) {
      lesson.tags = lesson.tagIds.map((id) => tags.singleWhere((item) => item.id == id)).toList();
    });
  }

  @override
  void clearData() {
    _lessons = null;
    _equipment = null;
    _tags = null;
  }
}
