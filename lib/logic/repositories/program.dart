import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/invitation_record.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/place.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';
import 'package:lesson_scheduling_pga/logic/services/api/api.dart';

import 'base.dart';

class ProgramRepository extends BaseRepository {
  ProgramRepository(Api api) : super(api);

  List<Program> _programs;

  Future<Status<List<Program>>> programs({bool force = false}) async {
    if (_programs != null && !force) return Status.success(_programs);

    final response = await api.programs();
    if (response is ResponseSuccess) {
      _programs = response.asList((raw) => Program.fromJson(raw));
      return Status.success(_programs);
    }
    return Status.error();
  }

  Future<Status<bool>> create(Program newProgram, bool isPublish) async {
    newProgram.isPublish = isPublish;
    newProgram.invitationRecords = List<InvitationRecord>.from(
      newProgram.players.map((item) => InvitationRecord.fromPlayerId(item.id)),
    );
    final response = await api.createProgram(newProgram);
    if (response is ResponseSuccess) {
      return Status.success(true);
    }
    return Status.error();
  }

  Future<Status<bool>> edit(Program program) async {
    program.isPublish = true;
    program.invitationRecords = List<InvitationRecord>.from(
      program.players.map((item) => InvitationRecord.fromPlayerId(item.id)),
    );
    final response = await api.editProgram(program.id, program);
    if (response is ResponseSuccess) {
      return Status.success(true);
    }
    return Status.error();
  }

  Future<Status> delete(int programId) async {
    final response = await api.deleteProgram(programId);
    if (response is ResponseSuccess) {
      return Status.success(true);
    }
    return Status.error();
  }

  @override
  void clearData() {
    _programs = null;
  }

  static fillPlayers(List<Program> programs, List<Student> students) {
    programs.forEach((program) {
      program.players = program.invitationRecords
          .map((record) => students.singleWhere((item) => item.id == record.playerId))
          .toList();
    });
  }

  static fillLessons(List<Program> programs, List<Lesson> lessons) {
    programs.forEach((program) {
      program.lessonsSchedule = program.lessonsSchedule ??
          program.lessonsScheduleMap.map((id, list) {
            final lesson = lessons.singleWhere((item) => item.id == id, orElse: () => null);
            return MapEntry(
                lesson,
                program.lessonsScheduleMap[id]
                    .map((task) => Schedule(
                          DateTime(task.startDate.year, task.startDate.month, task.startDate.day),
                          TimeOfDay.fromDateTime(task.startDate),
                          lesson,
                        ))
                    .toList());
          });
    });
  }

  static fillPlaces(List<Program> programs, List<Place> places) {
    programs.forEach((program) {
      program.place = program.place ?? places.singleWhere((item) => item.id == program.placeId, orElse: () => null);
    });
  }
}
