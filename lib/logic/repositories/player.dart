import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';
import 'package:lesson_scheduling_pga/logic/services/api/api.dart';

import 'base.dart';

class PlayerRepository extends BaseRepository {
  PlayerRepository(Api api) : super(api);

  List<Student> _students;

  Future<Status<List<Student>>> students({bool force = false}) async {
    if (_students != null && !force) return Status.success(_students);

    final response = await api.students();
    if (response is ResponseSuccess) {
      _students = response.asList((raw) => Student.fromJson(raw));
      return Status.success(_students);
    }
    return Status.error();
  }

  Future<Status<bool>> create(Student student) async {
    final response = await api.createStudent(student);
    if (response is ResponseSuccess) {
      return Status.success(true);
    }
    return Status.error();
  }

  Future<Status<bool>> edit(int userId, Student student) async {
    final response = await api.createStudent(student);
    if (response is ResponseSuccess) {
      return Status.success(true);
    }
    return Status.error();
  }

  @override
  void clearData() {
    _students = null;
  }
}
