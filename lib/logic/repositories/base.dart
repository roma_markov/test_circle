import 'package:lesson_scheduling_pga/logic/services/api/api.dart';

abstract class BaseRepository {
  Api _api;

  BaseRepository(Api api) : this._api = api;

  Api get api => _api;

  void clearData();
}

class Status<T> {
  final T data;
  final String errorMessage;
  final isSuccess;

  Status(this.data, this.errorMessage, this.isSuccess);

  Status.success(T data) : this(data, null, true);

  Status.error({String message}) : this(null, message, false);
}
