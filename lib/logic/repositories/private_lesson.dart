import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/invitation_record.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/place.dart';
import 'package:lesson_scheduling_pga/logic/models/private_lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';
import 'package:lesson_scheduling_pga/logic/services/api/api.dart';

import 'base.dart';

class PrivateLessonRepository extends BaseRepository {
  PrivateLessonRepository(Api api) : super(api);

  List<PrivateLesson> _privateLessons;

  Future<Status<List<PrivateLesson>>> privateLessons({bool force = false}) async {
    if (_privateLessons != null && !force) return Status.success(_privateLessons);

    final response = await api.privateLessons();
    if (response is ResponseSuccess) {
      _privateLessons = response.asList((raw) => PrivateLesson.fromJson(raw));
      return Status.success(_privateLessons);
    }
    return Status.error();
  }

  Future<Status<bool>> create(PrivateLesson privateLesson) async {
    privateLesson.invitationRecords =
        List<InvitationRecord>.from(privateLesson.players.map((item) => InvitationRecord.fromPlayerId(item.id)));
    final response = await api.createPrivateLessons(privateLesson);
    if (response is ResponseSuccess) {
      return Status.success(true);
    }
    return Status.error();
  }

  Future<Status> delete(int privateLessonId) async {
    final response = await api.deletePrivateLessons(privateLessonId);
    if (response is ResponseSuccess) {
      return Status.success(true);
    }
    return Status.error();
  }

  @override
  void clearData() {
    _privateLessons = null;
  }

  static fillPlayers(List<PrivateLesson> privateLessons, List<Student> students) {
    privateLessons.forEach((item) {
      item.players =
          item.invitationRecords.map((record) => students.singleWhere((item) => item.id == record.playerId)).toList();
    });
  }

  static fillLessons(List<PrivateLesson> privateLessons, List<Lesson> lessons) {
    privateLessons.forEach((item) {
      if (item.scheduleTask != null) {
        item.lesson = lessons.singleWhere((lesson) => lesson.id == item.scheduleTask.lessonId, orElse: () => null);
      }
    });
  }

  static fillPlaces(List<PrivateLesson> privateLessons, List<Place> places) {
    privateLessons.forEach((lesson) {
      lesson.place = places.singleWhere((item) => item.id == lesson.placeId, orElse: () => null);
    });
  }
}
