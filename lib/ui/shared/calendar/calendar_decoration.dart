import 'dart:math';

import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/private_lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/event_list.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

enum CalendarDecorationStyle {
  colored,
  grayed,
}

class CalendarDecoration {
  static Widget buildDecoration(
    BuildContext context,
    DateTime day,
    EventList eventList, {
    List<Program> activePrograms,
    CalendarDecorationStyle style = CalendarDecorationStyle.colored,
    WidgetBuilder additionalBuilder,
  }) {
    final showPrivateLessonEvent = eventList.getEvents(day).length > 0;
    final programs = eventList.getPrograms(day);
    Set<Lesson> programLessons = Set();
    programs.forEach((program) {
      if (activePrograms == null || !activePrograms.contains(program)) {
        program.lessonsSchedule.forEach((lesson, schedules) {
          if (schedules.any((item) => item.date == day)) {
            programLessons.add(lesson);
          }
        });
      }
    });
    Set<Lesson> activeProgramLessons = Set();
    activePrograms?.forEach((program) {
      program.lessonsSchedule.forEach((lesson, schedules) {
        if (schedules.any((item) => item.date == day)) {
          activeProgramLessons.add(lesson);
        }
      });
    });
    return Stack(
      children: <Widget>[
        Positioned(
          bottom: 5,
          right: 5,
          child: _buildLessonIndicators(
            showPrivateLessonEvent,
            programLessons,
            style,
            activeLessons: activeProgramLessons,
          ),
        ),
        Positioned(
          top: 0,
          right: 0,
          left: 0,
          child: _buildProgramIndicators(programs, style),
        ),
        Positioned.fill(
          child: additionalBuilder != null ? additionalBuilder(context) : Container(),
        ),
      ],
    );
  }

  static _buildLessonIndicators(
    bool showPrivateLessonEvent,
    Set<Lesson> programLessons,
    CalendarDecorationStyle style, {
    Set<Lesson> activeLessons,
  }) {
    List<Widget> indicators = programLessons.map((lesson) {
      return Container(
        width: 5,
        height: 5,
        margin: EdgeInsets.only(right: 2),
        decoration: BoxDecoration(
          color: style == CalendarDecorationStyle.colored ? colorById(lesson.id) : lightGrayColor,
          shape: BoxShape.circle,
        ),
      );
    }).toList();
    activeLessons?.forEach((lesson) {
      indicators.add(Container(
        width: 5,
        height: 5,
        margin: EdgeInsets.only(right: 2),
        decoration: BoxDecoration(
          color: colorById(lesson.id),
          shape: BoxShape.circle,
        ),
      ));
    });
    if (showPrivateLessonEvent) {
      indicators.add(Container(
        width: 5,
        height: 5,
        margin: EdgeInsets.only(right: 2),
        decoration: BoxDecoration(
          color: style == CalendarDecorationStyle.colored ? appRedColor : lightGrayColor,
          shape: BoxShape.circle,
        ),
      ));
    }
    return Row(
      children: indicators,
    );
  }

  static _buildProgramIndicators(List<Program> programs, CalendarDecorationStyle style) {
    double itemHeight = 12.0 / max(3, programs.length);
    final indicators = programs
        .map((program) => Container(
              height: itemHeight,
              color: style == CalendarDecorationStyle.colored ? colorById(program.id) : lightGrayColor,
            ))
        .toList();
    return Column(
      children: indicators,
    );
  }
}
