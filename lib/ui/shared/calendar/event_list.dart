import 'package:lesson_scheduling_pga/logic/models/private_lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/utils/date_utils.dart';

class EventList {
  Map<DateTime, List<PrivateLesson>> privateLessons = Map();
  Map<Program, List<DatePair>> selectedProgramRanges = Map();

  void add(DateTime date, PrivateLesson event) {
    if (!privateLessons.containsKey(date)) {
      privateLessons[date] = [event];
    } else {
      privateLessons[date].add(event);
    }
  }

  void addAll(DateTime date, List<PrivateLesson> lessons) {
    if (!privateLessons.containsKey(date)) {
      privateLessons[date] = lessons;
    } else {
      privateLessons[date].addAll(lessons);
    }
  }

  bool remove(DateTime date, PrivateLesson event) {
    return privateLessons.containsKey(date) ? privateLessons[date].remove(event) : true;
  }

  List<PrivateLesson> removeAll(DateTime date) {
    return privateLessons.containsKey(date) ? privateLessons.remove(date) : [];
  }

  void clear() {
    privateLessons?.clear();
  }

  void addPrograms(List<Program> programs) {
    selectedProgramRanges.clear();
    programs?.forEach((program) {
      var selectedRanges = List<DatePair>();
      program.startDates?.forEach((start) {
        selectedRanges.add(DatePair(start, start.add(Duration(days: program.durationDays - 1))));
      });
      selectedProgramRanges[program] = selectedRanges;
    });
  }

  void addPrivateLessons(List<PrivateLesson> lessons) {
    lessons.forEach((event) {
      add(DateTime(event.start.year, event.start.month, event.start.day), event);
    });
  }

  List<PrivateLesson> getEvents(DateTime date) {
    return privateLessons.containsKey(date) ? privateLessons[date] : [];
  }

  List<Program> getPrograms(DateTime date) {
    Set<Program> resultSet = Set();
    selectedProgramRanges?.forEach((program, ranges) {
      if (DateUtils.isInRanges(ranges, date)) {
        resultSet.add(program);
      }
    });
    return resultSet.toList();
  }

  List<PrivateLesson> getPrivateEventsForMonth(DateTime monthDate) {
    final startDate = DateTime(monthDate.year, monthDate.month);
    final endDate = DateTime(monthDate.year, monthDate.month + 1);
    List<PrivateLesson> resultList = [];
    privateLessons?.forEach((date, events) {
      if (date.isAfter(startDate) && date.isBefore(endDate)) {
        events.forEach((event) {
          resultList.add(event);
        });
      }
    });
    return resultList;
  }

  List<Program> getProgramsForMonth(DateTime monthDate) {
    Set<Program> resultSet = Set();
    selectedProgramRanges.forEach((program, ranges) {
      if (ranges.any((range) =>
          (range.start.year == monthDate.year &&
              range.start.month <= monthDate.month &&
              range.end.year == monthDate.year &&
              range.end.month >= monthDate.month) ||
          (range.start.year == monthDate.year &&
              range.start.month <= monthDate.month &&
              range.end.year > monthDate.year) ||
          (range.start.year < monthDate.year &&
              range.end.month >= monthDate.month &&
              range.end.year == monthDate.year))) {
        resultSet.add(program);
      }
    });
    return resultSet.toList();
  }
}
