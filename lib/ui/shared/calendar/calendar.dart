library flutter_calendar_dooboo;

import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'package:lesson_scheduling_pga/ui/shared/calendar/widgets/week_widget.dart';
import 'package:lesson_scheduling_pga/utils/date_utils.dart';

import 'event_list.dart';
import 'widgets/calendar_header.dart';
import 'widgets/month_widget.dart';
import 'widgets/weekday_row.dart';

const _kCurve = Curves.ease;

typedef DayWidgetBuilder = Widget Function(BuildContext context, DateTime day, EventList eventList);

class JumpTodayController extends ChangeNotifier {
  void jumpToToday() {
    notifyListeners();
  }
}

class Calendar<T> extends StatefulWidget {
  final height = double.infinity;
  final width = double.infinity;
  final Color dayButtonColor;
  final Color todayButtonColor;
  final List<DateTime> selectedDateTimes;
  final Color selectedDayButtonColor;
  final Color selectedDayTextColor;
  final Function(DateTime) onDayPressed;
  final Function onCalendarChanged;
  final Widget headerText;
  final EdgeInsets headerMargin;
  final double childAspectRatio;
  final EdgeInsets weekDayMargin;
  final bool weekFormat;
  final bool dayFormat;
  final bool showWeekDays;
  final bool showHeader;
  final bool showHeaderButton;
  final Widget leftButtonIcon;
  final Widget rightButtonIcon;
  final String locale;
  final int firstDayOfWeek;
  final DateTime minSelectedDate;
  final DateTime maxSelectedDate;
  final DateTime firstDate = DateTime(1960);
  final EventList eventList;
  final DayWidgetBuilder decoratorBuilder;
  final JumpTodayController controller;

  Calendar({
    this.eventList,
    this.dayButtonColor = Colors.transparent,
    this.todayButtonColor = Colors.grey,
    this.selectedDateTimes,
    this.selectedDayButtonColor = Colors.green,
    this.onDayPressed,
    this.headerText,
    this.headerMargin = const EdgeInsets.symmetric(vertical: 16.0),
    this.childAspectRatio = 1.15,
    this.weekDayMargin = const EdgeInsets.only(bottom: 4.0),
    this.showWeekDays = true,
    this.weekFormat = false,
    this.showHeader = true,
    this.showHeaderButton = true,
    this.leftButtonIcon,
    this.rightButtonIcon,
    this.onCalendarChanged,
    this.locale = "en",
    this.firstDayOfWeek,
    this.minSelectedDate,
    this.maxSelectedDate,
    this.selectedDayTextColor = Colors.black,
    this.decoratorBuilder,
    this.dayFormat = false,
    this.controller,
  });

  @override
  _CalendarState<T> createState() => _CalendarState<T>();

  static calendarHeight(BuildContext context, int rows, {double aspect = 1.15}) {
    final height = (MediaQuery.of(context).size.width / 7.0 * rows * 1.0 / aspect) +
        kCalendarHeaderHeight +
        kCalendarWeekdayHeight;
    return height;
  }
}

class _CalendarState<T> extends State<Calendar<T>> {
  PageController _controller;

  List<DateTime> _selectedDates = [];
  DateFormat _localeDate;
  int firstDayOfWeek;
  int todayIndex = 0;
  int currentIndex = 0;

  @override
  initState() {
    super.initState();
    initializeDateFormatting();
    if (widget.dayFormat) {
      todayIndex = _daysDelta(widget.firstDate, DateTime.now());
    } else {
      todayIndex = widget.weekFormat
          ? _weeksDelta(widget.firstDate, DateTime.now())
          : _monthDelta(widget.firstDate, DateTime.now());
    }

    currentIndex = todayIndex;
    _controller = PageController(initialPage: currentIndex);
    _localeDate = DateFormat.yMMM(widget.locale);
    if (widget.firstDayOfWeek == null)
      firstDayOfWeek = (_localeDate.dateSymbols.FIRSTDAYOFWEEK + 1) % 7;
    else
      firstDayOfWeek = widget.firstDayOfWeek;

    if (widget.selectedDateTimes != null) _selectedDates = widget.selectedDateTimes;

    widget.controller?.addListener(_onJumpToday);
  }

  _onJumpToday() {
    if (!widget.dayFormat && _controller.hasClients) {
      _controller.jumpToPage(todayIndex);
    } else if (widget.dayFormat) {
      setState(() {
        currentIndex = todayIndex;
        _notifyCalendarChanged();
      });
    }
  }

  @override
  dispose() {
    _controller.dispose();
    widget.controller?.removeListener(_onJumpToday);
    super.dispose();
  }

  _notifyCalendarChanged() {
    if (widget.onCalendarChanged != null) {
      if (widget.dayFormat) {
        widget.onCalendarChanged(
          widget.firstDate.add(Duration(days: currentIndex)),
        );
        return;
      }
      final result = widget.weekFormat
          ? _addWeeksToWeekDate(widget.firstDate, currentIndex)
          : _addMonthsToMonthDate(widget.firstDate, currentIndex);
      widget.onCalendarChanged(result);
    }
  }

  @override
  Widget build(BuildContext context) {
    _selectedDates = widget.selectedDateTimes;
    return Container(
      width: widget.width,
      height: widget.height,
      child: Column(
        children: <Widget>[
          _buildHeader(),
          widget.dayFormat ? Container() : _buildWeekDays(),
          widget.dayFormat
              ? Container()
              : Expanded(
                  child: PageView.builder(
                    onPageChanged: (index) {
                      setState(() => currentIndex = index);
                      _notifyCalendarChanged();
                    },
                    controller: _controller,
                    itemBuilder: (context, index) => widget.weekFormat ? weekBuilder(index) : monthBuilder(index),
                  ),
                ),
        ],
      ),
    );
  }

  Widget _buildHeader() {
    String title;
    String subTitle;
    if (widget.dayFormat) {
      final day = widget.firstDate.add(Duration(days: currentIndex));
      title = DateFormat('MMMM dd').format(day);
      subTitle = DateFormat.EEEE().format(day);
    } else if (widget.weekFormat) {
      final weekDays = _addWeeksToWeekDate(widget.firstDate, currentIndex);
      final sameMonth = weekDays.first.month == weekDays.last.month;
      final endDateFormat = sameMonth ? DateFormat('dd') : DateFormat('MMMM dd');
      title = DateFormat('MMMM dd').format(weekDays.first) + ' - ' + endDateFormat.format(weekDays.last);
      subTitle = weekDays.first.year.toString();
    } else {
      final month = _addMonthsToMonthDate(widget.firstDate, currentIndex);
      title = DateFormat.y().format(month);
      subTitle = DateFormat.MMMM().format(month);
    }

    return CalendarHeader(
      headerTitle: title,
      headerSubTitle: subTitle,
      onLeftButtonPressed: () => _prev(),
      onRightButtonPressed: () => _next(),
    );
  }

  Widget _buildWeekDays() {
    return WeekdayRow(
      firstDayOfWeek,
      weekdayMargin: widget.weekDayMargin,
      localeDate: _localeDate,
    );
  }

  Widget monthBuilder(int index) {
    final DateTime month = _addMonthsToMonthDate(widget.firstDate, index);
    double screenWidth = MediaQuery.of(context).size.width;
    return SizedBox(
      height: widget.height,
      width: screenWidth,
      child: MonthWidget(
        eventList: widget.eventList,
        monthDate: month,
        decoratorBuilder: widget.decoratorBuilder,
        onDayPressed: _onDayPressed,
        selectedDayButtonColor: widget.selectedDayButtonColor,
        selectedDayTextColor: widget.selectedDayTextColor,
        selectedDateTimes: _selectedDates,
        todayButtonColor: Color(0x66D8D8D8),
        dayButtonColor: Colors.white,
        childAspectRatio: widget.childAspectRatio,
        firstDayOfWeek: firstDayOfWeek,
        localeDate: _localeDate,
        maxSelectedDate: widget.maxSelectedDate,
        minSelectedDate: widget.minSelectedDate,
        thisMonthDayBorderColor: Color(0x99D8D8D8),
      ),
    );
  }

  Widget weekBuilder(int index) {
    final weekDays = _addWeeksToWeekDate(widget.firstDate, index);
    double screenWidth = MediaQuery.of(context).size.width;
    return SizedBox(
      height: widget.height,
      width: screenWidth,
      child: WeekWidget(
        eventList: widget.eventList,
        weekDays: weekDays,
        onDayPressed: _onDayPressed,
        selectedDayButtonColor: widget.selectedDayButtonColor,
        selectedDayTextColor: widget.selectedDayTextColor,
        selectedDateTimes: _selectedDates,
        todayButtonColor: Color(0x66D8D8D8),
        dayButtonColor: Colors.white,
        childAspectRatio: widget.childAspectRatio,
        decoratorBuilder: widget.decoratorBuilder,
        firstDayOfWeek: firstDayOfWeek,
        localeDate: _localeDate,
        maxSelectedDate: widget.maxSelectedDate,
        minSelectedDate: widget.minSelectedDate,
        thisMonthDayBorderColor: Color(0x99D8D8D8),
      ),
    );
  }

  DateTime _addMonthsToMonthDate(DateTime monthDate, int monthsToAdd) {
    return DateTime(monthDate.year + monthsToAdd ~/ 12, monthDate.month + monthsToAdd % 12);
  }

  List<DateTime> _addWeeksToWeekDate(DateTime weekDate, int weeksToAdd) {
    return getDaysInWeek(widget.firstDate.add(Duration(days: (weeksToAdd) * 7)));
  }

  _onDayPressed(DateTime picked) {
    if (picked == null) return;
    if (widget.minSelectedDate != null && picked.millisecondsSinceEpoch < widget.minSelectedDate.millisecondsSinceEpoch)
      return;
    if (widget.maxSelectedDate != null && picked.millisecondsSinceEpoch > widget.maxSelectedDate.millisecondsSinceEpoch)
      return;

    if (widget.onDayPressed != null) widget.onDayPressed(picked);
  }

  static int _monthDelta(DateTime startDate, DateTime endDate) {
    return (endDate.year - startDate.year) * 12 + endDate.month - startDate.month;
  }

  static int _daysDelta(DateTime startDate, DateTime endDate) {
    return endDate.difference(startDate).inDays;
  }

  static int _weeksDelta(DateTime startDate, DateTime endDate) {
    final date = endDate;
    final firstMonday = startDate.weekday;
    final daysInFirstWeek = 8 - firstMonday;
    final diff = date.difference(startDate);
    var weeks = ((diff.inDays - daysInFirstWeek) / 7).ceil();
    if (daysInFirstWeek > 3) {
      weeks += 1;
    }
    return weeks;
  }

  _next() {
    if (widget.dayFormat) {
      setState(() {
        currentIndex += 1;
        _notifyCalendarChanged();
      });
      return;
    }
    _controller.animateToPage(
      _controller.page.toInt() + 1,
      duration: Duration(milliseconds: 200),
      curve: _kCurve,
    );
  }

  _prev() {
    if (widget.dayFormat) {
      setState(() {
        currentIndex -= 1;
        _notifyCalendarChanged();
      });
      return;
    }
    _controller.animateToPage(
      _controller.page.toInt() - 1,
      duration: Duration(milliseconds: 200),
      curve: _kCurve,
    );
  }
}

List<DateTime> getDaysInWeek([DateTime selectedDate]) {
  if (selectedDate == null) selectedDate = new DateTime.now();

  var firstDayOfCurrentWeek = DateUtils.firstDayOfWeek(selectedDate);
  var lastDayOfCurrentWeek = DateUtils.lastDayOfWeek(selectedDate);
  return DateUtils.daysInRange(firstDayOfCurrentWeek, lastDayOfCurrentWeek).toList();
}
