import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../calendar.dart';
import '../event_list.dart';

class WeekWidget extends StatelessWidget {
  final List<DateTime> weekDays;
  final double childAspectRatio;
  final Color selectedDayButtonColor;
  final Color selectedDayTextColor;
  final Color todayButtonColor;
  final Color dayButtonColor;
  final DateTime minSelectedDate;
  final DateTime maxSelectedDate;
  final List<DateTime> selectedDateTimes;
  final Color thisMonthDayBorderColor;
  final int firstDayOfWeek;
  final DateFormat localeDate;
  final Function onDayPressed;
  final EventList eventList;
  final DayWidgetBuilder decoratorBuilder;

  const WeekWidget({
    Key key,
    this.weekDays,
    this.selectedDayButtonColor,
    this.todayButtonColor,
    this.dayButtonColor,
    this.minSelectedDate,
    this.maxSelectedDate,
    this.thisMonthDayBorderColor,
    this.selectedDateTimes,
    this.firstDayOfWeek,
    this.localeDate,
    this.onDayPressed,
    this.eventList,
    this.selectedDayTextColor,
    this.decoratorBuilder,
    this.childAspectRatio,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Positioned(
          child: Container(
            color: Colors.white,
            width: double.infinity,
            child: GridView.count(
              physics: NeverScrollableScrollPhysics(),
              mainAxisSpacing: 0,
              crossAxisCount: 7,
              childAspectRatio: childAspectRatio,
              children: List.generate(
                  7,
                  (index) => dayWidget(
                        context,
                        index,
                        weekDays[index],
                      )),
            ),
          ),
        ),
      ],
    );
  }

  Widget dayWidget(BuildContext context, int index, DateTime date) {
    final nowDay = DateTime.now();
    bool isToday = nowDay.day == date.day && nowDay.month == date.month && nowDay.year == date.year;
    bool isSelectedDay = selectedDateTimes != null &&
        selectedDateTimes.any((selected) =>
            selected != null && selected.year == date.year && selected.month == date.month && selected.day == date.day);

    DateTime now = DateTime(date.year, date.month, date.day);
    TextStyle textStyle = _daysTextStyle(Colors.black);

    bool isSelectable = true;
    if (minSelectedDate != null && now.millisecondsSinceEpoch < minSelectedDate.millisecondsSinceEpoch)
      isSelectable = false;
    else if (maxSelectedDate != null && now.millisecondsSinceEpoch > maxSelectedDate.millisecondsSinceEpoch)
      isSelectable = false;

    TextStyle style;
    if ((index % 7 == 0 || index % 7 == 6) && !isSelectedDay && !isToday) {
      style = (isSelectable ? textStyle : _daysTextStyle(Colors.black38));
    } else {
      style = isToday ? _daysTextStyle(Colors.black) : isSelectable ? textStyle : _daysTextStyle(Colors.black38);
    }
    if (isSelectedDay) {
      style = _daysTextStyle(selectedDayTextColor);
    }

    return Stack(
      children: <Widget>[
        Positioned.fill(
          child: Container(
            color: isSelectedDay ? selectedDayButtonColor : (isToday ? todayButtonColor : dayButtonColor),
          ),
        ),
        Positioned.fill(
            child: Container(
          decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.all(
              width: 0.5,
              color: thisMonthDayBorderColor,
            ),
            shape: BoxShape.rectangle,
          ),
        )),
        Positioned.fill(
          child: decoratorBuilder != null ? decoratorBuilder(context, now, eventList) : Container(),
        ),
        Positioned.fill(
          child: FlatButton(
            onPressed: () => onDayPressed(now),
            padding: EdgeInsets.all(2.0),
            child: Padding(
              padding: const EdgeInsets.only(bottom: 16.0),
              child: Text(
                '${now.day}',
                style: style,
                maxLines: 1,
              ),
            ),
          ),
        ),
      ],
    );
  }

  _daysTextStyle(Color color) => TextStyle(
        color: color,
        fontSize: 12.0,
      );
}
