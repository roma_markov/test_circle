import 'package:flutter/material.dart';

import '../../style.dart';

double kCalendarHeaderHeight = 50;

class CalendarHeader extends StatelessWidget {
  final String headerTitle;
  final String headerSubTitle;
  final VoidCallback onLeftButtonPressed;
  final VoidCallback onRightButtonPressed;

  CalendarHeader({
    @required this.headerTitle,
    @required this.headerSubTitle,
    @required this.onLeftButtonPressed,
    @required this.onRightButtonPressed,
  });

  Widget _leftButton() => IconButton(
        onPressed: onLeftButtonPressed,
        icon: Icon(Icons.chevron_left, color: inputHintColor),
      );

  Widget _rightButton() => IconButton(
        onPressed: onRightButtonPressed,
        icon: Icon(Icons.chevron_right, color: inputHintColor),
      );

  @override
  Widget build(BuildContext context) {
    return Container(
      height: kCalendarHeaderHeight,
      color: appBackgroundColor,
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
        _leftButton(),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(headerTitle, style: todoDownHintStyle),
            Text(headerSubTitle, style: calendarSubTextStyle),
          ],
        ),
        _rightButton(),
      ]),
    );
  }
}
