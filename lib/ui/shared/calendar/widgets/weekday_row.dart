import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../style.dart';

final List<String> weekDayTitles = ['S', 'M', 'T', 'W', 'TH', 'F', 'S'];

double kCalendarWeekdayHeight = 40;
class WeekdayRow extends StatelessWidget {
  WeekdayRow(
    this.firstDayOfWeek, {
    @required this.weekdayMargin,
    @required this.localeDate,
  });

  final EdgeInsets weekdayMargin;
  final DateFormat localeDate;
  final int firstDayOfWeek;

  Widget _weekdayContainer(String weekDay) => Expanded(
          child: Container(
        color: Colors.white,
        height: 40,
        child: Center(
          child: DefaultTextStyle(
            style: defaultWeekdayTextStyle,
            child: Text(
              weekDay,
              style: defaultWeekdayTextStyle,
            ),
          ),
        ),
      ));

  List<Widget> _renderWeekDays() {
    List<Widget> list = [];
    for (var i = firstDayOfWeek, count = 0; count < 7; i = (i + 1) % 7, count++) {
      list.add(_weekdayContainer(weekDayTitles[i]));
    }
    return list;
  }

  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: _renderWeekDays(),
      );
}
