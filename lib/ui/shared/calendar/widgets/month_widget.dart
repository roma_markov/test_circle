import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../calendar.dart';
import '../event_list.dart';

class MonthWidget extends StatelessWidget {
  final DateTime monthDate;
  final double childAspectRatio;
  final Color selectedDayButtonColor;
  final Color selectedDayTextColor;
  final Color todayButtonColor;
  final Color dayButtonColor;
  final DateTime minSelectedDate;
  final DateTime maxSelectedDate;
  final List<DateTime> selectedDateTimes;
  final Color thisMonthDayBorderColor;
  final int firstDayOfWeek;
  final DateFormat localeDate;
  final Function onDayPressed;
  final EventList eventList;
  final DayWidgetBuilder decoratorBuilder;

  const MonthWidget({
    Key key,
    this.monthDate,
    this.selectedDayButtonColor,
    this.todayButtonColor,
    this.dayButtonColor,
    this.minSelectedDate,
    this.maxSelectedDate,
    this.thisMonthDayBorderColor,
    this.selectedDateTimes,
    this.firstDayOfWeek,
    this.localeDate,
    this.onDayPressed,
    this.eventList,
    this.selectedDayTextColor,
    this.decoratorBuilder,
    this.childAspectRatio,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DateTime date1 = DateTime(this.monthDate.year, this.monthDate.month, 1);
    DateTime date2 = DateTime(this.monthDate.year, this.monthDate.month + 1, 1);
    int startWeekday = date1.weekday - firstDayOfWeek;
    int endWeekday = date2.weekday - firstDayOfWeek;
    int totalItemCount = 42;
    int year = monthDate.year;
    int month = monthDate.month;
    return Stack(
      children: <Widget>[
        Positioned(
          child: Container(
            color: Colors.white,
            width: double.infinity,
            child: GridView.count(
              physics: NeverScrollableScrollPhysics(),
              mainAxisSpacing: 0,
              crossAxisCount: 7,
              childAspectRatio: childAspectRatio,
              children: List.generate(
                  totalItemCount,
                  (index) => dayWidget(
                        context,
                        index,
                        year,
                        month,
                        startWeekday,
                        endWeekday,
                      )),
            ),
          ),
        ),
      ],
    );
  }

  Widget dayWidget(BuildContext context, int index, int year, int month, int startWeekday, int endWeekday) {
    final nowDay = DateTime.now();
    bool isToday = nowDay.day == index + 1 - startWeekday && nowDay.month == month && nowDay.year == year;
    bool isPrevMonthDay = index < startWeekday;
    bool isNextMonthDay = index >= (DateTime(year, month + 1, 0).day) + startWeekday;
    bool isThisMonthDay = !isPrevMonthDay && !isNextMonthDay;

    DateTime now = DateTime(year, month, 1);
    TextStyle textStyle;
    if (isPrevMonthDay) {
      now = now.subtract(Duration(days: startWeekday - index));
      textStyle = _daysTextStyle(Colors.black38);
    } else if (isThisMonthDay) {
      now = DateTime(year, month, index + 1 - startWeekday);
      textStyle = _daysTextStyle(Colors.black);
    } else {
      now = DateTime(year, month, index + 1 - startWeekday);
      textStyle = _daysTextStyle(Colors.black38);
    }

    bool isSelectedDay = selectedDateTimes != null &&
        selectedDateTimes.isNotEmpty &&
        selectedDateTimes.any((selected) =>
            selected != null && selected.year == now.year && selected.month == now.month && selected.day == now.day);

    bool isSelectable = true;
    if (minSelectedDate != null && now.millisecondsSinceEpoch < minSelectedDate.millisecondsSinceEpoch)
      isSelectable = false;
    else if (maxSelectedDate != null && now.millisecondsSinceEpoch > maxSelectedDate.millisecondsSinceEpoch)
      isSelectable = false;

    TextStyle style = (localeDate.dateSymbols.WEEKENDRANGE.contains((index - 1 + firstDayOfWeek) % 7)) &&
            !isSelectedDay &&
            !isToday
        ? (isPrevMonthDay ? _daysTextStyle(Colors.black38) : isSelectable ? textStyle : _daysTextStyle(Colors.black38))
        : isToday ? _daysTextStyle(Colors.black) : isSelectable ? textStyle : _daysTextStyle(Colors.black38);
    if (isSelectedDay) {
      style = _daysTextStyle(selectedDayTextColor);
    }

    return Stack(
      children: <Widget>[
        Positioned.fill(
          child: Container(
            color: isSelectedDay ? selectedDayButtonColor : (isToday ? todayButtonColor : dayButtonColor),
          ),
        ),
        Positioned.fill(
            child: Container(
          decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.all(
              width: 0.5,
              color: thisMonthDayBorderColor,
            ),
            shape: BoxShape.rectangle,
          ),
        )),
        Positioned.fill(
          child: decoratorBuilder != null ? decoratorBuilder(context, now, eventList) : Container(),
        ),
        Positioned.fill(
          child: FlatButton(
            onPressed: () => onDayPressed(now),
            padding: EdgeInsets.all(2.0),
            child: Padding(
              padding: const EdgeInsets.only(bottom: 16.0),
              child: Text(
                '${now.day}',
                style: style,
                maxLines: 1,
              ),
            ),
          ),
        ),
      ],
    );
  }

  _daysTextStyle(Color color) => TextStyle(
        color: color,
        fontSize: 12.0,
      );
}
