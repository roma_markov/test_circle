import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class PortraitMode extends StatelessWidget {
  final Widget app;

  PortraitMode({@required this.app});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return app;
  }
}
