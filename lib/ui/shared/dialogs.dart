import 'package:flutter/material.dart';

import '../index.dart';

Future<T> showConfirmDialog<T>(BuildContext context, String message,
    {String confirmTitle, String cancelTitle, VoidCallback confirmationCallback}) {
  return showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text(message),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              cancelTitle ?? 'CANCEL',
            ),
          ),
          FlatButton(
            onPressed: () {
              if (confirmationCallback != null) {
                confirmationCallback();
              }
              Navigator.of(context).pop();
            },
            child: Text(
              confirmTitle ?? 'OK',
            ),
          ),
        ],
      );
    },
  );
}

Future<T> showInfoDialog<T>(BuildContext context, String message, {VoidCallback callback}) {
  return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(message),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                Navigator.of(context).pop();
                if (callback != null) {
                  callback();
                }
              },
              child: Text('OK'),
            ),
          ],
        );
      });
}

typedef void DialogCompletion<T>(List<T> selected);

class SelectListDialog<T> extends StatefulWidget {
  final List<T> items;
  final List<T> selectedItems;
  final DialogCompletion<T> completion;
  final String title;

  SelectListDialog({
    @required this.items,
    @required this.selectedItems,
    @required this.completion,
    @required this.title,
  });

  @override
  State<StatefulWidget> createState() => _SelectListDialogState();
}

class _SelectListDialogState<T> extends State<SelectListDialog> {
  List<T> _selectedItems = [];

  @override
  void initState() {
    super.initState();
    _selectedItems = widget.selectedItems;
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      titlePadding: EdgeInsets.zero,
      contentPadding: EdgeInsets.zero,
      children: [
        Material(
          elevation: 2,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 24),
            height: 56,
            alignment: Alignment.centerLeft,
            child: Text(
              widget.title,
              style: appBarLightTextStyle,
            ),
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width * 0.7,
          height: 224,
          child: ListView.builder(
              itemCount: widget.items.length,
              itemBuilder: (context, position) {
                final item = widget.items[position];
                return CheckboxListTile(
                  title: Text(
                    item.name,
                    maxLines: 1,
                    style: fieldTextStyle(),
                  ),
                  activeColor: appGreenColor,
                  value: _selectedItems.contains(item),
                  onChanged: (checked) => setState(() {
                    (checked) ? _selectedItems.add(item) : _selectedItems.remove(item);
                  }),
                );
              }),
        ),
        Container(
          color: const Color(0xFF000000).withAlpha(12),
          height: 1,
        ),
        Container(
          height: 52,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                child: Text(
                  'CANCEL',
                  style: appBarLightTextStyle,
                ),
                onPressed: () => Navigator.of(context).pop(),
              ),
              FlatButton(
                child: Text(
                  'OK',
                  style: appBarLightTextStyle,
                ),
                onPressed: () {
                  widget.completion(_selectedItems);
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        )
      ],
    );
  }
}
