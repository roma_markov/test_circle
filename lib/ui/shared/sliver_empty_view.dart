import 'package:flutter/cupertino.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

class SliverEmptyView extends StatelessWidget {
  final String text;
  final Color textColor;

  const SliverEmptyView({
    Key key,
    @required this.text,
    this.textColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _wrapSliverList([
      EmptyView(
        text: text,
        textColor: textColor,
      )
    ]);
  }

  _wrapSliverList(List<Widget> children) {
    return SliverList(
      delegate: SliverChildListDelegate(children),
    );
  }
}

class EmptyView extends StatelessWidget {
  final String text;
  final Color textColor;

  const EmptyView({
    Key key,
    @required this.text,
    this.textColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _sectionHeader(text);
  }

  _sectionHeader(String text) {
    return Container(
      height: 52,
      padding: EdgeInsets.symmetric(horizontal: 18),
      alignment: Alignment.centerLeft,
      child: Text(
        text,
        style: summarySectionTextStyle(color: textColor),
      ),
    );
  }
}
