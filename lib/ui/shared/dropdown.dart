import 'package:flutter/material.dart';

import 'style.dart';

typedef void SelectedDropdownItem(int index);
typedef TitleBuilder = String Function(BuildContext context, dynamic item);

class Dropdown extends StatefulWidget {
  final List<dynamic> items;
  final SelectedDropdownItem callback;
  final dynamic defaultValue;
  final String hint;
  final bool isEnabled;
  final TitleBuilder builder;

  Dropdown({
    this.items,
    this.defaultValue,
    this.callback,
    this.hint,
    this.isEnabled = true,
    this.builder,
  });

  @override
  State<StatefulWidget> createState() => _DropdownState();
}

class _DropdownState extends State<Dropdown> {
  dynamic selected;

  @override
  void initState() {
    super.initState();
    selected = widget.defaultValue;
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(canvasColor: Colors.white),
      child: DropdownButtonFormField(
        decoration: fieldInputDecoration(
          widget.hint,
          color: const Color(0xFFDDDDDD),
        ),
        value: selected,
        onChanged: (value) => setState(() {
          selected = value;
          widget.callback(widget.items.indexOf(value));
        }),
        items: widget.isEnabled
            ? List<DropdownMenuItem>.generate(widget.items.length, (index) {
                final item = widget.items[index];
                return DropdownMenuItem(
                  value: item,
                  child: Text(
                    (widget.builder == null)
                        ? item.toString()
                        : widget.builder(
                            context,
                            item,
                          ),
                    style: dropDownStyle,
                  ),
                );
              })
            : null,
      ),
    );
  }
}
