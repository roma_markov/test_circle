import 'package:flutter/material.dart';

class SlideUpRoute extends PageRoute {
  final Widget page;

  static final Tween<Offset> _bottomUpTween = Tween<Offset>(begin: const Offset(0.0, 1.0), end: Offset.zero);
  static final Tween<Offset> _bottomUpSecondaryTween = Tween<Offset>(begin: Offset.zero, end: const Offset(0.0, -0.15));

  SlideUpRoute(RouteSettings settings, this.page) : super(settings: settings, fullscreenDialog: true);

  Widget _transitionBuilder(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    final positionAnimation = CurvedAnimation(parent: animation, curve: Curves.fastOutSlowIn);
    final positionSecondaryAnimation = CurvedAnimation(parent: secondaryAnimation, curve: Curves.fastOutSlowIn);
    return SlideTransition(
      transformHitTests: false,
      position: _bottomUpTween.animate(positionAnimation),
      child: SlideTransition(
        transformHitTests: false,
        position: _bottomUpSecondaryTween.animate(positionSecondaryAnimation),
        child: child,
      ),
    );
  }

  @override
  bool canTransitionTo(TransitionRoute nextRoute) {
    return nextRoute is SlideUpRoute;
  }

  @override
  bool canTransitionFrom(TransitionRoute previousRoute) {
    return previousRoute is SlideUpRoute;
  }

  @override
  Color get barrierColor => null;

  @override
  String get barrierLabel => null;

  @override
  Widget buildPage(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
    return _transitionBuilder(context, animation, secondaryAnimation, page);
  }

  @override
  bool get maintainState => true;

  @override
  Duration get transitionDuration => const Duration(milliseconds: 300);
}
