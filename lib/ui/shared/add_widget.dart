import 'package:flutter/material.dart';

import 'style.dart';

class AddWidget extends StatefulWidget {
  final ValueChanged<String> onChanged;
  final VoidCallback onCreatePressed;
  final String hint;
  final bool enabled;
  final TextEditingController searchController;

  const AddWidget({
    Key key,
    @required this.onChanged,
    @required this.hint,
    @required this.searchController,
    @required this.onCreatePressed,
    @required this.enabled,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _AddWidgetState();
  }
}

class _AddWidgetState extends State<AddWidget> {
  bool showClear = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 16, right: 16, top: 8),
      child: TextField(
        enabled: widget.enabled,
        style: searchInputStyle,
        textCapitalization: TextCapitalization.sentences,
        controller: widget.searchController,
        decoration: searchInputDecoration(
          widget.hint,
          showClear,
          () => widget.onCreatePressed(),
          icon: Icons.place,
          rightIcon: Icons.add_circle_outline,
        ),
        onChanged: _onChanged,
      ),
    );
  }

  _onChanged(String value) {
    widget.onChanged(value);
    if (showClear != value.isNotEmpty) {
      setState(() => showClear = value.isNotEmpty);
    }
  }
}
