import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';

import '../index.dart';

typedef ButtonPressed = void Function(BuildContext context);
enum AppButtonStyle { green, gray }

final kBottomButtonHeight = 60.0;

class AppButton extends StatelessWidget {
  final bool enabled;
  final ButtonPressed onPressed;
  final String title;
  final AppButtonStyle style;

  const AppButton({
    Key key,
    @required this.title,
    @required this.onPressed,
    this.enabled = true,
    this.style = AppButtonStyle.green,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: kBottomButtonHeight,
      child: FlatButton(
        color: appGreenColor,
        disabledColor: appGrayColor,
        shape: RoundedRectangleBorder(),
        child: Text(
          title,
          style: buttonTextStyle,
        ),
        onPressed: enabled ? () => onPressed(context) : null,
      ),
    );
  }
}

class AppButtonGrey extends StatelessWidget {
  final bool enabled;
  final ButtonPressed onPressed;
  final String title;
  final IconData iconData;
  final Color color;
  final Color textColor;

  const AppButtonGrey({
    Key key,
    @required this.title,
    @required this.onPressed,
    this.enabled = true,
    this.iconData,
    this.color,
    this.textColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: kBottomButtonHeight,
      child: FlatButton(
        color: color ?? lightAppBarColor,
        shape: RoundedRectangleBorder(),
        disabledColor: color?.withOpacity(0.5) ?? lightAppBarColor.withOpacity(0.5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            iconData != null
                ? Padding(
                    padding: const EdgeInsets.only(right: 12.0),
                    child: Icon(
                      Icons.edit,
                      color: textColor ?? inputHintColor,
                    ),
                  )
                : Container(),
            Text(
              title,
              style: TextStyle(
                fontFamily: fontRobotoBold,
                fontSize: 14,
                color: textColor ?? inputHintColor,
              ),
            ),
          ],
        ),
        onPressed: enabled ? () => onPressed(context) : null,
      ),
    );
  }
}

class AppButtonDashed extends StatelessWidget {
  final bool enabled;
  final ButtonPressed onPressed;
  final String title;
  final AppButtonStyle style;
  final IconData iconData;

  const AppButtonDashed({
    Key key,
    @required this.title,
    @required this.onPressed,
    this.enabled = true,
    this.iconData,
    this.style = AppButtonStyle.green,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DottedBorder(
      padding: EdgeInsets.zero,
      color: appBlackColor.withOpacity(enabled ? 0.2 : 0.1),
      dashPattern: [10, 10],
      child: SizedBox(
        height: 60,
        child: FlatButton(
          color: Colors.transparent,
          padding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(),
          disabledColor: Colors.transparent,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              iconData != null
                  ? Padding(
                      padding: const EdgeInsets.only(right: 12.0),
                      child: Icon(
                        iconData,
                        color: enabled ? inputHintColor : inputHintColor.withOpacity(0.1),
                      ),
                    )
                  : Container(),
              Text(
                title,
                style: TextStyle(
                  fontFamily: fontRobotoBold,
                  fontSize: 14,
                  color: enabled ? inputHintColor : inputHintColor.withOpacity(0.1),
                ),
              ),
            ],
          ),
          onPressed: enabled ? () => onPressed(context) : null,
        ),
      ),
    );
  }
}
