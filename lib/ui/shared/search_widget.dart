import 'package:flutter/material.dart';

import 'style.dart';

class SearchWidget extends StatefulWidget {
  final ValueChanged<String> onChanged;
  final String hint;
  final double bottomPadding;
  final TextEditingController searchController;

  const SearchWidget({
    Key key,
    @required this.onChanged,
    @required this.hint,
    @required this.searchController,
    this.bottomPadding = 16,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _SearchWidgetState();
  }
}

class _SearchWidgetState extends State<SearchWidget> {
  bool showClear = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 16, right: 16, bottom: widget.bottomPadding, top: 8),
      child: TextField(
        style: searchInputStyle,
        controller: widget.searchController,
        decoration: searchInputDecoration(
          widget.hint,
          showClear,
          () => WidgetsBinding.instance.addPostFrameCallback((_) => _clearSearch()),
        ),
        onChanged: _onChanged,
      ),
    );
  }

  _onChanged(String value) {
    widget.onChanged(value);
    if (showClear != value.isNotEmpty) {
      setState(() => showClear = value.isNotEmpty);
    }
  }

  _clearSearch() {
    widget.searchController.clear();
    widget.onChanged('');
    setState(() => showClear = false);
  }
}
