import 'package:flutter/cupertino.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

final kSectionHeight = 52.0;
class SliverSectionHeader extends StatelessWidget {
  final String text;
  final Color textColor;
  final Color backgroundColor;

  const SliverSectionHeader({
    Key key,
    @required this.text,
    this.textColor,
    this.backgroundColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: SectionHeader(
        text: text,
        textColor: textColor,
        backgroundColor: backgroundColor,
      ),
    );
  }
}

class SectionHeader extends StatelessWidget {
  final String text;
  final Color textColor;
  final Color backgroundColor;

  const SectionHeader({
    Key key,
    @required this.text,
    this.textColor,
    this.backgroundColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _sectionHeader(text);
  }

  _sectionHeader(String text) {
    return Container(
      height: kSectionHeight,
      padding: EdgeInsets.symmetric(horizontal: 18),
      alignment: Alignment.centerLeft,
      color: backgroundColor ?? appBackgroundColor,
      child: Text(
        text,
        style: summarySectionTextStyle(color: textColor),
      ),
    );
  }
}
