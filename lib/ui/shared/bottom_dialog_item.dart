import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

final kBottomItemHeight = 56.0;
final kBottomDialogPadding = 16.0;
class BottomDialogItem extends StatelessWidget {
  final String title;
  final IconData icon;
  final VoidCallback onPressed;

  const BottomDialogItem(
    this.title,
    this.icon,
    this.onPressed, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onPressed(),
      child: Container(
        height: kBottomItemHeight,
        child: Row(
          children: <Widget>[
            Container(
              width: 72,
              child: Icon(
                icon,
                color: inputHintColor,
              ),
            ),
            Text(
              title,
              style: lightButtonTextStyle,
            )
          ],
        ),
      ),
    );
  }
}
