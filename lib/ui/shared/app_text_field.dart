import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../index.dart';

class AppTextField extends StatefulWidget {
  final TextEditingController controller;
  final String hint;
  final bool isPassword;
  final bool isMultiline;
  final bool isEnabled;
  final ValueChanged<String> onFieldSubmitted;
  final FormFieldValidator<String> validator;
  final TextInputAction textInputAction;
  final TextInputType keyboardType;
  final TextInputFormatter formatter;
  final TextCapitalization textCapitalization;

  const AppTextField({
    Key key,
    this.controller,
    this.hint,
    this.isPassword = false,
    this.isMultiline = false,
    this.onFieldSubmitted,
    this.textInputAction,
    this.keyboardType,
    this.formatter,
    this.isEnabled = true,
    this.validator,
    this.textCapitalization = TextCapitalization.none,
  }) : super(key: key);

  @override
  _AppTextFieldState createState() => _AppTextFieldState();
}

class _AppTextFieldState extends State<AppTextField> {
  bool showPassword = false;

  @override
  Widget build(BuildContext context) {
    return !widget.isPassword ? _textField() : _passwordField();
  }

  _textField() {
    return TextFormField(
      controller: widget.controller,
      style: fieldTextStyle(),
      keyboardType: widget.keyboardType,
      onFieldSubmitted: widget.onFieldSubmitted,
      validator: widget.validator,
      maxLines: widget.isMultiline ? null : 1,
      enabled: widget.isEnabled,
      textCapitalization: widget.textCapitalization,
      minLines: widget.isMultiline ? 4 : 1,
      textInputAction: widget.textInputAction,
      inputFormatters: widget.formatter != null ? <TextInputFormatter>[widget.formatter] : null,
      decoration: fieldInputDecoration(
        widget.hint,
        color: const Color(0xFFDDDDDD),
      ),
    );
  }

  _passwordField() {
    return TextFormField(
      obscureText: !showPassword,
      controller: widget.controller,
      textInputAction: widget.textInputAction,
      style: fieldTextStyle(),
      onFieldSubmitted: widget.onFieldSubmitted,
      decoration: fieldInputDecoration(
        widget.hint,
        color: const Color(0xFFDDDDDD),
        suffix: FlatButton(
          onPressed: () => setState(() => showPassword = !showPassword),
          child: Icon(
            showPassword ? Icons.visibility : Icons.visibility_off,
            color: appBlackColor,
          ),
        ),
      ),
    );
  }
}
