import '../../platform/mobile.dart';

const appBackgroundColor = Color(0xFFF6F6F6);
const appNavigationColor = Color(0xFF4F4F4F);
const appGreenColor = Color(0xFF36D284);
const appGrayColor = Color(0xFFDBDBDB);
const appYellowColor = Color(0xFFFFCB52);
const appRedColor = Color(0xFFAE2C2C);
const appWarningColor = Color(0xFFba4343);
const appWhiteColor = Color(0xFFFFFFFF);
const appLightWhiteColor = Color(0xDEFFFFFF);
const appBlackColor = Color(0xFF000000);
const inputHintColor = Color(0x66000000);
const placeInputHintColor = Color(0x33000000);
const drawerItemTextColor = Color(0x99000000);
const lightAppBarColor = Color(0xFFF6F6F6);
const lightGrayColor = Color(0xFFD8D8D8);
const approvedColor = Color(0xFF5EB700);
const dividerColor = Color(0xFFDDDDDD);

const lessonColors = [
  Color(0xFFFF802B),
  Color(0xFF52B5FF),
  Color(0xFFFFCB52),
  Color(0xFF6EC594),
  Color(0xFFA467FF),
  Color(0xFF37DEDE),
  Color(0xFFFF8FE1),
  Color(0xFFB8DE37),
  Color(0xFFD2DC8F)
];

Color colorById(int id) {
  return lessonColors[(id) % lessonColors.length];
}

const fontRobotoBold = 'RobotoBold';
const fontRobotoRegular = 'Roboto';
const fontRobotoMedium = 'Roboto';
const fontMerriweatherBold = 'MerriweatherBold';

const TextStyle buttonTextStyle = TextStyle(
  fontFamily: fontRobotoBold,
  fontSize: 14,
  color: appWhiteColor,
);

const TextStyle appBarLightTextStyle = TextStyle(
  fontFamily: fontRobotoBold,
  fontSize: 14,
  color: inputHintColor,
);

TextStyle mwTextStyle({Color color, double fontSize}) => TextStyle(
      fontFamily: fontMerriweatherBold,
      fontSize: fontSize ?? 14,
      color: color ?? appBlackColor,
    );

TextStyle switchButtonTextStyle(Color color) => TextStyle(
      fontFamily: fontRobotoRegular,
      fontSize: 14,
      color: color,
    );

const TextStyle appBarLightSubTextStyle = TextStyle(
  fontFamily: fontRobotoBold,
  fontSize: 12,
  color: inputHintColor,
);

TextStyle summarySectionTextStyle({Color color}) => TextStyle(
      fontFamily: fontRobotoBold,
      fontSize: 12,
      color: color ?? appBlackColor,
    );

const TextStyle warningTextStyle = TextStyle(
  fontFamily: fontRobotoBold,
  fontSize: 12,
  color: appRedColor,
);

const TextStyle approvedTextStyle = TextStyle(
  fontFamily: fontRobotoBold,
  fontSize: 12,
  color: approvedColor,
);

const TextStyle appBarDarkTextStyle = TextStyle(
  fontFamily: fontRobotoBold,
  fontSize: 14,
  color: appWhiteColor,
);

const TextStyle lightButtonTextStyle = TextStyle(
  fontFamily: fontRobotoBold,
  fontSize: 14,
  color: inputHintColor,
);
const TextStyle lessonItemNameStyle = TextStyle(
  fontFamily: fontMerriweatherBold,
  fontSize: 16,
  color: appBlackColor,
);

const TextStyle lessonItemGoalStyle = TextStyle(
  fontFamily: fontRobotoRegular,
  fontSize: 12,
  color: inputHintColor,
);

const TextStyle lessonNameTextStyle = TextStyle(
  fontFamily: fontMerriweatherBold,
  fontSize: 20,
  color: appBlackColor,
);

const TextStyle drawerHeaderTextStyle = TextStyle(
  fontFamily: fontMerriweatherBold,
  fontSize: 20,
  color: appWhiteColor,
);

const TextStyle drawerItemTextStyle = TextStyle(
  fontFamily: fontRobotoRegular,
  fontSize: 12,
  color: drawerItemTextColor,
);

TextStyle hintTextStyle({Color color = inputHintColor}) => TextStyle(
      fontFamily: fontRobotoRegular,
      fontSize: 16,
      color: color,
    );

TextStyle fieldTextStyle({Color color = appBlackColor}) => TextStyle(
      fontFamily: fontRobotoRegular,
      fontSize: 16,
      color: color,
    );

const TextStyle dropDownStyle = TextStyle(
  fontFamily: fontRobotoRegular,
  fontSize: 16,
  color: appBlackColor,
);

const TextStyle todoDownHintStyle = TextStyle(
  fontFamily: fontRobotoRegular,
  fontSize: 14,
  color: appBlackColor,
);

const TextStyle calendarSubTextStyle = TextStyle(
  fontFamily: fontRobotoRegular,
  fontSize: 12,
  color: inputHintColor,
);

const TextStyle searchPlaceholderStyle = TextStyle(
  fontFamily: fontRobotoRegular,
  fontSize: 14,
  color: inputHintColor,
);

const TextStyle searchInputStyle = TextStyle(
  fontFamily: fontRobotoRegular,
  fontSize: 14,
  color: appBlackColor,
);

const TextStyle lessonLabelTextStyle = TextStyle(
  fontFamily: fontRobotoBold,
  fontSize: 12,
  color: inputHintColor,
);

const TextStyle studentCreateTextStyle = TextStyle(
  fontFamily: fontRobotoBold,
  fontSize: 12,
  color: appBlackColor,
);

const TextStyle studentDescTextStyle = TextStyle(
  fontFamily: fontRobotoRegular,
  fontSize: 12,
  color: appBlackColor,
);

const TextStyle lessonTextStyle = TextStyle(
  fontFamily: fontRobotoRegular,
  fontSize: 16,
  color: appBlackColor,
);

const TextStyle lessonWarningTextStyle = TextStyle(
  fontFamily: fontRobotoRegular,
  fontSize: 16,
  color: appRedColor,
);

const TextStyle lessonHintTextStyle = TextStyle(
  fontFamily: fontRobotoRegular,
  fontSize: 16,
  color: inputHintColor,
);

const TextStyle defaultWeekdayTextStyle = TextStyle(
  fontFamily: fontRobotoBold,
  fontSize: 14,
  color: appBlackColor,
);

InputDecoration searchInputDecoration(
  String hint,
  bool showClear,
  VoidCallback clear, {
  IconData icon = Icons.search,
  IconData rightIcon = Icons.close,
}) {
  return InputDecoration(
    hintStyle: searchPlaceholderStyle,
    hintText: hint,
    border: UnderlineInputBorder(),
    prefixIcon: IconTheme(
      data: IconThemeData(color: inputHintColor),
      child: Padding(
        padding: const EdgeInsets.only(bottom: 6.0),
        child: Icon(
          icon,
          size: 28,
        ),
      ),
    ),
    suffixIcon: showClear
        ? IconButton(
            onPressed: () => clear(),
            icon: Icon(rightIcon),
          )
        : null,
  );
}

InputDecoration fieldInputDecoration(
  String labelText, {
  @required color,
  Widget prefix,
  Widget suffix,
  String counterText,
}) =>
    InputDecoration(
      hintStyle: hintTextStyle(),
      hintText: labelText,
      contentPadding: EdgeInsets.all(24),
      errorStyle: warningTextStyle,
      prefix: prefix,
      suffixIcon: suffix,
      enabledBorder: inputBorder(color),
      focusedBorder: inputBorder(color),
      errorBorder: inputBorder(appRedColor),
      focusedErrorBorder: inputBorder(appRedColor),
      counterText: counterText ?? null,
    );

OutlineInputBorder inputBorder(Color color) => OutlineInputBorder(
      borderSide: BorderSide(color: color),
      borderRadius: BorderRadius.all(Radius.circular(0)),
    );

InputDecoration placeAddInputDecoration(String hint, bool showClear, VoidCallback clear) => InputDecoration(
      hintStyle: TextStyle(
        fontFamily: fontRobotoBold,
        fontSize: 14,
        color: placeInputHintColor,
      ),
      hintText: hint,
      focusColor: placeInputHintColor,
      fillColor: placeInputHintColor,
      border: UnderlineInputBorder(),
      suffixIcon: showClear
          ? IconButton(
              onPressed: () => clear(),
              icon: Icon(Icons.close),
            )
          : null,
    );
