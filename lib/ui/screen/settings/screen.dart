import 'package:flutter/widgets.dart';
import 'package:package_info/package_info.dart';

class SettingsScreen extends StatefulWidget {
  static const String route = '/SettingsScreen';

  SettingsScreen._init();

  factory SettingsScreen.fromArgs(Map<String, Object> args) {
    return SettingsScreen._init();
  }

  static Future push(BuildContext context) {
    return Navigator.of(context).pushNamed(SettingsScreen.route, arguments: {});
  }

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        padding: EdgeInsets.all(16),
        alignment: Alignment.bottomCenter,
        child: _versionWidget(),
      ),
    );
  }

  _versionWidget() {
    return FutureBuilder<PackageInfo>(
      future: PackageInfo.fromPlatform(),
      builder: (context, snapshot) {
        var version = snapshot.data?.version ?? '';
        var build = snapshot.data?.buildNumber ?? '';
        return Text(
          'Version $version ($build)',
          textAlign: TextAlign.end,
        );
      },
    );
  }
}
