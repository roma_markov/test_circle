import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:lesson_scheduling_pga/ui/screen/add_event/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/schedule/week/screen.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/calendar.dart';
import 'package:rxdart/rxdart.dart';

import 'day/screen.dart';
import 'month/screen.dart';

class ScheduleScreen extends StatefulWidget {
  static const String route = '/ScheduleScreen';

  final JumpTodayController todayController;

  ScheduleScreen._init(this.todayController);

  factory ScheduleScreen.fromArgs(JumpTodayController todayController) {
    return ScheduleScreen._init(todayController);
  }

  static Future push(BuildContext context) {
    return Navigator.of(context).pushNamed(ScheduleScreen.route, arguments: {});
  }

  @override
  _ScheduleScreenState createState() => _ScheduleScreenState();
}

class _ScheduleScreenState extends State<ScheduleScreen> with TickerProviderStateMixin {
  DateTime _selectedDate;
  TabController _tabController;
  List<JumpTodayController> _todayControllers = [JumpTodayController(), JumpTodayController(), JumpTodayController()];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    widget.todayController.addListener(() {
      _todayControllers[_tabController.index].jumpToToday();
    });
    WidgetsBinding.instance.addPostFrameCallback((_) => _refreshEvents(false));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: _onAddEventPressed,
        child: const Icon(Icons.add),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: [
          Positioned.fill(
            top: 56,
            child: TabBarView(
              controller: _tabController,
              physics: NeverScrollableScrollPhysics(),
              children: [
                ScheduleDayScreen.fromArgs(_todayControllers[0], _onRefresh),
                ScheduleWeekScreen.fromArgs(_onSelectedDateChanged, _todayControllers[1], _onRefresh),
                ScheduleMonthScreen.fromArgs(_onSelectedDateChanged, _todayControllers[2], _onRefresh),
              ],
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            height: 56,
            child: Container(
              color: appNavigationColor,
              child: TabBar(
                controller: _tabController,
                indicatorColor: appWhiteColor,
                onTap: (index) => _tabController.index = index,
                tabs: [
                  const Tab(text: 'DAY'),
                  const Tab(text: 'WEEK'),
                  const Tab(text: 'MONTH'),
                ],
              ),
            ),
          ),
          _progress(),
        ],
      ),
    );
  }

  _progress() {
    final bloc = Provider.of<BLoC>(context);
    final progressStream = ZipStream.zip2(
      bloc.program.progressStream,
      bloc.privateLesson.progressStream,
      (programProgress, lessonProgress) => programProgress || lessonProgress,
    );
    return StreamBuilder<bool>(
        stream: progressStream,
        initialData: false,
        builder: (context, snapshot) {
          return Positioned(
            left: 0,
            right: 0,
            top: 56,
            height: 5,
            child: snapshot.data ? LinearProgressIndicator() : Container(),
          );
        });
  }

  _refreshEvents(bool force) async {
    final provider = Provider.of<BLoC>(context);
    await provider.program.requestPrograms(force: force);
    await provider.privateLesson.requestPrivateLessons(force: force);
  }

  _onRefresh() {
    _refreshEvents(true);
  }

  _onSelectedDateChanged(DateTime date) {
    _selectedDate = date;
  }

  _onAddEventPressed() {
    AddEventScreen.push(context, selectedDate: _selectedDate);
  }
}
