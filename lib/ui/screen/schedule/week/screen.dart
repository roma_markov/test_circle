import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:lesson_scheduling_pga/logic/bloc/bloc.dart';
import 'package:lesson_scheduling_pga/logic/models/private_lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/private_details/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/scheduled_details/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/schedule/item_private_lesson.dart';
import 'package:lesson_scheduling_pga/ui/screen/schedule/item_program_lesson.dart';
import 'package:lesson_scheduling_pga/ui/shared/block_provider.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/calendar.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/calendar_decoration.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/event_list.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

enum ScheduleWeekMode { week, day }

class ScheduleWeekScreen extends StatefulWidget {
  static const String route = '/ScheduleWeekScreen';

  final ValueChanged<DateTime> onDateSelected;
  final JumpTodayController todayController;
  final VoidCallback onRefresh;

  ScheduleWeekScreen._init(this.onDateSelected, this.todayController, this.onRefresh);

  factory ScheduleWeekScreen.fromArgs(
    ValueChanged<DateTime> onDateSelected,
    JumpTodayController todayController,
    VoidCallback onRefresh,
  ) {
    return ScheduleWeekScreen._init(onDateSelected, todayController, onRefresh);
  }

  static Future push(BuildContext context) {
    return Navigator.of(context).pushNamed(ScheduleWeekScreen.route, arguments: {});
  }

  @override
  _ScheduleWeekScreenState createState() => _ScheduleWeekScreenState();
}

class _ScheduleWeekScreenState extends State<ScheduleWeekScreen> {
  ScheduleWeekMode mode = ScheduleWeekMode.week;
  DateTime selectedDate;
  List<DateTime> selectedWeek = getDaysInWeek(DateTime.now());

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<BLoC>(context);
    return Container(
      child: StreamBuilder<EventList>(
        initialData: EventList(),
        stream: bloc.eventsStream(),
        builder: (context, snapshot) {
          final eventList = snapshot.data;
          final thisWeekList = List<dynamic>();
          final thisDayEvents = List<dynamic>();
          if (selectedDate != null) {
            thisDayEvents.addAll(eventList.getEvents(selectedDate).whereType<PrivateLesson>().toList());
            final thisMonthProgramEvents = eventList.getProgramsForMonth(selectedDate);
            thisMonthProgramEvents.forEach((event) {
              event.lessonsSchedule.forEach((lesson, schedules) {
                final schedule = schedules.firstWhere((item) => item.date == selectedDate, orElse: () => null);
                if (schedule != null) {
                  schedule.program = event;
                  thisDayEvents.add(schedule);
                }
              });
            });
          }
          selectedWeek.forEach((day) {
            List<dynamic> listForDay = [];
            DateTime date = DateTime(day.year, day.month, day.day);
            listForDay.addAll(
              eventList.getEvents(date).whereType<PrivateLesson>().toList(),
            );
            final thisMonthProgramEvents = eventList.getProgramsForMonth(date);
            thisMonthProgramEvents.forEach((event) {
              event.lessonsSchedule.forEach((lesson, schedules) {
                final schedule = schedules.firstWhere((item) => item.date == date, orElse: () => null);
                if (schedule != null) {
                  schedule.program = event;
                  listForDay.add(schedule);
                }
              });
            });
            if (listForDay.isNotEmpty) {
              thisWeekList.add(day);
              thisWeekList.addAll(listForDay);
            }
          });
          int itemsCount = (mode == ScheduleWeekMode.week) ? thisWeekList.length : thisDayEvents.length;
          return RefreshIndicator(
            onRefresh: () async => widget.onRefresh(),
            child: CustomScrollView(
              slivers: <Widget>[
                _calendarSliver(eventList),
                _sectionHeaderSliver(),
                SliverList(
                  delegate: SliverChildBuilderDelegate((context, position) {
                    return mode == ScheduleWeekMode.week
                        ? _weekModeItem(position, thisWeekList)
                        : _dayModeItem(position, thisDayEvents);
                  }, childCount: itemsCount),
                )
              ],
            ),
          );
        },
      ),
    );
  }

  _sectionHeaderSliver() {
    return SliverToBoxAdapter(
      child: mode == ScheduleWeekMode.day
          ? SectionHeader(
              textColor: inputHintColor,
              text:
                  '${DateFormat("EEEE d").format(selectedDate).toUpperCase()}${_isToday(selectedDate) ? ' (TODAY)' : ''}',
            )
          : Container(),
    );
  }

  _calendarSliver(EventList eventList) {
    return SliverToBoxAdapter(
      child: Container(
          height: Calendar.calendarHeight(context, 1),
          child: Calendar(
            eventList: eventList,
            selectedDateTimes: [selectedDate],
            controller: widget.todayController,
            selectedDayTextColor: appWhiteColor,
            selectedDayButtonColor: appBlackColor,
            onDayPressed: _onDayPressed,
            decoratorBuilder: CalendarDecoration.buildDecoration,
            onCalendarChanged: _onWeekChanged,
            weekFormat: true,
          )),
    );
  }

  _weekModeItem(int position, List<dynamic> thisWeekList) {
    if (thisWeekList.isNotEmpty) {
      final item = thisWeekList[position];
      if (item is DateTime) {
        return SectionHeader(
          textColor: inputHintColor,
          text: '${DateFormat("EEEE d").format(item).toUpperCase()}${_isToday(item) ? ' (TODAY)' : ''}',
        );
      } else if (item is PrivateLesson) {
        return PrivateLessonListItem(
          indicatorColor: appRedColor,
          event: item,
          onPressed: _onPrivateLessonPressed,
        );
      } else if (item is Schedule) {
        return ProgramLessonListItem(
          indicatorColor: colorById(item.lesson.id),
          event: item,
          onPressed: _onLessonPressed,
        );
      }
    }
  }

  _dayModeItem(int position, List<dynamic> thisDayEvents) {
    final item = thisDayEvents[position];
    if (item is PrivateLesson) {
      return PrivateLessonListItem(
        indicatorColor: appRedColor,
        event: item,
        onPressed: _onPrivateLessonPressed,
      );
    } else if (item is Schedule) {
      return ProgramLessonListItem(
        indicatorColor: colorById(item.lesson.id),
        event: item,
        onPressed: _onLessonPressed,
      );
    }
  }

  _isToday(DateTime date) {
    if (date == null) return false;
    final now = DateTime.now();
    return now.month == date.month && now.day == date.day;
  }

  _onWeekChanged(List<DateTime> week) {
    setState(() => selectedWeek = week);
  }

  _onDayPressed(DateTime date) {
    if (selectedDate == null ||
        (selectedDate.month != date.month) ||
        (selectedDate.month == date.month && selectedDate.day != date.day)) {
      selectedDate = date;
      widget.onDateSelected(date);
      mode = ScheduleWeekMode.day;
    } else {
      selectedDate = null;
      mode = ScheduleWeekMode.week;
    }
    setState(() {});
  }

  _onLessonPressed(Schedule schedule) {
    ScheduledLessonDetails.push(context, schedule);
  }

  _onPrivateLessonPressed(PrivateLesson event) {
    ScheduledPrivateLessonDetails.push(context, event);
  }
}
