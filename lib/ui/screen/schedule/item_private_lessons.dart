import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/l10n/localization.dart';
import 'package:lesson_scheduling_pga/logic/models/private_lesson.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

class PrivateLessonsListItem extends StatelessWidget {
  final Color indicatorColor;
  final List<PrivateLesson> events;

  const PrivateLessonsListItem({
    Key key,
    @required this.events,
    @required this.indicatorColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int studentsCount = 0;
    events.forEach((event) {
      studentsCount += event.players.length;
    });
    int hoursDuration = 0;
    events.forEach((event) {
      hoursDuration += event.lesson.durationHours;
    });
    return Container(
      height: 80,
      child: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Container(
              color: appWhiteColor,
            ),
          ),
          Positioned(
            left: 0,
            top: 0,
            bottom: 0,
            child: Container(
              width: 4,
              color: indicatorColor,
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              height: 1,
              color: Color(0xFFE5E5E5),
            ),
          ),
          Positioned(
            left: 24,
            right: 0,
            top: 24,
            child: Text(
              'Private lessons',
              style: lessonItemNameStyle,
            ),
          ),
          Positioned(
            left: 24,
            right: 24,
            top: 47,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                  child: Text(
                    AppLocalizations.of(context).lessons(events.length),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: lessonItemGoalStyle,
                  ),
                ),
                Flexible(
                  child: Text(
                    "    |    ${AppLocalizations.of(context).hours(hoursDuration)}",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: lessonItemGoalStyle,
                  ),
                ),
                Flexible(
                  child: Text(
                    "    |    ${AppLocalizations.of(context).students(studentsCount)}",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: lessonItemGoalStyle,
                  ),
                ),
              ],
            ),
          ),
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                highlightColor: indicatorColor.withOpacity(0.2),
                onTap: () {},
                child: Container(),
              ),
            ),
          )
        ],
      ),
    );
  }
}
