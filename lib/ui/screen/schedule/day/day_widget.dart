import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lesson_scheduling_pga/logic/bloc/bloc.dart';
import 'package:lesson_scheduling_pga/logic/models/private_lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/private_details/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/scheduled_details/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/schedule/choose_time/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/schedule/item_private_lesson.dart';
import 'package:lesson_scheduling_pga/ui/screen/schedule/item_program_lesson.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/calendar.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/event_list.dart';
import 'package:lesson_scheduling_pga/ui/shared/dashed_painter.dart';

final kHourHeight = 72.0;
final kStartHour = 7;

class DayWidget extends StatefulWidget {
  final JumpTodayController controller;
  final DateTime selectedDate;
  final Function(List<Pair> lessonRanges) onRangesChanged;
  final bool isDragging;

  const DayWidget({
    Key key,
    @required this.selectedDate,
    this.controller,
    this.onRangesChanged,
    this.isDragging = false,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _DayWidgetState();
  }
}

class _DayWidgetState extends State<DayWidget> {
  ScrollController controller = ScrollController(initialScrollOffset: kHourHeight * kStartHour);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<BLoC>(context);
    return StreamBuilder<EventList>(
      initialData: EventList(),
      stream: bloc.eventsStream(),
      builder: (context, snapshot) {
        final eventList = snapshot.data;
        final thisDayEvents = List<dynamic>();
        thisDayEvents.addAll(eventList.getEvents(widget.selectedDate).whereType<PrivateLesson>().toList());
        final thisMonthProgramEvents = eventList.getProgramsForMonth(widget.selectedDate);
        thisMonthProgramEvents.forEach((event) {
          event.lessonsSchedule.forEach((lesson, schedules) {
            final schedule = schedules.firstWhere((item) => item.date == widget.selectedDate, orElse: () => null);
            if (schedule != null) {
              schedule.program = event;
              thisDayEvents.add(schedule);
            }
          });
        });
        return _hoursSliver(thisDayEvents);
      },
    );
  }

  _hoursSliver(List<dynamic> events) {
    return SliverToBoxAdapter(
      child: Stack(children: _eventItems(events)),
    );
  }

  _dayModeItem(dynamic events, int position, List<Pair> lessonRanges) {
    if (events is PrivateLesson) {
      final index = events.start.hour - 1;
      final minutesAdd = (events.start.minute * kHourHeight / 60.0);
      final height = (kHourHeight - 1) * events.lesson.durationHours;
      final top = 1.0 + (index * kHourHeight) + minutesAdd;
      lessonRanges[position] = Pair(top, top + height);
      return Positioned(
        top: top,
        left: 55,
        right: 0,
        height: height,
        child: PrivateLessonListItem(
          indicatorColor: appRedColor,
          event: events,
          onPressed: _onPrivateLessonPressed,
        ),
      );
    } else if (events is Schedule) {
      final index = events.start.hour - 1;
      final minutesAdd = (events.start.minute * kHourHeight / 60.0);
      final height = (kHourHeight - 1) * events.lesson.durationHours;
      final top = 1.0 + (index * kHourHeight) + minutesAdd;
      lessonRanges[position] = Pair(top, top + height);
      return Positioned(
        top: top,
        left: 55,
        right: 0,
        height: height,
        child: ProgramLessonListItem(
          indicatorColor: colorById(events.lesson.id),
          onPressed: _onLessonPressed,
          event: events,
        ),
      );
    }
  }

  List<Widget> _eventItems(List<dynamic> events) {
    List<Pair> lessonRanges = List.generate(events.length, (_) => Pair.empty());
    final list = <Widget>[Column(children: List.generate(24, (index) => _hoursItem(index)))];
    list.addAll(events.map((event) => _dayModeItem(event, events.indexOf(event), lessonRanges)));
    if (widget.onRangesChanged != null) widget.onRangesChanged(lessonRanges);
    return list;
  }

  Widget _hoursItem(int position) {
    DateTime now = DateTime.now();
    DateTime start = DateTime(now.year, now.month, now.day, position + 1);
    return Container(
      height: kHourHeight,
      child: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            left: 55,
            right: 0,
            height: 0.5,
            child: Container(
              color: const Color(0xFF979797),
            ),
          ),
          Positioned(
            top: kHourHeight / 2.0,
            left: 55,
            right: 0,
            height: 0.5,
            child: CustomPaint(painter: LineDashedPainter()),
          ),
          Positioned(
            top: 0,
            left: 0,
            width: 44,
            child: AnimatedOpacity(
              child: Text(
                DateFormat.j().format(start),
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: fontRobotoRegular,
                  fontSize: 12,
                  color: inputHintColor,
                ),
              ),
              duration: Duration(milliseconds: 100),
              opacity: widget.isDragging ? 0.3 : 1.0,
            ),
          ),
        ],
      ),
    );
  }

  _onLessonPressed(Schedule schedule) {
    ScheduledLessonDetails.push(context, schedule);
  }

  _onPrivateLessonPressed(PrivateLesson event) {
    ScheduledPrivateLessonDetails.push(context, event);
  }
}
