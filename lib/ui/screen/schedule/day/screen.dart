import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/calendar.dart';

import 'day_widget.dart';

class ScheduleDayScreen extends StatefulWidget {
  static const String route = '/ScheduleDayScreen';
  final JumpTodayController todayController;
  final VoidCallback onRefresh;

  ScheduleDayScreen._init(this.todayController, this.onRefresh);

  factory ScheduleDayScreen.fromArgs(JumpTodayController todayController, VoidCallback onRefresh) {
    return ScheduleDayScreen._init(todayController, onRefresh);
  }

  static Future push(BuildContext context) {
    return Navigator.of(context).pushNamed(ScheduleDayScreen.route, arguments: {});
  }

  @override
  _ScheduleDayScreenState createState() => _ScheduleDayScreenState();
}

class _ScheduleDayScreenState extends State<ScheduleDayScreen> {
  DateTime selectedDate;
  ScrollController controller = ScrollController(initialScrollOffset: kHourHeight * kStartHour);

  @override
  void initState() {
    super.initState();
    final now = DateTime.now();
    selectedDate = DateTime(now.year, now.month, now.day);
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async => widget.onRefresh(),
      child: CustomScrollView(
        controller: controller,
        slivers: <Widget>[
          _calendarSliver(),
          DayWidget(
            selectedDate: selectedDate,
            controller: widget.todayController,
          ),
        ],
      ),
    );
  }

  _calendarSliver() {
    return SliverToBoxAdapter(
      child: Container(
          height: 66,
          child: Calendar(
            onCalendarChanged: _onDayChanged,
            controller: widget.todayController,
            dayFormat: true,
          )),
    );
  }

  _onDayChanged(DateTime day) {
    setState(() => selectedDate = day);
  }
}
