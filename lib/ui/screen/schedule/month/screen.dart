import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:lesson_scheduling_pga/logic/bloc/bloc.dart';
import 'package:lesson_scheduling_pga/logic/models/private_lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/private_details/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/scheduled_details/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/details/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/list/list_item.dart';
import 'package:lesson_scheduling_pga/ui/screen/schedule/item_private_lesson.dart';
import 'package:lesson_scheduling_pga/ui/screen/schedule/item_private_lessons.dart';
import 'package:lesson_scheduling_pga/ui/screen/schedule/item_program_lesson.dart';
import 'package:lesson_scheduling_pga/ui/shared/block_provider.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/calendar.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/calendar_decoration.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/event_list.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

enum ScheduleMonthMode { month, day }

class ScheduleMonthScreen extends StatefulWidget {
  static const String route = '/ScheduleMonthScreen';
  final ValueChanged<DateTime> onDateSelected;
  final JumpTodayController todayController;
  final VoidCallback onRefresh;

  ScheduleMonthScreen._init(this.onDateSelected, this.todayController, this.onRefresh);

  factory ScheduleMonthScreen.fromArgs(
    ValueChanged<DateTime> onDateSelected,
    JumpTodayController controller,
    VoidCallback onRefresh,
  ) {
    return ScheduleMonthScreen._init(onDateSelected, controller, onRefresh);
  }

  static Future push(BuildContext context) {
    return Navigator.of(context).pushNamed(ScheduleMonthScreen.route, arguments: {});
  }

  @override
  _ScheduleMonthScreenState createState() => _ScheduleMonthScreenState();
}

class _ScheduleMonthScreenState extends State<ScheduleMonthScreen> {
  ScheduleMonthMode mode = ScheduleMonthMode.month;
  DateTime selectedDate;
  DateTime selectedMonth = DateTime.now();
  int maxProgramsForDay = 0;

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<BLoC>(context);
    return Container(
      child: StreamBuilder<EventList>(
        initialData: EventList(),
        stream: bloc.eventsStream(),
        builder: (context, snapshot) {
          final eventList = snapshot.data;
          final thisMonthPrivateEvents = eventList.getPrivateEventsForMonth(selectedMonth);
          final thisMonthProgramEvents = eventList.getProgramsForMonth(selectedMonth);
          List<dynamic> thisDayEvents = [];
          thisDayEvents.addAll(eventList.getEvents(selectedDate).whereType<PrivateLesson>().toList());
          thisMonthProgramEvents.forEach((event) {
            event.lessonsSchedule.forEach((lesson, schedules) {
              final schedule = schedules.firstWhere((item) => item.date == selectedDate, orElse: () => null);
              if (schedule != null) {
                schedule.program = event;
                thisDayEvents.add(schedule);
              }
            });
          });
          int itemsCount = 0;
          if (mode == ScheduleMonthMode.month) {
            itemsCount += (thisMonthPrivateEvents.isNotEmpty ? 1 : 0) + thisMonthProgramEvents.length;
          } else {
            itemsCount += thisDayEvents.length;
          }
          return RefreshIndicator(
            onRefresh: () async => widget.onRefresh(),
            child: CustomScrollView(
              slivers: <Widget>[
                _calendarSliver(eventList),
                _sectionHeaderSliver(),
                SliverList(
                  delegate: SliverChildBuilderDelegate((context, position) {
                    return (mode == ScheduleMonthMode.month)
                        ? _monthModeItem(position, thisMonthProgramEvents, thisMonthPrivateEvents)
                        : _dayModeItem(position, thisDayEvents);
                  }, childCount: itemsCount),
                )
              ],
            ),
          );
        },
      ),
    );
  }

  _monthModeItem(int position, List<Program> programEvents, List<PrivateLesson> privateEvents) {
    if (position == 0 && privateEvents.isNotEmpty) {
      return PrivateLessonsListItem(
        indicatorColor: appRedColor,
        events: privateEvents,
      );
    }
    final programEvent = programEvents[position - (privateEvents.isNotEmpty ? 1 : 0)];
    return ProgramListItem(
      program: programEvent,
      indicatorColor: colorById(programEvent.id),
      onPressed: _onProgramPressed,
    );
  }

  _dayModeItem(int position, List<dynamic> thisDayEvents) {
    final item = thisDayEvents[position];
    if (item is PrivateLesson) {
      return PrivateLessonListItem(
        indicatorColor: appRedColor,
        event: item,
        onPressed: _onPrivateLessonPressed,
      );
    } else if (item is Schedule) {
      return ProgramLessonListItem(
        indicatorColor: colorById(item.lesson.id),
        event: item,
        onPressed: _onLessonPressed,
      );
    }
  }

  _calendarSliver(EventList eventList) {
    return SliverToBoxAdapter(
      child: Container(
          height: Calendar.calendarHeight(context, 6),
          child: Calendar(
            controller: widget.todayController,
            eventList: eventList,
            selectedDateTimes: [selectedDate],
            selectedDayTextColor: appWhiteColor,
            selectedDayButtonColor: appBlackColor,
            onDayPressed: _onDayPressed,
            onCalendarChanged: _onMonthChanged,
            decoratorBuilder: CalendarDecoration.buildDecoration,
            weekFormat: false,
          )),
    );
  }

  _sectionHeaderSliver() {
    return SliverSectionHeader(
        textColor: inputHintColor,
        text: mode == ScheduleMonthMode.month
            ? 'PROGRAMS & LESSONS'
            : '${DateFormat("EEEE d").format(selectedDate).toUpperCase()}${_isToday() ? ' (TODAY)' : ''}');
  }

  _isToday() {
    if (selectedDate == null) return false;
    final now = DateTime.now();
    return now.month == selectedDate.month && now.day == selectedDate.day;
  }

  _onMonthChanged(DateTime date) {
    setState(() => selectedMonth = date);
  }

  _onDayPressed(DateTime date) {
    if (selectedDate == null ||
        (selectedDate.month != date.month) ||
        (selectedDate.month == date.month && selectedDate.day != date.day)) {
      selectedDate = date;
      widget.onDateSelected(date);
      mode = ScheduleMonthMode.day;
    } else {
      selectedDate = null;
      mode = ScheduleMonthMode.month;
    }
    setState(() {});
  }

  _onLessonPressed(Schedule schedule) {
    ScheduledLessonDetails.push(context, schedule);
  }

  _onProgramPressed(Program item) {
    ProgramDetailsScreen.push(context, item.id);
  }

  _onPrivateLessonPressed(PrivateLesson event) {
    ScheduledPrivateLessonDetails.push(context, event);
  }
}
