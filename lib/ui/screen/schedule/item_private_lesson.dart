import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lesson_scheduling_pga/l10n/localization.dart';
import 'package:lesson_scheduling_pga/logic/models/private_lesson.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

typedef PrivateLessonPressed = void Function(PrivateLesson event);

class PrivateLessonListItem extends StatelessWidget {
  final Color indicatorColor;
  final PrivateLesson event;
  final PrivateLessonPressed onPressed;

  const PrivateLessonListItem({
    Key key,
    @required this.event,
    @required this.indicatorColor,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 96,
      child: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Container(
              color: appWhiteColor,
            ),
          ),
          Positioned(
            left: 0,
            top: 0,
            bottom: 0,
            child: Container(
              width: 4,
              color: indicatorColor,
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              height: 1,
              color: Color(0xFFE5E5E5),
            ),
          ),
          Positioned(
            left: 24,
            right: 0,
            top: 12,
            child: Text(
              '${DateFormat.Hm().format(event.start)} - ${DateFormat.Hm().format(event.end)}',
              style: TextStyle(
                fontFamily: fontRobotoMedium,
                fontSize: 12,
                color: appBlackColor.withOpacity(0.6),
              ),
            ),
          ),
          Positioned(
            left: 24,
            right: 0,
            top: 32,
            child: Text(
              event.lesson.name,
              style: lessonItemNameStyle,
            ),
          ),
          Positioned(
            left: 24,
            right: 0,
            top: 52,
            child: Text(
              'Private lesson',
              style: TextStyle(
                fontFamily: fontRobotoRegular,
                fontSize: 12,
                color: appBlackColor.withOpacity(0.4),
              ),
            ),
          ),
          Positioned(
            left: 24,
            right: 0,
            top: 72,
            child: Text(
              AppLocalizations.of(context).playersCount(event.players.length).toUpperCase(),
              style: TextStyle(
                fontFamily: fontRobotoRegular,
                fontSize: 10,
                color: appBlackColor,
              ),
            ),
          ),
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                highlightColor: indicatorColor.withOpacity(0.2),
                onTap: () => onPressed(event),
                child: Container(),
              ),
            ),
          )
        ],
      ),
    );
  }
}
