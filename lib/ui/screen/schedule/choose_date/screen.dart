import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/calendar.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

class ChooseDateScreen extends StatefulWidget {
  static const String route = '/ChooseDateScreen';
  final DateTime selectedDate;

  ChooseDateScreen._init(this.selectedDate);

  factory ChooseDateScreen.fromArgs(Map<String, Object> args) {
    return ChooseDateScreen._init(args['selectedDate']);
  }

  static Future push(BuildContext context, {DateTime selectedDate}) {
    return Navigator.of(context).pushNamed(
      ChooseDateScreen.route,
      arguments: {'selectedDate': selectedDate},
    );
  }

  @override
  _ChooseDateScreenState createState() => _ChooseDateScreenState();
}

class _ChooseDateScreenState extends State<ChooseDateScreen> {
  DateTime selectedDay;

  @override
  void initState() {
    super.initState();
    selectedDay = widget.selectedDate ?? DateTime.now();
  }

  @override
  Widget build(BuildContext context) {
    final now = DateTime.now();
    return Scaffold(
      backgroundColor: appWhiteColor,
      appBar: _appBar(),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            child: Calendar(
              onDayPressed: (DateTime date) {
                setState(() => selectedDay = date);
              },
              selectedDayButtonColor: appRedColor.withOpacity(0.5),
              minSelectedDate: DateTime(now.year, now.month, now.day),
              weekFormat: false,
              selectedDateTimes: [selectedDay],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: AppButton(
              title: 'SUBMIT',
              onPressed: _onSetPressed,
            ),
          )
        ],
      ),
    );
  }

  _appBar() {
    return AppBar(
      titleSpacing: 0,
      brightness: Brightness.light,
      leading: IconButton(
        icon: Icon(Icons.close),
        onPressed: _onClosePressed,
      ),
      iconTheme: IconThemeData(color: inputHintColor),
      backgroundColor: lightAppBarColor,
      title: Text(
        'SET LESSON DATE',
        style: appBarLightTextStyle,
      ),
    );
  }

  _onSetPressed(BuildContext context) {
    Navigator.of(context).pop(selectedDay);
  }

  _onClosePressed() {
    Navigator.of(context).pop();
  }
}
