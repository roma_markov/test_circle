import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lesson_scheduling_pga/l10n/localization.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';

typedef LessonPressed = void Function(Lesson item);

class ChooseTimeLessonListItem extends StatelessWidget {
  final Color indicatorColor;
  final Lesson lesson;
  final LessonPressed onPressed;
  final LessonPressed onLongPressed;
  final bool isSelected;
  final bool isShowTime;
  final List<Schedule> schedules;
  final DateTime time;

  const ChooseTimeLessonListItem({
    Key key,
    @required this.lesson,
    @required this.onPressed,
    @required this.indicatorColor,
    @required this.isShowTime,
    this.onLongPressed,
    this.isSelected = false,
    this.schedules,
    this.time,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: appWhiteColor,
      child: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            bottom: 0,
            width: 4,
            child: Container(color: indicatorColor),
          ),
          Positioned(
            left: 24,
            right: 0,
            top: 4,
            child: AnimatedOpacity(
              opacity: isShowTime ? 1.0 : 0.0,
              duration: const Duration(milliseconds: 100),
              child: Text(
                time == null
                    ? ''
                    : '${DateFormat.Hm().format(time)} - ${DateFormat.Hm().format(time.add(Duration(hours: lesson.durationHours)))}',
                style: TextStyle(
                  fontFamily: fontRobotoMedium,
                  fontSize: 12,
                  color: appBlackColor.withOpacity(0.6),
                ),
              ),
            ),
          ),
          Positioned(
            left: 24,
            right: 0,
            top: 17,
            child: Text(
              lesson.name,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: lessonItemNameStyle,
            ),
          ),
          Positioned(
            left: 24,
            right: 24,
            top: 42,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                  child: Text(
                    lesson.goal,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: lessonItemGoalStyle,
                  ),
                ),
                Text(
                  '    |    ${AppLocalizations.of(context).hours(lesson.durationHours)}',
                  style: lessonItemGoalStyle,
                )
              ],
            ),
          ),
          Positioned.fill(
            child: Material(
              color: indicatorColor.withOpacity(0.1),
              child: InkWell(
                highlightColor: indicatorColor.withOpacity(0.2),
                onTap: () => onPressed(lesson),
                onLongPress: onLongPressed != null ? () => onLongPressed(lesson) : null,
                child: Container(
                  decoration: BoxDecoration(
                    border: schedules == null
                        ? Border(
                            bottom: BorderSide(
                              color: Color(0xFFE5E5E5),
                              width: 1,
                            ),
                          )
                        : Border.all(
                            color: Color(0xFFE5E5E5),
                            width: 1,
                          ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
