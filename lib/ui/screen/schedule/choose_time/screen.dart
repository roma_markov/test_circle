import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:lesson_scheduling_pga/ui/screen/schedule/day/day_widget.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';

import 'list_item.dart';

final _kScrollDuration = 3000.0;

class ChooseTimeScreen extends StatefulWidget {
  static const String route = '/ChooseTimeScreen';
  final Lesson lesson;
  final bool isPrivate;
  final DateTime selectedDate;
  final TimeOfDay selectedTime;

  ChooseTimeScreen._init(
    this.lesson,
    this.selectedDate,
    this.selectedTime,
    this.isPrivate,
  );

  factory ChooseTimeScreen.fromArgs(Map<String, Object> args) {
    return ChooseTimeScreen._init(
      args['lesson'],
      args['selectedDate'],
      args['selectedTime'],
      args['isPrivate'],
    );
  }

  static Future push(
    BuildContext context,
    Lesson lesson,
    DateTime selectedDate,
    TimeOfDay selectedTime,
    bool isPrivate,
  ) {
    return Navigator.of(context).pushNamed(
      ChooseTimeScreen.route,
      arguments: {
        'lesson': lesson,
        'selectedDate': selectedDate,
        'selectedTime': selectedTime,
        'isPrivate': isPrivate,
      },
    );
  }

  @override
  _ChooseTimeScreenState createState() => _ChooseTimeScreenState();
}

class _ChooseTimeScreenState extends State<ChooseTimeScreen> {
  DateTime selectedDate;
  Offset offset = Offset.zero;
  bool isDragging = false;
  bool skipStep = false;
  ScrollController scrollController;
  Offset tapLocation;
  bool firstTapped = false;
  double fixedDy = 0.0;
  bool canDrag = true;
  double viewportHeight;
  double maxScrollOffset;
  double lessonItemHeight;
  List<Pair> lessonRanges = [];
  double lastTop;
  final now = DateTime.now();
  double initialScrollOffset = kHourHeight * (kStartHour - 1);

  @override
  void initState() {
    super.initState();
    selectedDate = DateTime(widget.selectedDate.year, widget.selectedDate.month, widget.selectedDate.day);
    if (widget.selectedTime != null) {
      firstTapped = true;
      offset = Offset(0, _offsetByTime());
    }

    scrollController = ScrollController();
    lessonItemHeight = (kHourHeight - 1) * widget.lesson.durationHours;
    scrollController.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (initialScrollOffset != 0) {
        scrollController.jumpTo(initialScrollOffset);
        initialScrollOffset = 0;
      }
    });
    return Scaffold(
      appBar: _appBar(),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned.fill(
            bottom: 60,
            child: LayoutBuilder(builder: (context, snapshot) {
              viewportHeight = snapshot.biggest.height;
              maxScrollOffset = (kHourHeight * 24) - viewportHeight;
              return GestureDetector(
                onTapUp: _onTapUp,
                child: CustomScrollView(
                  physics: ClampingScrollPhysics(),
                  controller: scrollController,
                  slivers: <Widget>[
                    DayWidget(
                      selectedDate: selectedDate,
                      onRangesChanged: _onLessonRangesChanged,
                      isDragging: isDragging,
                    ),
                  ],
                ),
              );
            }),
          ),
          firstTapped == false ? Container() : _draggable(),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: AppButton(
              title: 'SUBMIT',
              enabled: firstTapped,
              onPressed: _onSetPressed,
            ),
          ),
        ],
      ),
    );
  }

  _appBar() {
    return AppBar(
      titleSpacing: 0,
      brightness: Brightness.light,
      leading: IconButton(
        icon: Icon(Icons.close),
        onPressed: _onClosePressed,
      ),
      iconTheme: IconThemeData(color: inputHintColor),
      backgroundColor: lightAppBarColor,
      title: Text(
        'SET LESSON TIME',
        style: appBarLightTextStyle,
      ),
    );
  }

  _draggableWidget(double top, double scrollOffset, bool isAvailable) {
    return Positioned(
      left: 0,
      right: 0,
      top: top,
      height: lessonItemHeight,
      child: GestureDetector(
        onVerticalDragStart: (_) => isDragging = true,
        onVerticalDragEnd: (_) => _onDragEnd(),
        onVerticalDragUpdate: (details) => _onDragUpdate(details, scrollOffset, lessonItemHeight),
        child: Container(
          height: lessonItemHeight,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: 55,
                padding: EdgeInsets.only(right: 11),
                child: AnimatedOpacity(
                  duration: Duration(milliseconds: 100),
                  opacity: isDragging ? 1.0 : 0.0,
                  child: Text(
                    DateFormat.jm().format(_timeByTopOffset()),
                    textAlign: TextAlign.end,
                    style: TextStyle(
                      fontFamily: fontRobotoRegular,
                      fontSize: 12,
                      color: appGreenColor,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: ChooseTimeLessonListItem(
                  indicatorColor:
                      isAvailable ? widget.isPrivate ? appRedColor : colorById(widget.lesson.id) : Colors.grey,
                  lesson: widget.lesson,
                  isShowTime: !isDragging,
                  time: _timeByTopOffset(),
                  onPressed: (Lesson item) {},
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _normalizeTop(double top) {
    double step = (kHourHeight / 12.0);
    double diff = (top % step);
    return (top - diff - _scrollOffsetDiff());
  }

  _draggable() {
    final scrollOffset = (scrollController.hasClients) ? scrollController.offset : 0.0;
    if (tapLocation != null) {
      offset = Offset(0, min(tapLocation.dy + scrollOffset, maxScrollOffset + viewportHeight - lessonItemHeight));
      tapLocation = null;
    }
    double top = offset.dy - scrollOffset;

    if (isDragging) {
      top = _normalizeTop(top);
    }

    Pair nearestPair = _nearestLessonRange();
    bool isAvailable = nearestPair == null;
    if (skipStep) {
      skipStep = false;
      offset = Offset(0, lastTop + scrollOffset);
      top = lastTop;
    }

    if (!isAvailable && !isDragging) {
      double itemPosition = _normalizeTop(top + scrollOffset);
      double step = (kHourHeight / 12.0);
      double moveTo =
          itemPosition < nearestPair.center ? (nearestPair.top - lessonItemHeight - step) : nearestPair.bottom + step;
      moveTo = (moveTo < 0) ? nearestPair.bottom : moveTo;
      moveTo =
          (moveTo > maxScrollOffset + viewportHeight - lessonItemHeight) ? nearestPair.top - lessonItemHeight : moveTo;
      top = _normalizeTop(moveTo - scrollOffset);
      offset = Offset(0, top + scrollOffset);
      isAvailable = true;
    }
    if (!canDrag) {
      offset = Offset(0, fixedDy);
    }

    if (isDragging) {
      top = (top < 0) ? 1 : top;
      top = (top + lessonItemHeight > viewportHeight) ? viewportHeight - lessonItemHeight : top;
    } else {
      top = (top + scrollOffset < 0) ? 1 : top;
    }
    top = !canDrag ? fixedDy : top;

    lastTop = top;
    return _draggableWidget(top, scrollOffset, isAvailable);
  }

  _combineRanges(double itemHeight) {
    if (lessonRanges.isEmpty) return;

    lessonRanges.sort((left, right) => left.top.compareTo(right.top));
    if (lessonRanges.first.top < itemHeight) {
      lessonRanges.first = Pair(0, lessonRanges.first.bottom);
    }
    if (lessonRanges.last.bottom > maxScrollOffset + viewportHeight - itemHeight) {
      lessonRanges.last = Pair(lessonRanges.last.top, maxScrollOffset + viewportHeight);
    }
    Pair pair = lessonRanges.first;
    for (int index = 1; index < lessonRanges.length; index++) {
      final item = lessonRanges[index];
      if (item.top - pair.bottom < itemHeight) {
        pair.bottom = item.bottom;
        lessonRanges.removeAt(index);
      } else {
        pair = item;
      }
    }
  }

  _scrollOffsetDiff() {
    double dy = (scrollController.hasClients ? scrollController.offset : 0.0);
    double step = (kHourHeight / 12.0);
    double diff = (dy % step);
    return diff;
  }

  _timeByTopOffset() {
    double dy = (!canDrag) ? scrollController.offset + fixedDy : offset.dy - (isDragging ? _scrollOffsetDiff() : 0);
    dy = (dy - (dy % (kHourHeight / 12.0)));
    int hour = dy ~/ kHourHeight;
    double min = (dy % kHourHeight) * 60.0 / kHourHeight;
    return DateTime(now.year, now.month, now.day, hour + 1, min.toInt());
  }

  _offsetByTime() {
    double hoursDy = (widget.selectedTime.hour - 1) * kHourHeight;
    double minutesDy = kHourHeight * widget.selectedTime.minute / 60;
    return hoursDy + minutesDy;
  }

  _nearestLessonRange() {
    double dy = (!canDrag) ? scrollController.offset + fixedDy : offset.dy;
    return lessonRanges.firstWhere(
      (pair) => pair.isIntersects(dy + lessonItemHeight, dy),
      orElse: () => null,
    );
  }

  _onTapUp(TapUpDetails details) {
    if (firstTapped) return;
    tapLocation = details.localPosition;
    firstTapped = true;
    setState(() {});
  }

  _onDragUpdate(DragUpdateDetails details, double scrollOffset, double itemHeight) {
    if (!canDrag) {
      if ((fixedDy == 0 && details.delta.dy > 0) || (fixedDy > 0 && details.delta.dy < 0)) {
        scrollController.jumpTo(scrollController.offset);
        canDrag = true;
      }
      return;
    }
    final newDy = offset.dy + details.delta.dy;
    final isOnTop = newDy - scrollOffset < 0;
    final isOnBottom = newDy + itemHeight - scrollOffset > viewportHeight;
    if (isOnTop && scrollOffset > 0.0) {
      _startScrollAnimation(0, scrollOffset, 0);
    } else if (isOnBottom && scrollOffset < maxScrollOffset) {
      _startScrollAnimation(maxScrollOffset, scrollOffset, viewportHeight - itemHeight);
    } else {
      if (newDy < 0 || isOnBottom) return;
      setState(() => offset = Offset(0, newDy));
    }
  }

  _startScrollAnimation(double scrollTo, double scrollOffset, double fixedY) {
    int duration = (scrollTo == 0 ? scrollOffset : scrollTo - scrollOffset) * _kScrollDuration ~/ maxScrollOffset;
    canDrag = false;
    fixedDy = fixedY;
    scrollController.animateTo(scrollTo, duration: Duration(milliseconds: duration), curve: Curves.easeIn).then((_) {
      offset = Offset(0, scrollController.offset + fixedY);
      canDrag = true;
    });
  }

  _onDragEnd() {
    skipStep = true;
    isDragging = false;
    scrollController.jumpTo(scrollController.offset);
    setState(() {});
  }

  _onSetPressed(BuildContext context) {
    final result = TimeOfDay.fromDateTime(_timeByTopOffset());
    Navigator.of(context).pop(result);
  }

  _onClosePressed() {
    Navigator.of(context).pop();
  }

  _onLessonRangesChanged(List<Pair> lessonRanges) {
    this.lessonRanges = lessonRanges;
    _combineRanges(lessonItemHeight);
  }
}

class Pair {
  Pair(this.top, this.bottom);

  double top;
  double bottom;

  double get height => bottom - top;

  double get center => top + height / 2.0;

  bool isIntersects(double itemBottom, double itemTop) {
    return (itemBottom > top && itemBottom < bottom) || (itemTop > top && itemTop < bottom);
  }

  factory Pair.empty() {
    return Pair(0, 0);
  }
}
