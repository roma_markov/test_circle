import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';

import '../../../index.dart';

typedef StudentPressed = void Function(Student item);

class StudentListItem extends StatelessWidget {
  final Student student;
  final StudentPressed onPressed;
  final StudentPressed onLongPressed;
  final bool isSelected;

  const StudentListItem({
    Key key,
    @required this.student,
    @required this.onPressed,
    this.onLongPressed,
    this.isSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
      height: 112,
      child: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Container(
              decoration: BoxDecoration(
                color: appWhiteColor,
                border: Border.all(color: Color(0xFFE5E5E5)),
              ),
            ),
          ),
          Positioned(
            left: 24,
            right: 60,
            top: 27,
            child: Text(
              '${student.firstName} ${student.lastName}',
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: lessonItemNameStyle,
            ),
          ),
          Positioned(
            left: 24,
            right: 0,
            top: 52,
            child: Text(
              student.email,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: lessonItemGoalStyle,
            ),
          ),
          Positioned.fill(
            child: Material(
              color: isSelected ? appGreenColor.withOpacity(0.2) : Colors.transparent,
              child: InkWell(
                onTap: () => onPressed(student),
                onLongPress: onLongPressed != null ? () => onLongPressed(student) : null,
                child: Container(),
              ),
            ),
          ),
          (onLongPressed != null)
              ? Positioned(
                  right: 0,
                  top: 0,
                  bottom: 0,
                  width: 60,
                  child: Checkbox(
                    activeColor: inputHintColor,
                    onChanged: (bool value) => isSelected ? onPressed(student) : onLongPressed(student),
                    value: isSelected,
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
