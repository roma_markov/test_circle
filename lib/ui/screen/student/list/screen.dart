import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';
import 'package:lesson_scheduling_pga/ui/screen/student/details/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/student/list/students_widget.dart';

class StudentsScreen extends StatefulWidget {
  static const String route = '/StudentsScreen';

  StudentsScreen._init();

  factory StudentsScreen.fromArgs(Map<String, Object> args) {
    return StudentsScreen._init();
  }

  static Future push(BuildContext context) {
    return Navigator.of(context).pushNamed(StudentsScreen.route, arguments: {});
  }

  @override
  _StudentsScreenState createState() => _StudentsScreenState();
}

class _StudentsScreenState extends State<StudentsScreen> {
  @override
  Widget build(BuildContext context) {
    return StudentsWidget(onPressed: _onStudentPressed);
  }

  _onStudentPressed(Student student) {
    StudentDetailsScreen.push(context, student);
  }
}
