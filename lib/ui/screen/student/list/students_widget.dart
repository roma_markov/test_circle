import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/l10n/localization.dart';
import 'package:lesson_scheduling_pga/logic/bloc/bloc.dart';
import 'package:lesson_scheduling_pga/logic/bloc/modules/student.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';
import 'package:lesson_scheduling_pga/ui/shared/block_provider.dart';
import 'package:lesson_scheduling_pga/ui/shared/search_widget.dart';

import 'list_item.dart';

class StudentsWidget extends StatefulWidget {
  final StudentPressed onPressed;
  final StudentPressed onLongPressed;
  final Set<Student> selectedStudents;

  const StudentsWidget({
    Key key,
    this.onPressed,
    this.onLongPressed,
    this.selectedStudents,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _StudentsWidgetState();
  }
}

class _StudentsWidgetState extends State<StudentsWidget> {
  final searchController = TextEditingController();
  StudentBLoC studentBLoC;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      studentBLoC.requestStudents();
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    studentBLoC = Provider.of<BLoC>(context).student;
  }

  @override
  dispose() {
    studentBLoC.changeSearchFilter('');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Student>>(
        initialData: <Student>[],
        stream: studentBLoC.studentsStream,
        builder: (context, snapshot) {
          final list = snapshot.data;
          return Stack(
            children: [
              _listViewWidget(list),
              _progressIndicator(),
            ],
          );
        });
  }

  _listViewWidget(List<Student> list) {
    return RefreshIndicator(
      child: ListView.builder(
          itemCount: list.length + 1,
          itemBuilder: (context, position) {
            if (position == 0) return _searchWidget(context);
            return _listItemWidget(list[position - 1]);
          }),
      onRefresh: () => _refreshLessons(),
    );
  }

  _listItemWidget(Student student) {
    return StudentListItem(
      student: student,
      isSelected: widget.selectedStudents?.contains(student) ?? false,
      onLongPressed: widget.onLongPressed,
      onPressed: widget.onPressed,
    );
  }

  _progressIndicator() {
    return StreamBuilder<bool>(
        initialData: true,
        stream: studentBLoC.progressStream,
        builder: (context, snapshot) {
          return snapshot.data ? LinearProgressIndicator() : Container();
        });
  }

  _searchWidget(BuildContext context) {
    return SearchWidget(
      searchController: searchController,
      onChanged: _changeSearch,
      hint: AppLocalizations.of(context).studentSearchHint,
    );
  }

  //Actions
  _changeSearch(String value) {
    studentBLoC.changeSearchFilter(value);
  }

  _refreshLessons() async {
    await studentBLoC.requestStudents(force: true);
  }
}
