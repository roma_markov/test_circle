import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_text_field.dart';
import 'package:lesson_scheduling_pga/ui/shared/dropdown.dart';

import '../../../shared/style.dart';

class StudentCreateScreen extends StatefulWidget {
  static const String route = '/StudentCreateScreen';

  final Student student;

  StudentCreateScreen._init(this.student);

  factory StudentCreateScreen.fromArgs(Map<String, Object> args) {
    return StudentCreateScreen._init(args["student"]);
  }

  static Future push(BuildContext context, {Student student}) {
    return Navigator.of(context).pushNamed(
      StudentCreateScreen.route,
      arguments: {'student': student},
    );
  }

  @override
  _StudentCreateScreenState createState() => _StudentCreateScreenState();
}

class _StudentCreateScreenState extends State<StudentCreateScreen> {
  var createEnabled = false;
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final emailController = TextEditingController();

  final skillLevelController = TextEditingController();
  final noteController = TextEditingController();
  int selectedAge;
  int selectedGender;

  bool get isEditing => widget.student != null;

  @override
  void initState() {
    super.initState();
    if (isEditing) {
      _fillInfoField(widget.student);
      _onFormChanged();
    }
  }

  _fillInfoField(Student student) {
    firstNameController.text = student.firstName;
    lastNameController.text = student.lastName;
    emailController.text = student.email;
    skillLevelController.text = student.level;
    noteController.text = student.notes;
    selectedAge = student.age;
    selectedGender = student.gender;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appWhiteColor,
      appBar: AppBar(
        titleSpacing: 0,
        brightness: Brightness.light,
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: _onClosePressed,
        ),
        iconTheme: IconThemeData(color: inputHintColor),
        backgroundColor: lightAppBarColor,
        title: Text(
          'INVITE NEW PLAYER',
          style: appBarLightTextStyle,
        ),
      ),
      body: Stack(
        children: [
          Form(
            onChanged: _onFormChanged,
            child: ListView(
              padding: EdgeInsets.only(left: 16, right: 16, top: 20, bottom: 84),
              children: <Widget>[
                _label('PLAYER INFORMATION (REQUIRED)'),
                AppTextField(
                  hint: 'First Name',
                  textInputAction: TextInputAction.next,
                  textCapitalization: TextCapitalization.sentences,
                  controller: firstNameController,
                  onFieldSubmitted: (term) => FocusScope.of(context).nextFocus(),
                ),
                SizedBox(height: 12),
                AppTextField(
                  hint: 'Last Name',
                  textInputAction: TextInputAction.next,
                  textCapitalization: TextCapitalization.sentences,
                  controller: lastNameController,
                  onFieldSubmitted: (term) => FocusScope.of(context).nextFocus(),
                ),
                SizedBox(height: 12),
                AppTextField(
                  hint: 'Email',
                  textInputAction: TextInputAction.next,
                  keyboardType: TextInputType.emailAddress,
                  controller: emailController,
                  onFieldSubmitted: (term) => FocusScope.of(context).nextFocus(),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 4, top: 24, right: 4),
                  child: Text(
                    'When you invite players you send an email confirmation to them which requests both booking and payment.',
                    style: studentDescTextStyle,
                  ),
                ),
                _label('YOUR MESSAGE'),
                AppTextField(
                  isMultiline: true,
                  textCapitalization: TextCapitalization.sentences,
                  hint: 'Message',
                ),
                _label('ADDITIONAL INFO'),
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Dropdown(
                        items: _ages,
                        hint: "Age",
                        defaultValue: selectedAge,
                        callback: _onAgeSelected,
                      ),
                    ),
                    SizedBox(width: 24),
                    Expanded(
                      flex: 1,
                      child: Dropdown(
                        items: _genders,
                        hint: "Gender",
                        defaultValue: selectedGender,
                        callback: _onGenderSelected,
                        builder: (context, value) => value == 0 ? 'Male' : 'Female',
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 12),
                AppTextField(
                  hint: 'Skill level',
                  textInputAction: TextInputAction.next,
                  textCapitalization: TextCapitalization.sentences,
                  controller: skillLevelController,
                  onFieldSubmitted: (term) => FocusScope.of(context).nextFocus(),
                ),
                SizedBox(height: 12),
                AppTextField(
                  hint: 'Personal notes about this player',
                  textInputAction: TextInputAction.done,
                  textCapitalization: TextCapitalization.sentences,
                  controller: noteController,
                  onFieldSubmitted: (term) => FocusScope.of(context).unfocus(),
                  isMultiline: true,
                ),
                SizedBox(height: 12),
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: AppButton(
              title: 'SEND INVITE',
              enabled: createEnabled,
              onPressed: _onSavePressed,
            ),
          )
        ],
      ),
    );
  }

  _label(String text) {
    return Padding(
      padding: EdgeInsets.only(top: 24, bottom: 14, left: 4),
      child: Text(text, style: studentCreateTextStyle),
    );
  }

  _onFormChanged() {
    var newValue =
        firstNameController.text.isNotEmpty && lastNameController.text.isNotEmpty && emailController.text.isNotEmpty;
    if (createEnabled != newValue) setState(() => createEnabled = newValue);
  }

  _onClosePressed() {
    Navigator.of(context).pop();
  }

  _onGenderSelected(int index) {
    setState(() {
      selectedGender = _genders[index];
    });
  }

  _onAgeSelected(int index) {
    setState(() {
      selectedAge = _ages[index];
    });
  }

  _onSavePressed(BuildContext context) async {
    if (!isEditing) {
      final bloc = Provider.of<BLoC>(context).student;
      final result = await bloc.createStudent(_createStudent());
      if (result.isSuccess) {
        Navigator.of(context).pop();
      }
    }
  }

  _createStudent() {
    return Student(
      -1,
      firstNameController.text,
      lastNameController.text,
      emailController.text,
      selectedAge,
      skillLevelController.text,
      selectedGender,
      noteController.text,
    );
  }
}

final _genders = [0, 1];
final _ages = List.generate(80, (value) => value + 18);
