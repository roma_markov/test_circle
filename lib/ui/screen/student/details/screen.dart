import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';
import 'package:lesson_scheduling_pga/ui/screen/add_event/schedule_lesson_list/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/add_event/schedule_private_lesson/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/student/create/screen.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/bottom_dialog_item.dart';

import '../../../index.dart';

enum StudentDetailsMode { none, selection }

class StudentDetailsScreen extends StatefulWidget {
  static const String route = '/StudentDetailsScreen';

  final Student student;
  final StudentDetailsMode mode;

  StudentDetailsScreen._init(this.student, this.mode);

  factory StudentDetailsScreen.fromArgs(Map<String, Object> args) {
    return StudentDetailsScreen._init(
      args["student"],
      args["selectMode"] ?? StudentDetailsMode.none,
    );
  }

  static Future push(
    BuildContext context,
    Student student, {
    StudentDetailsMode mode = StudentDetailsMode.none,
  }) {
    return Navigator.of(context).pushNamed(
      StudentDetailsScreen.route,
      arguments: {
        'student': student,
        'selectMode': mode,
      },
    );
  }

  @override
  _StudentDetailsScreenState createState() => _StudentDetailsScreenState();
}

class _StudentDetailsScreenState extends State<StudentDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: Stack(
        children: <Widget>[
          ListView(
            padding: EdgeInsets.only(top: 40, left: 32, right: 32, bottom: 80),
            children: <Widget>[
              Container(
                width: 92,
                height: 92,
                margin: EdgeInsets.only(bottom: 26),
                child: Icon(
                  Icons.face,
                  size: 48,
                  color: appBlackColor.withOpacity(0.2),
                ),
                decoration: BoxDecoration(
                  color: appBlackColor.withOpacity(0.1),
                  shape: BoxShape.circle,
                ),
              ),
              Text(
                '${widget.student.firstName} ${widget.student.lastName}',
                textAlign: TextAlign.center,
                style: lessonNameTextStyle,
              ),
              SizedBox(height: 12),
              Text(
                widget.student.email,
                textAlign: TextAlign.center,
                style: hintTextStyle(color: appBlackColor),
              ),
              Row(
                children: <Widget>[
                  Expanded(flex: 1, child: _label('GENDER')),
                  Expanded(flex: 1, child: _label('AGE')),
                  Expanded(flex: 1, child: _label('LEVEL')),
                ],
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Text(
                      widget.student.gender == 0 ? 'Male' : 'Female',
                      style: lessonTextStyle,
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(
                      widget.student.age.toString(),
                      style: lessonTextStyle,
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(
                      widget.student.level,
                      style: lessonTextStyle,
                    ),
                  ),
                ],
              ),
              _label('PLAYER NOTES'),
              Text(
                widget.student.notes,
                style: lessonTextStyle,
              ),
            ],
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: AppButton(
              title: 'ADD TO EVENT',
              enabled: true,
              onPressed: _onAddPressed,
            ),
          )
        ],
      ),
    );
  }

  _appBar() {
    return AppBar(
      brightness: Brightness.light,
      titleSpacing: 0,
      actions: widget.mode == StudentDetailsMode.none
          ? <Widget>[
              IconButton(
                icon: Icon(Icons.more_vert),
                onPressed: _onMorePressed,
              )
            ]
          : null,
      iconTheme: IconThemeData(color: inputHintColor),
      backgroundColor: lightAppBarColor,
      title: Text(
        'PLAYER INFO',
        style: appBarLightTextStyle,
      ),
    );
  }

  _label(String text) {
    return Padding(
      padding: EdgeInsets.only(top: 24, bottom: 14),
      child: Text(text, style: lessonLabelTextStyle),
    );
  }

  _onAddPressed(BuildContext context) {
    if (widget.mode == StudentDetailsMode.selection) {
      Navigator.of(context).pop(widget.student);
    } else {
      _onSchedulePrivateLessonPressed();
    }
  }

  _onMorePressed() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return SizedBox(
              height: kBottomItemHeight + kBottomDialogPadding,
              child: Column(children: [
                Padding(padding: EdgeInsets.only(top: kBottomDialogPadding / 2.0)),
                //BottomDialogItem("EDIT STUDENT", Icons.edit, _onEditPressed),
                //BottomDialogItem("DELETE STUDENT", Icons.delete, _onDeletePressed),
                BottomDialogItem("SCHEDULE NEW PRIVATE LESSON", Icons.calendar_today, _onSchedulePrivateLessonPressed),
              ]));
        });
  }

  _onEditPressed() {
    Navigator.of(context).pop();
    StudentCreateScreen.push(context, student: widget.student);
  }

  _onDeletePressed() async {
    Navigator.of(context).pop();
  }

  _onSchedulePrivateLessonPressed() async {
    final result = await ScheduleLessonListScreen.push(context);
    if (result != null) {
      Navigator.of(context).pop(result);
      SchedulePrivateLessonScreen.push(context, result.id, student: widget.student);
    }
  }
}
