import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lesson_scheduling_pga/logic/models/place.dart';
import 'package:lesson_scheduling_pga/logic/models/private_lesson.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';

class LessonScheduleWidget extends StatefulWidget {
  final Function(DateTime, TimeOfDay) onDateTimeChanged;
  final VoidCallback onPlacePressed;
  final DateTime selectedDate;
  final Place selectedPlace;
  final PrivateLesson event;

  const LessonScheduleWidget({
    Key key,
    @required this.onDateTimeChanged,
    @required this.onPlacePressed,
    @required this.event,
    this.selectedDate,
    this.selectedPlace,
  }) : super(key: key);

  @override
  _LessonScheduleWidgetState createState() => _LessonScheduleWidgetState();
}

class _LessonScheduleWidgetState extends State<LessonScheduleWidget> {
  DateTime selectedDate = DateTime.now();
  GlobalKey key = GlobalKey();
  TimeOfDay selectedTime;

  @override
  void initState() {
    super.initState();
    selectedDate = widget.selectedDate ?? DateTime.now();
  }

  @override
  Widget build(BuildContext context) {
    return SliverList(
      delegate: SliverChildListDelegate([
        SizedBox(height: 10),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: _label('LESSON DATE'),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: _dateTimeButton(
            DateFormat('MMMM dd, yyyy').format(selectedDate),
            Icons.calendar_today,
            _onDatePressed,
            false,
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: _label('LESSON TIME'),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: _dateTimeButton(
            selectedTime == null ? 'Select Time' : selectedTime.format(context),
            Icons.access_time,
            _onTimePressed,
            selectedTime == null,
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: _label('WHERE TO MEET'),
        ),
        Padding(
          key: key,
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: placeField(
            widget.selectedPlace == null ? 'Select a place' : widget.selectedPlace.name,
            Icons.arrow_drop_down,
            () => widget.onPlacePressed(),
            widget.selectedPlace == null,
          ),
        ),
        SizedBox(height: 40)
      ]),
    );
  }

  _label(String text) {
    return Padding(
      padding: EdgeInsets.only(top: 24, bottom: 14),
      child: Text(text, style: lessonLabelTextStyle),
    );
  }

  _dateTimeButton(String text, IconData icon, VoidCallback onPressed, bool isHint) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: lightGrayColor,
              width: 1.0,
            ),
          ),
        ),
        padding: EdgeInsets.only(top: 14, bottom: 14),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(child: Text(text, style: isHint ? lessonHintTextStyle : lessonTextStyle)),
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Icon(
                icon,
                color: inputHintColor,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget placeField(String text, IconData icon, VoidCallback onPressed, bool isHint) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: lightGrayColor,
              width: 1.0,
            ),
          ),
        ),
        padding: EdgeInsets.only(top: 14, bottom: 14),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Icon(
                Icons.location_on,
                color: inputHintColor,
              ),
            ),
            Expanded(
                child: Text(
              text,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: isHint ? lessonHintTextStyle : lessonTextStyle,
            )),
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Icon(icon),
            )
          ],
        ),
      ),
    );
  }

  _onDatePressed() async {
    final result = await ChooseDateScreen.push(context, selectedDate: selectedDate);
    if (result == null) return;
    setState(() {
      if (selectedDate != result) selectedTime = null;
      selectedDate = result;
    });
    _notifyDateTimeChanged();
  }

  _onTimePressed() async {
    final result = await ChooseTimeScreen.push(
      context,
      widget.event.lesson,
      selectedDate,
      selectedTime,
      true,
    );
    if (result == null) return;
    setState(() => selectedTime = result);
    _notifyDateTimeChanged();
  }

  _notifyDateTimeChanged() {
    if (widget.onDateTimeChanged != null) {
      widget.onDateTimeChanged(selectedDate, selectedTime);
    }
  }
}
