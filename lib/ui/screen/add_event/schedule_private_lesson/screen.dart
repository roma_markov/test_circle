import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/l10n/localization.dart';
import 'package:lesson_scheduling_pga/logic/bloc/bloc.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/place.dart';
import 'package:lesson_scheduling_pga/logic/models/private_lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:lesson_scheduling_pga/ui/screen/place/screen.dart';

import 'lesson_schedule_widget.dart';

class SchedulePrivateLessonScreen extends StatefulWidget {
  static const String route = '/SchedulePivateLessonScreen';
  final int lessonId;
  final Student student;
  final DateTime date;

  SchedulePrivateLessonScreen._init(this.lessonId, {this.student, this.date});

  factory SchedulePrivateLessonScreen.fromArgs(Map<String, Object> args) {
    return SchedulePrivateLessonScreen._init(
      args["lessonId"],
      student: args["student"],
      date: args["date"],
    );
  }

  static Future push(
    BuildContext context,
    int lessonId, {
    Student student,
    DateTime date,
  }) {
    return Navigator.of(context).pushNamed(
      SchedulePrivateLessonScreen.route,
      arguments: {
        'lessonId': lessonId,
        'student': student,
        'date': date,
      },
    );
  }

  @override
  _SchedulePrivateLessonScreenState createState() => _SchedulePrivateLessonScreenState();
}

class _SchedulePrivateLessonScreenState extends State<SchedulePrivateLessonScreen> {
  PrivateLesson event = PrivateLesson.withStartDate(DateTime.now());
  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime;
  Place selectedPlace;

  @override
  void initState() {
    super.initState();
    if (widget.student != null) event.players.add(widget.student);
    if (widget.date != null) event.start = widget.date;
    selectedDate = event.start;
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<BLoC>(context);
    provider.place.requestPlaces();
    return StreamBuilder<bool>(
        initialData: false,
        stream: provider.privateLesson.progressStream,
        builder: (context, snapshot) {
          return IgnorePointer(
            ignoring: snapshot.data,
            child: _content(snapshot.data),
          );
        });
  }

  _content(bool progress) {
    final bloc = Provider.of<BLoC>(context).lesson;
    bloc.lesson(widget.lessonId);
    return StreamBuilder<Lesson>(
        stream: bloc.lessonDetailsStream,
        builder: (context, snapshot) {
          if (snapshot.data == null) return Container();
          final lesson = snapshot.data;
          event.lesson = lesson;
          return WillPopScope(
            onWillPop: () async {
              _onClosePressed();
              return false;
            },
            child: Scaffold(
              backgroundColor: appWhiteColor,
              appBar: _appBar(lesson),
              body: Stack(
                fit: StackFit.expand,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 60),
                    child: CustomScrollView(
                      slivers: <Widget>[
                        LessonScheduleWidget(
                          selectedDate: widget.date,
                          event: event,
                          selectedPlace: selectedPlace,
                          onDateTimeChanged: _onDateTimeChanged,
                          onPlacePressed: _onPlacePressed,
                        ),
                        SliverSectionHeader(text: '${event.players.length} PLAYERS'),
                        AddedPlayersWidget(
                          addedPlayers: event.players,
                          onPlayersChanged: _onPlayersChanged,
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: AppButton(
                      title: 'PUBLISH',
                      enabled: _canSchedule(),
                      onPressed: _onSavePressed,
                    ),
                  ),
                  Positioned(
                    left: 0,
                    right: 0,
                    top: 0,
                    height: 5,
                    child: progress ? LinearProgressIndicator() : Container(),
                  ),
                ],
              ),
            ),
          );
        });
  }

  _appBar(Lesson lesson) {
    return AppBar(
      titleSpacing: 0,
      brightness: Brightness.light,
      leading: IconButton(
        icon: Icon(Icons.close),
        onPressed: _onClosePressed,
      ),
      bottom: PreferredSize(
        preferredSize: Size(MediaQuery.of(context).size.width, 50),
        child: Container(
          padding: EdgeInsets.only(left: 32, bottom: 12),
          width: MediaQuery.of(context).size.width,
          height: 50,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    width: 8,
                    height: 8,
                    decoration: BoxDecoration(
                      color: appRedColor,
                      shape: BoxShape.circle,
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: Text(
                        lesson.name,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                        style: lessonItemNameStyle,
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 24.0),
                child: Text(
                  AppLocalizations.of(context).hours(lesson.durationHours).toUpperCase(),
                  textAlign: TextAlign.left,
                  style: appBarLightSubTextStyle,
                ),
              ),
            ],
          ),
        ),
      ),
      iconTheme: IconThemeData(color: inputHintColor),
      backgroundColor: lightAppBarColor,
      title: Container(
        alignment: Alignment.centerLeft,
        child: Text(
          'SCHEDULE PRIVATE LESSON',
          style: appBarLightTextStyle,
        ),
      ),
    );
  }

  _onPlayersChanged(List<Student> players) {
    setState(() {
      event.players = players;
    });
  }

  _canSchedule() {
    return event?.lesson != null && event?.players != null && event.players.length > 0 && selectedTime != null;
  }

  _onClosePressed() async {
    final result = await QuitSchedulingScreen.push(context, mode: QuitSchedulingMode.lesson);
    if (result.quit) {
      Navigator.of(context).popUntil((route) => route.settings.name == HomeScreen.route);
    }
  }

  _onSavePressed(BuildContext context) async {
    event.start = DateTime(
      selectedDate.year,
      selectedDate.month,
      selectedDate.day,
      selectedTime.hour,
      selectedTime.minute,
    );
    event.place = selectedPlace;
    final bloc = Provider.of<BLoC>(context).privateLesson;
    final result = await bloc.createPrivateLesson(event);
    if (result.isSuccess) {
      Navigator.of(context).popUntil((route) => route.settings.name == HomeScreen.route);
    }
  }

  _onDateTimeChanged(DateTime date, TimeOfDay time) {
    selectedTime = time;
    selectedDate = date;
    setState(() {});
  }

  _onPlacePressed() async {
    final place = await PlacesScreen.push(context);
    if (place != null) setState(() => selectedPlace = place);
  }
}
