import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';

class ScheduleProgramListScreen extends StatefulWidget {
  static const String route = '/ScheduleProgramListScreen';

  ScheduleProgramListScreen._init();

  factory ScheduleProgramListScreen.fromArgs(Map<String, Object> args) {
    return ScheduleProgramListScreen._init();
  }

  static Future push(BuildContext context) {
    return Navigator.of(context).pushNamed(
      ScheduleProgramListScreen.route,
      arguments: Map<String, dynamic>(),
    );
  }

  @override
  _ScheduleProgramListScreenState createState() => _ScheduleProgramListScreenState();
}

class _ScheduleProgramListScreenState extends State<ScheduleProgramListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: ProgramsWidget(
        onPressed: _onProgramPressed,
      ),
    );
  }

  _appBar() {
    return AppBar(
      titleSpacing: 0,
      brightness: Brightness.light,
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.add),
          onPressed: _onAddPressed,
        )
      ],
      iconTheme: IconThemeData(color: inputHintColor),
      backgroundColor: lightAppBarColor,
      title: Text(
        'SCHEDULE PROGRAM',
        style: appBarLightTextStyle,
      ),
    );
  }

  _onAddPressed() {
    ProgramCreateInfoScreen.push(context);
  }

  _onProgramPressed(Program item) {
    ProgramCreateInfoScreen.push(context, program: item);
  }
}
