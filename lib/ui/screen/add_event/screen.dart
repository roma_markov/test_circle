import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';

class AddEventScreen extends StatefulWidget {
  static const String route = '/AddEventScreen';
  final DateTime selectedDate;

  AddEventScreen._init(this.selectedDate);

  factory AddEventScreen.fromArgs(Map<String, Object> args) {
    return AddEventScreen._init(args['selectedDate']);
  }

  static Future push(BuildContext context, {DateTime selectedDate}) {
    return Navigator.of(context).pushNamed(
      AddEventScreen.route,
      arguments: {'selectedDate': selectedDate ?? DateTime.now()},
    );
  }

  @override
  _AddEventScreenState createState() => _AddEventScreenState();
}

class _AddEventScreenState extends State<AddEventScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        titleSpacing: 0,
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: _onClosePressed,
        ),
        iconTheme: IconThemeData(color: inputHintColor),
        backgroundColor: lightAppBarColor,
        title: Text(
          'NEW EVENT',
          style: appBarLightTextStyle,
        ),
      ),
      body: Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _button('Schedule lesson', _onScheduleLessonPressed),
            SizedBox(height: 20),
            _button('Schedule program', _onScheduleProgramPressed),
          ],
        ),
      ),
    );
  }

  _button(String text, VoidCallback onPressed) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Material(
        color: appWhiteColor,
        child: InkWell(
          onTap: onPressed,
          child: Container(
            height: 140,
            alignment: Alignment.center,
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: fontMerriweatherBold,
                fontSize: 20,
                color: appBlackColor,
              ),
            ),
          ),
        ),
      ),
    );
  }

  _onScheduleLessonPressed() async {
    final result = await ScheduleLessonListScreen.push(context);
    if (result != null) {
      Navigator.of(context).pop(result);
      SchedulePrivateLessonScreen.push(context, result.id, date: widget.selectedDate);
    }
  }

  _onScheduleProgramPressed() {
    ScheduleProgramListScreen.push(context);
  }

  _onClosePressed() {
    Navigator.of(context).pop();
  }
}
