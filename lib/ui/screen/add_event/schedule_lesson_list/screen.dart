import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/create/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/details/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/list/lessons_widget.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

class ScheduleLessonListScreen extends StatefulWidget {
  static const String route = '/ScheduleLessonListScreen';

  ScheduleLessonListScreen._init();

  factory ScheduleLessonListScreen.fromArgs(Map<String, Object> args) {
    return ScheduleLessonListScreen._init();
  }

  static Future push(BuildContext context) {
    return Navigator.of(context).pushNamed(
      ScheduleLessonListScreen.route,
      arguments: Map<String, dynamic>(),
    );
  }

  @override
  _ScheduleLessonListScreenState createState() => _ScheduleLessonListScreenState();
}

class _ScheduleLessonListScreenState extends State<ScheduleLessonListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: LessonsWidget(onPressed: _onLessonPressed),
    );
  }

  _appBar() {
    return AppBar(
      titleSpacing: 0,
      brightness: Brightness.light,
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.add),
          onPressed: _onAddPressed,
        )
      ],
      iconTheme: IconThemeData(color: inputHintColor),
      backgroundColor: lightAppBarColor,
      title: Text(
        'ADD PRIVATE LESSON',
        style: appBarLightTextStyle,
      ),
    );
  }

  _onAddPressed() {
    LessonCreateScreen.push(context);
  }

  _onLessonPressed(Lesson item) async {
    final result = await LessonDetailsScreen.push(context, item.id, mode: LessonDetailsMode.schedule);
    if (result != null) {
      Navigator.of(context).pop(result);
    }
  }
}
