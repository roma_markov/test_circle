import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/general/create_program_general.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/schedule_lesson/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_4/screen.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';

import '../../../../index.dart';
import 'added_lessons_widget.dart';

class ProgramCreateAddLessonsScreen extends StatefulWidget {
  static const String route = '/ProgramCreateAddLessonsScreen';
  final Program program;

  ProgramCreateAddLessonsScreen._init(this.program);

  factory ProgramCreateAddLessonsScreen.fromArgs(Map<String, Object> args) {
    return ProgramCreateAddLessonsScreen._init(args['program']);
  }

  static Future push(BuildContext context, Program program) {
    return Navigator.of(context).pushNamed(
      ProgramCreateAddLessonsScreen.route,
      arguments: {'program': program},
    );
  }

  @override
  _ProgramCreateAddLessonsScreenState createState() => _ProgramCreateAddLessonsScreenState();
}

class _ProgramCreateAddLessonsScreenState extends State<ProgramCreateAddLessonsScreen> with CreateProgramMixin {
  @override
  void initState() {
    super.initState();
    program = widget.program;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appWhiteColor,
      appBar: buildAppBar(3, 'ADD LESSONS', programName: program.name),
      body: Stack(
        fit: StackFit.expand,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 60),
            child: CustomScrollView(
              slivers: <Widget>[
                AddedLessonsWidget(
                  addedLessons: program.lessonsSchedule ?? Map<Lesson, List<Schedule>>(),
                  onLessonsChanged: _onLessonsChanged,
                  onPressed: _onLessonPressed,
                ),
              ],
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: _bottomButtons(),
          )
        ],
      ),
    );
  }

  _bottomButtons() {
    return Container(
      height: 60,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: AppButtonGrey(
              title: '< PROGRAM START',
              enabled: true,
              onPressed: _onBackPressed,
            ),
          ),
          Expanded(
            flex: 1,
            child: AppButton(
              title: 'PLAYERS >',
              enabled: true,
              onPressed: _onAddPlayersPressed,
            ),
          ),
        ],
      ),
    );
  }

  _onAddPlayersPressed(BuildContext context) {
    ProgramCreateAddPlayersScreen.push(
      context,
      program,
    );
  }

  _onLessonsChanged(List<Lesson> lessons) {
    final oldSchedules = program.lessonsSchedule;
    program.lessonsSchedule = Map.fromIterable(
      lessons,
      key: (lesson) => lesson,
      value: (lesson) => oldSchedules[lesson] ?? [],
    );
  }

  _onBackPressed(BuildContext context) {
    Navigator.of(context).pop();
  }

  _onLessonPressed(Lesson lesson) {
    ScheduleProgramLessonScreen.push(context, program, lesson);
  }
}
