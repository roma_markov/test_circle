import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

import 'add_lessons/screen.dart';
import 'list_item.dart';

class AddedLessonsWidget extends StatefulWidget {
  final Map<Lesson, List<Schedule>> addedLessons;
  final ValueChanged<List<Lesson>> onLessonsChanged;
  final LessonAddedPressed onPressed;

  const AddedLessonsWidget({
    Key key,
    @required this.addedLessons,
    @required this.onLessonsChanged,
    @required this.onPressed,
  }) : super(key: key);

  @override
  _AddedLessonsWidgetState createState() => _AddedLessonsWidgetState();
}

class _AddedLessonsWidgetState extends State<AddedLessonsWidget> {
  final List<Lesson> selectedLessons = [];

  @override
  void initState() {
    super.initState();
    selectedLessons.addAll(widget.addedLessons.keys ?? []);
  }

  @override
  Widget build(BuildContext context) {
    return selectedLessons.isEmpty ? _emptyListWidget() : _listWidget();
  }

  _emptyListWidget() {
    return SliverList(
      delegate: SliverChildListDelegate([
        SizedBox(height: 24),
        Padding(
          padding: const EdgeInsets.only(left: 16, right: 16, bottom: 24),
          child: Text('Add lessons\nto this program.',
              style: TextStyle(
                fontFamily: fontMerriweatherBold,
                fontSize: 20,
                color: appBlackColor.withOpacity(0.2),
              )),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 16, right: 16, bottom: 24),
          child: Text(
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
              style: TextStyle(
                fontFamily: fontRobotoRegular,
                fontSize: 14,
                color: appBlackColor.withOpacity(0.2),
              )),
        ),
        Padding(
          padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
          child: _addButton(),
        ),
      ]),
    );
  }

  _listWidget() {
    return SliverList(
      delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
        if (index == 0)
          return Padding(
            padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 8),
            child: _addButton(),
          );
        final lesson = selectedLessons[index - 1];
        return LessonAddedListItem(
          lesson: lesson,
          isScheduled: widget.addedLessons[lesson].isNotEmpty,
          indicatorColor: colorById(lesson.id),
          onPressed: widget.onPressed,
          onRemovePressed: _onRemoveLessonPressed,
        );
      }, childCount: selectedLessons.length + 1),
    );
  }

  _addButton() {
    return AppButtonDashed(
      onPressed: _onAddLessonsPressed,
      iconData: Icons.add,
      title: 'ADD LESSONS',
    );
  }

  _onRemoveLessonPressed(Lesson lesson) {
    selectedLessons.remove(lesson);
    _notifyPlayersChanged();
  }

  _onAddLessonsPressed(BuildContext context) async {
    final newSelectedLessons = await SelectLessonsScreen.push(context);
    if (newSelectedLessons == null) return;

    final set = selectedLessons.toSet();
    set.addAll(newSelectedLessons);
    selectedLessons.clear();
    selectedLessons.addAll(set);
    _notifyPlayersChanged();
  }

  _notifyPlayersChanged() {
    setState(() {});
    if (widget.onLessonsChanged != null) {
      widget.onLessonsChanged(selectedLessons);
    }
  }
}
