import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/create/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/details/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/list/lessons_widget.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

class SelectLessonsScreen extends StatefulWidget {
  static const String route = '/SelectLessonsScreen';

  SelectLessonsScreen._init();

  factory SelectLessonsScreen.fromArgs(Map<String, Object> args) {
    return SelectLessonsScreen._init();
  }

  static Future push(BuildContext context) {
    return Navigator.of(context).pushNamed(
      SelectLessonsScreen.route,
      arguments: Map<String, Object>(),
    );
  }

  @override
  _SelectLessonsScreenState createState() => _SelectLessonsScreenState();
}

class _SelectLessonsScreenState extends State<SelectLessonsScreen> {
  bool isSelectionMode = false;
  final Set<Lesson> selectedLessons = Set();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: isSelectionMode ? _selectionAppBar() : _defaultAppBar(),
      body: Stack(
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: isSelectionMode ? 60 : 0),
            child: LessonsWidget(
              selectedLessons: selectedLessons,
              onPressed: _onLessonPressed,
              onLongPressed: _onLessonLoginPressed,
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: isSelectionMode
                ? AppButton(
                    title: 'ADD ${selectedLessons.length} LESSON',
                    onPressed: _addLessonsPressed,
                  )
                : Container(),
          )
        ],
      ),
    );
  }

  _defaultAppBar() {
    return AppBar(
      brightness: Brightness.light,
      titleSpacing: 0,
      iconTheme: IconThemeData(color: inputHintColor),
      backgroundColor: lightAppBarColor,
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.add),
          onPressed: _onAddPressed,
        )
      ],
      title: Text(
        'ADD LESSONS',
        style: appBarLightTextStyle,
      ),
    );
  }

  _onAddPressed() {
    LessonCreateScreen.push(context);
  }

  _selectionAppBar() {
    return AppBar(
      titleSpacing: 0,
      leading: IconButton(
        icon: Icon(Icons.close),
        onPressed: _onClosePressed,
      ),
      backgroundColor: appNavigationColor,
      title: Text(
        selectedLessons.length.toString(),
        style: appBarDarkTextStyle,
      ),
    );
  }

  _onLessonPressed(Lesson lesson) {
    if (isSelectionMode) {
      setState(() {
        if (selectedLessons.contains(lesson)) {
          selectedLessons.remove(lesson);
        } else {
          selectedLessons.add(lesson);
        }
        if (selectedLessons.isEmpty) {
          isSelectionMode = false;
        }
      });
    } else {
      _openLessonDetails(lesson);
    }
  }

  _onLessonLoginPressed(Lesson lesson) {
    setState(() {
      isSelectionMode = true;
      selectedLessons.add(lesson);
    });
  }

  _addLessonsPressed(BuildContext context) {
    Navigator.of(context).pop(selectedLessons);
  }

  _onClosePressed() {
    if (isSelectionMode) {
      setState(() {
        selectedLessons.clear();
        isSelectionMode = false;
      });
    } else {
      Navigator.of(context).pop();
    }
  }

  _openLessonDetails(Lesson lesson) async {
    final result = await LessonDetailsScreen.push(
      context,
      lesson.id,
      mode: LessonDetailsMode.selection,
    );
    if (result == null) return;
    Navigator.of(context).pop(Set<Lesson>()..add(result));
  }
}
