import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:lesson_scheduling_pga/l10n/localization.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

typedef LessonAddedPressed = void Function(Lesson item);

class LessonAddedListItem extends StatelessWidget {
  final Color indicatorColor;
  final bool isScheduled;
  final Lesson lesson;
  final LessonAddedPressed onPressed;
  final LessonAddedPressed onRemovePressed;

  const LessonAddedListItem({
    Key key,
    @required this.lesson,
    @required this.onPressed,
    @required this.onRemovePressed,
    @required this.indicatorColor,
    @required this.isScheduled,
  }) : super(key: key);

  Widget contentWidget(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      height: 136,
      child: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Container(
              decoration: BoxDecoration(
                color: appWhiteColor,
                border: Border.all(color: Color(0xFFE5E5E5)),
              ),
            ),
          ),
          Positioned(
            left: 0,
            top: 0,
            bottom: 0,
            child: Container(
              width: 4,
              color: indicatorColor,
            ),
          ),
          Positioned(
            left: 24,
            right: 0,
            top: 27,
            child: Text(
              lesson.name,
              style: lessonItemNameStyle,
            ),
          ),
          Positioned(
            left: 24,
            right: 24,
            top: 52,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                  child: Text(
                    lesson.goal,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: lessonItemGoalStyle,
                  ),
                ),
                Text(
                  '    |    ${AppLocalizations.of(context).hours(lesson.durationHours)}',
                  style: lessonItemGoalStyle,
                )
              ],
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              padding: EdgeInsets.only(left: 24, right: 24, bottom: 8),
              height: 48,
              child: Row(
                children: [
                  Icon(
                    isScheduled ? Icons.event_available : Icons.event_busy,
                    color: isScheduled ? approvedColor : appRedColor,
                  ),
                  SizedBox(width: 12),
                  Text(
                    isScheduled ? 'SCHEDULED' : 'NOT SCHEDULED',
                    style: isScheduled ? approvedTextStyle : warningTextStyle,
                  )
                ],
              ),
            ),
          ),
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                highlightColor: indicatorColor.withOpacity(0.2),
                onTap: () => onPressed(lesson),
                child: Container(),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Slidable(
        child: contentWidget(context),
        actionExtentRatio: 0.25,
        actionPane: SlidableStrechActionPane(),
        closeOnScroll: true,
        secondaryActions: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            color: appRedColor,
            child: IconSlideAction(
              caption: 'Delete',
              color: Colors.transparent,
              icon: Icons.delete,
              onTap: () => onRemovePressed(lesson),
            ),
          ),
        ],
      ),
    );
  }
}
