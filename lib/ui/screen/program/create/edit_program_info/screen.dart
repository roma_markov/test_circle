import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/models/place.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_1/basic_info_widget.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

class EditProgramInfoScreen extends StatefulWidget {
  static const String route = '/EditProgramInfoScreen';
  final Program program;

  EditProgramInfoScreen._init(this.program);

  factory EditProgramInfoScreen.fromArgs(Map<String, Object> args) {
    return EditProgramInfoScreen._init(args["program"]);
  }

  static Future push(BuildContext context, Program program) {
    return Navigator.of(context).pushNamed(
      EditProgramInfoScreen.route,
      arguments: {'program': program},
    );
  }

  @override
  _EditProgramInfoScreenState createState() => _EditProgramInfoScreenState();
}

class _EditProgramInfoScreenState extends State<EditProgramInfoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appWhiteColor,
      appBar: AppBar(
        titleSpacing: 0,
        brightness: Brightness.light,
        iconTheme: IconThemeData(color: inputHintColor),
        backgroundColor: lightAppBarColor,
        title: Text(
          'EDIT PROGRAM BASICS',
          style: appBarLightTextStyle,
        ),
      ),
      body: ProgramCreateInfoWidget(
        program: widget.program,
        buttonTitle: 'SAVE CHANGES',
        onButtonPressed: _onSavePressed,
        performSave: null,
      ),
    );
  }

  _onSavePressed(
    bool createEnabled,
    bool manual,
    String name,
    String goal,
    int duration,
    int capacity,
    Place place,
  ) async {
    widget.program.name = name;
    widget.program.goal = goal;
    widget.program.durationDays = duration;
    widget.program.capacity = capacity;
    widget.program.place = place;
    Navigator.of(context).pop(widget.program);
  }
}
