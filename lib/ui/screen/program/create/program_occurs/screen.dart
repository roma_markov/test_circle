import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/bloc/bloc.dart';
import 'package:lesson_scheduling_pga/logic/models/private_lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/list/list_item.dart';
import 'package:lesson_scheduling_pga/ui/screen/schedule/item_private_lessons.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/block_provider.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/calendar.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/calendar_decoration.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/event_list.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';
import 'package:lesson_scheduling_pga/utils/date_utils.dart';

class ChooseProgramDatesScreen extends StatefulWidget {
  static const String route = '/ChooseProgramDatesScreen';
  final List<DateTime> selectedStartDates;
  final Program program;

  ChooseProgramDatesScreen._init(this.selectedStartDates, this.program);

  factory ChooseProgramDatesScreen.fromArgs(Map<String, Object> args) {
    return ChooseProgramDatesScreen._init(
      args['selectedStartDates'],
      args['program'],
    );
  }

  static Future push(
    BuildContext context,
    Program program, {
    List<DateTime> selectedStartDates,
  }) {
    return Navigator.of(context).pushNamed(
      ChooseProgramDatesScreen.route,
      arguments: {
        'selectedStartDates': selectedStartDates,
        'program': program,
      },
    );
  }

  @override
  _ChooseProgramDatesScreenState createState() => _ChooseProgramDatesScreenState();
}

class _ChooseProgramDatesScreenState extends State<ChooseProgramDatesScreen> {
  List<DateTime> selectedStartDates = [];
  List<DatePair> selectedRanges = [];
  DateTime selectedMonth = DateTime.now();

  @override
  void initState() {
    super.initState();
    if (widget.selectedStartDates != null) {
      selectedStartDates = widget.selectedStartDates;
      selectedStartDates.forEach((start) {
        selectedRanges.add(DatePair(start, start.add(Duration(days: widget.program.durationDays - 1))));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appWhiteColor,
      appBar: _appBar(),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned.fill(bottom: 60, child: _calendar()),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: AppButton(
              title: 'SUBMIT',
              onPressed: _onSetPressed,
            ),
          )
        ],
      ),
    );
  }

  _appBar() {
    return AppBar(
      titleSpacing: 0,
      brightness: Brightness.light,
      leading: IconButton(
        icon: Icon(Icons.close),
        onPressed: _onClosePressed,
      ),
      iconTheme: IconThemeData(color: inputHintColor),
      backgroundColor: lightAppBarColor,
      title: Text(
        'SCHEDULE: ${widget.program.name.toUpperCase()}',
        style: appBarLightTextStyle,
      ),
    );
  }

  _calendar() {
    final bloc = Provider.of<BLoC>(context);
    return Container(
      child: StreamBuilder<EventList>(
        initialData: EventList(),
        stream: bloc.eventsStream(),
        builder: (context, snapshot) {
          final eventList = snapshot.data;
          final thisMonthPrivateEvents = eventList.getPrivateEventsForMonth(selectedMonth);
          final thisMonthProgramEvents = eventList.getProgramsForMonth(selectedMonth);
          int itemsCount = (thisMonthPrivateEvents.isNotEmpty ? 1 : 0) + thisMonthProgramEvents.length;
          return CustomScrollView(
            slivers: <Widget>[
              _calendarSliver(eventList),
              SliverList(
                delegate: SliverChildBuilderDelegate((context, position) {
                  return _monthModeItem(position, thisMonthProgramEvents, thisMonthPrivateEvents);
                }, childCount: itemsCount),
              )
            ],
          );
        },
      ),
    );
  }

  _calendarSliver(EventList eventList) {
    return SliverToBoxAdapter(
      child: Container(
          height: Calendar.calendarHeight(context, 6),
          child: Calendar(
            eventList: eventList,
            selectedDateTimes: selectedStartDates,
            selectedDayButtonColor: colorById(widget.program.id).withOpacity(0.5),
            onDayPressed: _onDayPressed,
            onCalendarChanged: _onMonthChanged,
            decoratorBuilder: _decoratorBuilder,
            weekFormat: false,
          )),
    );
  }

  _monthModeItem(int position, List<Program> programEvents, List<PrivateLesson> privateEvents) {
    if (position == 0 && privateEvents.isNotEmpty) {
      return PrivateLessonsListItem(
        indicatorColor: appRedColor,
        events: privateEvents,
      );
    }
    final programEvent = programEvents[position - (privateEvents.isNotEmpty ? 1 : 0)];
    return ProgramListItem(
      program: programEvent,
      indicatorColor: colorById(programEvent.id),
      onPressed: (Program item) {},
    );
  }

  _onMonthChanged(DateTime date) {
    setState(() => selectedMonth = date);
  }

  _onSetPressed(BuildContext context) {
    Navigator.of(context).pop(selectedStartDates);
  }

  _onClosePressed() {
    Navigator.of(context).pop();
  }

  _onDayPressed(DateTime date) {
    int programDuration = widget.program.durationDays;
    if (selectedStartDates.contains(date)) {
      selectedRanges.removeAt(selectedStartDates.indexOf(date));
      selectedStartDates.remove(date);
    } else {
      if (selectedStartDates.isNotEmpty &&
          selectedStartDates.any((selected) =>
              date.isAfter(selected.subtract(Duration(days: programDuration))) &&
              date.isBefore(selected.add(Duration(days: programDuration))))) return;

      selectedStartDates.add(date);
      selectedRanges.add(DatePair(date, date.add(Duration(days: programDuration - 1))));
    }
    setState(() {});
  }

  Widget _decoratorBuilder(BuildContext context, DateTime day, EventList eventList) {
    return CalendarDecoration.buildDecoration(context, day, eventList, additionalBuilder: (context) {
      return _buildSelectionDecoration(context, day, eventList);
    });
  }

  Widget _buildSelectionDecoration(BuildContext context, DateTime day, EventList eventList) {
    bool isSelected = DateUtils.isInRanges(selectedRanges, day);
    return Stack(
      children: <Widget>[
        Positioned.fill(
          child: Container(
            color: isSelected ? colorById(widget.program.id).withOpacity(0.2) : Colors.transparent,
          ),
        ),
      ],
    );
  }
}
