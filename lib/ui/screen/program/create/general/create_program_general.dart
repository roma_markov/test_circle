import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/logic/bloc/bloc.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/quit_scheduling/screen.dart';
import 'package:lesson_scheduling_pga/ui/shared/block_provider.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

mixin CreateProgramMixin<T extends StatefulWidget> on State<T> {
  Program program;

  PreferredSize buildAppBar(
    int step,
    String title, {
    String programName,
    VoidCallback saveFunction,
  }) {
    double bottomHeight = programName == null ? 30 : 55;
    return PreferredSize(
      preferredSize: Size.fromHeight(bottomHeight + kToolbarHeight - 10),
      child: WillPopScope(
        onWillPop: () async {
          onClosePressed(saveFunction ?? saveProgram);
          return false;
        },
        child: Stack(
          children: <Widget>[
            AppBar(
              brightness: Brightness.light,
              titleSpacing: 0,
              leading: IconButton(
                icon: Icon(Icons.close),
                onPressed: () => onClosePressed(saveFunction ?? saveProgram),
              ),
              iconTheme: IconThemeData(color: inputHintColor),
              backgroundColor: lightAppBarColor,
              title: Container(
                height: 80,
                alignment: Alignment.centerLeft,
                child: Text(
                  title,
                  style: appBarLightTextStyle,
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              child: PreferredSize(
                preferredSize: Size(MediaQuery.of(context).size.width, bottomHeight),
                child: Container(
                  padding: EdgeInsets.only(left: 56, bottom: 12),
                  width: MediaQuery.of(context).size.width,
                  height: bottomHeight,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      programName == null
                          ? Container(height: 0)
                          : Text(
                              programName,
                              textAlign: TextAlign.left,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: lessonItemNameStyle,
                            ),
                      Text(
                        "STEP $step / 5",
                        textAlign: TextAlign.left,
                        style: appBarLightSubTextStyle,
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  onClosePressed(VoidCallback saveFunction) async {
    QuitResult result = await QuitSchedulingScreen.push(context);
    if (result.quit) {
      if (result.save) saveFunction();
      close();
    }
  }

  saveProgram({bool addToEvent = false}) async {
    final provider = Provider.of<BLoC>(context);
    final status = await provider.program.createProgram(program, addToEvent);
    if (status.isSuccess) {
      close();
    }
  }

  close() {
    Navigator.of(context).popUntil((route) {
      return route.settings.name == HomeScreen.route;
    });
  }
}
