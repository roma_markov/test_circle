import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/bloc/bloc.dart';
import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_5/summary_widget.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/block_provider.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

class ProgramEditSummaryScreen extends StatefulWidget {
  static const String route = '/ProgramEditSummaryScreen';
  final Program program;

  ProgramEditSummaryScreen._init(this.program);

  factory ProgramEditSummaryScreen.fromArgs(Map<String, Object> args) {
    return ProgramEditSummaryScreen._init(
      args["program"],
    );
  }

  static Future push(BuildContext context, Program program) {
    return Navigator.of(context).pushNamed(
      ProgramEditSummaryScreen.route,
      arguments: {
        'program': program,
      },
    );
  }

  @override
  _ProgramEditSummaryScreenState createState() => _ProgramEditSummaryScreenState();
}

class _ProgramEditSummaryScreenState extends State<ProgramEditSummaryScreen> {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<BLoC>(context);
    return StreamBuilder<bool>(
        initialData: false,
        stream: provider.program.progressStream,
        builder: (context, snapshot) {
          return IgnorePointer(
            ignoring: snapshot.data,
            child: _content(snapshot.data),
          );
        });
  }

  _content(bool progress) {
    return Scaffold(
      backgroundColor: appWhiteColor,
      appBar: _appBar(),
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            bottom: 60,
            child: ProgramSummaryWidget(program: widget.program),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: AppButton(
              title: 'UPDATE PROGRAM',
              onPressed: _onSavePressed,
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            top: 0,
            height: 5,
            child: progress ? LinearProgressIndicator() : Container(),
          ),
        ],
      ),
    );
  }

  _appBar() {
    return AppBar(
      brightness: Brightness.light,
      titleSpacing: 0,
      leading: IconButton(
        icon: Icon(Icons.close),
        onPressed: _onClosePressed,
      ),
      iconTheme: IconThemeData(color: inputHintColor),
      backgroundColor: lightAppBarColor,
      title: Text(
        'EDIT PROGRAM',
        style: appBarLightTextStyle,
      ),
    );
  }

  _onSavePressed(BuildContext context) async {
    final provider = Provider.of<BLoC>(context);
    final result = await provider.program.editProgram(widget.program);
    if (result.isSuccess) {
      Navigator.of(context).pop();
    }
  }

  _onClosePressed() {
    Navigator.of(context).pop();
  }
}
