import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

enum QuitSchedulingMode {
  program,
  lesson,
}

class QuitSchedulingScreen extends StatefulWidget {
  static const String route = '/QuitScheduliingScreen';
  final QuitSchedulingMode mode;

  QuitSchedulingScreen._init(this.mode);

  factory QuitSchedulingScreen.fromArgs(Map<String, Object> args) {
    return QuitSchedulingScreen._init(args['mode']);
  }

  static Future push(
    BuildContext context, {
    QuitSchedulingMode mode = QuitSchedulingMode.program,
  }) {
    return Navigator.of(context).pushNamed(
      QuitSchedulingScreen.route,
      arguments: {'mode': mode},
    );
  }

  @override
  _QuitSchedulingScreenState createState() => _QuitSchedulingScreenState();
}

class _QuitSchedulingScreenState extends State<QuitSchedulingScreen> {
  bool saveTemplate = true;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        color: appWhiteColor,
        child: Stack(
          children: <Widget>[
            Positioned.fill(
              top: 110,
              left: 32,
              right: 32,
              bottom: 60,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    widget.mode == QuitSchedulingMode.program
                        ? 'Quit scheduling\nof this program?'
                        : 'Quit scheduling\nof this lesson?',
                    style: TextStyle(
                      fontFamily: fontMerriweatherBold,
                      fontSize: 20,
                      color: appBlackColor,
                    ),
                  ),
                  widget.mode == QuitSchedulingMode.program
                      ? Container(
                          margin: EdgeInsets.only(top: 24, bottom: 24),
                          height: 64,
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: _onCheckBoxChanged,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Checkbox(
                                    value: saveTemplate,
                                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                    onChanged: (bool value) => _onCheckBoxChanged(),
                                  ),
                                  Text(
                                    'Save program template',
                                    style: TextStyle(
                                      fontFamily: fontRobotoRegular,
                                      fontSize: 16,
                                      color: appBlackColor,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                      : Container(height: 24),
                  Text(
                    'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius',
                    style: TextStyle(
                      fontFamily: fontRobotoRegular,
                      fontSize: 16,
                      color: appBlackColor,
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              height: 60,
              child: _bottomButtons(),
            ),
          ],
        ),
      ),
    );
  }

  _bottomButtons() {
    return Container(
      height: 60,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: AppButtonGrey(
              title: 'NO',
              enabled: true,
              onPressed: _onNoPressed,
            ),
          ),
          Expanded(
            flex: 1,
            child: AppButton(
              title: 'YES, QUIT',
              enabled: true,
              onPressed: _onYesPressed,
            ),
          ),
        ],
      ),
    );
  }

  _onCheckBoxChanged() {
    setState(() => saveTemplate = !saveTemplate);
  }

  _onYesPressed(BuildContext context) {
    Navigator.of(context).pop(QuitResult(true, saveTemplate));
  }

  _onNoPressed(BuildContext context) {
    Navigator.of(context).pop(QuitResult(false, saveTemplate));
  }
}

class QuitResult {
  final bool quit;
  final bool save;

  QuitResult(this.quit, this.save);
}
