import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/bloc/bloc.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/general/create_program_general.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/program_occurs/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_3/screen.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';

import '../../../../index.dart';
import 'occurs_widget.dart';

class ProgramCreateScheduleStartScreen extends StatefulWidget {
  static const String route = '/ProgramCreateScheduleStartScreen';
  final Program program;

  ProgramCreateScheduleStartScreen._init(this.program);

  factory ProgramCreateScheduleStartScreen.fromArgs(Map<String, Object> args) {
    return ProgramCreateScheduleStartScreen._init(args['program']);
  }

  static Future push(BuildContext context, Program program) {
    return Navigator.of(context).pushNamed(
      ProgramCreateScheduleStartScreen.route,
      arguments: {'program': program},
    );
  }

  @override
  _ProgramCreateScheduleStartScreenState createState() => _ProgramCreateScheduleStartScreenState();
}

class _ProgramCreateScheduleStartScreenState extends State<ProgramCreateScheduleStartScreen> with CreateProgramMixin {
  List<DateTime> selectedStartDates = [];

  @override
  void initState() {
    super.initState();
    selectedStartDates = widget.program.startDates ?? [];
    program = widget.program;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appWhiteColor,
      appBar: buildAppBar(2, 'SCHEDULE PROGRAM START', programName: program.name),
      body: Stack(
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: 60),
            child: CustomScrollView(
              slivers: <Widget>[
                OccursWidget(
                  selectedStartDates: selectedStartDates,
                  onSchedulePressed: _onDatePressed,
                  durationDays: program.durationDays,
                ),
              ],
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: _bottomButtons(),
          )
        ],
      ),
    );
  }

  _bottomButtons() {
    return Container(
      height: 60,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: AppButtonGrey(
              title: '<BASIC INFO',
              enabled: true,
              onPressed: _onBackPressed,
            ),
          ),
          Expanded(
            flex: 1,
            child: AppButton(
              title: 'ADD LESSON >',
              enabled: selectedStartDates.isNotEmpty,
              onPressed: _onSavePressed,
            ),
          ),
        ],
      ),
    );
  }

  _onDatePressed() async {
    final result = await ChooseProgramDatesScreen.push(
      context,
      program,
      selectedStartDates: selectedStartDates,
    );
    if (result != null) {
      selectedStartDates = result;
      program.startDates = result;
      Provider.of<BLoC>(context).program.updateScheduledLessons(program);
      setState(() {});
    }
  }

  _onSavePressed(BuildContext context) async {
    ProgramCreateAddLessonsScreen.push(context, program);
  }

  _onBackPressed(BuildContext context) {
    Navigator.of(context).pop();
  }
}
