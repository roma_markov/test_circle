import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

class OccursWidget extends StatelessWidget {
  final List<DateTime> selectedStartDates;
  final int durationDays;
  final VoidCallback onSchedulePressed;

  const OccursWidget({
    Key key,
    @required this.selectedStartDates,
    @required this.durationDays,
    @required this.onSchedulePressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverList(
        delegate: SliverChildListDelegate(
      [
        _label('PROGRAM OCCURS ON'),
        _periodItems(),
        Padding(
          padding: EdgeInsets.only(left: 32, right: 32, top: 24, bottom: 14),
          child: Text(
            'If the program is periodic, select several dates.',
            style: TextStyle(
              fontFamily: fontRobotoRegular,
              fontSize: 14,
              color: appBlackColor.withOpacity(0.2),
            ),
          ),
        ),
        _dateButton(),
        SizedBox(
          height: 44,
        )
      ],
    ));
  }

  _periodItems() {
    final result = <Widget>[];
    selectedStartDates.forEach((start) {
      final end = start.add(Duration(days: durationDays));
      result.add(Padding(
        padding: EdgeInsets.only(left: 32, right: 32, top: 8, bottom: 8),
        child: Text(
          '${DateFormat.MMMMd().format(start)} - ${DateFormat.MMMMd().format(end)}',
          style: fieldTextStyle(),
        ),
      ));
    });
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: result,
    );
  }

  _label(String text) {
    return Padding(
      padding: EdgeInsets.only(left: 32, right: 32, top: 24, bottom: 14),
      child: Text(text, style: lessonLabelTextStyle),
    );
  }

  _dateButton() {
    return Padding(
      padding: EdgeInsets.only(left: 32, right: 32, top: 24),
      child: AppButtonGrey(
        title: 'OPEN SCHEDULE',
        iconData: Icons.calendar_today,
        color: inputHintColor,
        textColor: appWhiteColor,
        enabled: true,
        onPressed: (_) => onSchedulePressed(),
      ),
    );
  }
}
