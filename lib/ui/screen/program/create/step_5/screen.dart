import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/general/create_program_general.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_5/summary_widget.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';

class ProgramCreateSummaryScreen extends StatefulWidget {
  static const String route = '/ProgramCreateSummaryScreen';
  final Program program;

  ProgramCreateSummaryScreen._init(this.program);

  factory ProgramCreateSummaryScreen.fromArgs(Map<String, Object> args) {
    return ProgramCreateSummaryScreen._init(
      args["program"],
    );
  }

  static Future push(BuildContext context, Program program) {
    return Navigator.of(context).pushNamed(
      ProgramCreateSummaryScreen.route,
      arguments: {
        'program': program,
      },
    );
  }

  @override
  _ProgramCreateSummaryScreenState createState() => _ProgramCreateSummaryScreenState();
}

class _ProgramCreateSummaryScreenState extends State<ProgramCreateSummaryScreen> with CreateProgramMixin {
  @override
  void initState() {
    super.initState();
    program = widget.program;
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<BLoC>(context);
    return StreamBuilder<bool>(
        initialData: false,
        stream: provider.program.progressStream,
        builder: (context, snapshot) {
          return IgnorePointer(
            ignoring: snapshot.data,
            child: _content(snapshot.data),
          );
        });
  }

  _content(bool progress) {
    return Scaffold(
      backgroundColor: appWhiteColor,
      appBar: buildAppBar(5, 'PROGRAM SUMMARY'),
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            bottom: 60,
            child: ProgramSummaryWidget(program: program),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: AppButton(
              title: 'PUBLISH',
              onPressed: _onSavePressed,
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            top: 0,
            height: 5,
            child: progress ? LinearProgressIndicator() : Container(),
          )
        ],
      ),
    );
  }

  _onSavePressed(BuildContext context) async {
    await saveProgram(addToEvent: true);
  }
}
