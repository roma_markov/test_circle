import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/l10n/localization.dart';
import 'package:lesson_scheduling_pga/logic/bloc/bloc.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/edit_program_info/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/program_occurs/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/schedule_lesson/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_2/occurs_widget.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_3/added_lessons_widget.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_4/added_players_widget.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/details/general_info_widget.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/calendar.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/calendar_decoration.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/event_list.dart';
import 'package:lesson_scheduling_pga/ui/shared/sliver_section_header.dart';
import 'package:lesson_scheduling_pga/utils/date_utils.dart';

class ProgramSummaryWidget extends StatefulWidget {
  final Program program;

  const ProgramSummaryWidget({Key key, @required this.program}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ProgramSummaryWidgetState();
}

class _ProgramSummaryWidgetState extends State<ProgramSummaryWidget> {
  List<DatePair> selectedRanges = [];

  _buildDays(Program program) {
    selectedRanges.clear();
    final selectedStartDates = program.startDates;
    selectedStartDates.forEach((start) {
      selectedRanges.add(DatePair(start, start.add(Duration(days: program.durationDays - 1))));
    });
  }

  @override
  Widget build(BuildContext context) {
    _buildDays(widget.program);
    return CustomScrollView(slivers: _slivers(widget.program));
  }

  _slivers(Program program) {
    return <Widget>[
      SliverList(
        delegate: SliverChildListDelegate([
          ProgramGeneralInfoWidget(program: program),
          SizedBox(height: 28),
          Padding(
            padding: const EdgeInsets.only(left: 32, right: 32, bottom: 44),
            child: AppButtonGrey(
              title: 'EDIT',
              iconData: Icons.edit,
              color: inputHintColor,
              textColor: appWhiteColor,
              enabled: true,
              onPressed: _onEditInfoPressed,
            ),
          ),
        ]),
      ),
      SliverSectionHeader(text: 'SCHEDULING INFORMATION'),
      OccursWidget(
        selectedStartDates: program.startDates ?? [],
        durationDays: program.durationDays,
        onSchedulePressed: _onDatePressed,
      ),
      _calendarSliver(),
      SliverSectionHeader(
        text: AppLocalizations.of(context).lessons(program.lessonsSchedule.length).toUpperCase(),
      ),
      AddedLessonsWidget(
        addedLessons: program.lessonsSchedule ?? Map<Lesson, List<Schedule>>(),
        onLessonsChanged: _onLessonChanged,
        onPressed: _onLessonPressed,
      ),
      SliverSectionHeader(
        text: 'PLAYERS (${program.players?.length ?? 0}/${program.capacity})',
      ),
      AddedPlayersWidget(
        addedPlayers: program.players,
        maxCount: program.capacity,
        onPlayersChanged: _onPlayersChanged,
      )
    ];
  }

  _calendarSliver() {
    final bloc = Provider.of<BLoC>(context);
    return SliverList(
      delegate: SliverChildListDelegate.fixed([
        StreamBuilder<EventList>(
            initialData: EventList(),
            stream: bloc.eventsStream(),
            builder: (context, snapshot) {
              final eventList = snapshot.data;
              return Container(
                  height: Calendar.calendarHeight(context, 6),
                  child: Calendar(
                    decoratorBuilder: _buildDecoration,
                    eventList: eventList,
                    weekFormat: false,
                  ));
            }),
      ]),
    );
  }

  Widget _buildDecoration(BuildContext context, DateTime day, EventList eventList) {
    return CalendarDecoration.buildDecoration(
      context,
      day,
      eventList,
      activePrograms: [widget.program],
      style: CalendarDecorationStyle.grayed,
      additionalBuilder: (context) {
        return _buildCurrentProgramIndicators(day);
      },
    );
  }

  _buildCurrentProgramIndicators(DateTime day) {
    final show = DateUtils.isInRanges(selectedRanges, day);
    return Container(
      constraints: BoxConstraints.tightForFinite(),
      color: show ? lightGrayColor.withOpacity(0.2) : Colors.transparent,
    );
  }

  _onDatePressed() async {
    final result = await ChooseProgramDatesScreen.push(
      context,
      widget.program,
      selectedStartDates: widget.program.startDates,
    );
    if (result != null) {
      widget.program.startDates = result;
      Provider.of<BLoC>(context).program.updateScheduledLessons(widget.program);
      setState(() {});
    }
  }

  _onEditInfoPressed(BuildContext context) {
    EditProgramInfoScreen.push(context, widget.program);
  }

  _onLessonChanged(List<Lesson> lessons) {
    setState(() {
      final oldSchedules = widget.program.lessonsSchedule;
      widget.program.lessonsSchedule = Map.fromIterable(
        lessons,
        key: (lesson) => lesson,
        value: (lesson) => oldSchedules[lesson] ?? [],
      );
    });
  }

  _onLessonPressed(Lesson lesson) {
    ScheduleProgramLessonScreen.push(context, widget.program, lesson);
  }

  _onPlayersChanged(List<Student> value) {
    setState(() => widget.program.players = value);
  }
}
