import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

typedef StudentAddedPressed = void Function(Student item);

class StudentAddedListItem extends StatelessWidget {
  final Student student;
  final StudentAddedPressed onPressed;
  final StudentAddedPressed onRemovePressed;

  const StudentAddedListItem({
    Key key,
    @required this.student,
    @required this.onPressed,
    @required this.onRemovePressed,
  }) : super(key: key);

  Widget contentWidget(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      height: 112,
      child: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Container(
              decoration: BoxDecoration(
                color: appWhiteColor,
                border: Border.all(color: Color(0xFFE5E5E5)),
              ),
            ),
          ),
          Positioned(
            left: 24,
            right: 0,
            top: 27,
            child: Text(
              '${student.firstName} ${student.lastName}',
              style: lessonItemNameStyle,
            ),
          ),
          Positioned(
            left: 24,
            right: 0,
            top: 52,
            child: Text(
              student.email,
              style: lessonItemGoalStyle,
            ),
          ),
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () => onPressed(student),
                child: Container(),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Slidable(
        child: contentWidget(context),
        actionExtentRatio: 0.25,
        actionPane: SlidableStrechActionPane(),
        closeOnScroll: true,
        secondaryActions: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            color: appRedColor,
            child: IconSlideAction(
              caption: 'Delete',
              color: Colors.transparent,
              icon: Icons.delete,
              onTap: () => onRemovePressed(student),
            ),
          ),
        ],
      ),
    );
  }
}
