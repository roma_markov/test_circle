import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';
import 'package:lesson_scheduling_pga/ui/screen/student/details/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/student/list/students_widget.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

class SelectPlayerScreen extends StatefulWidget {
  static const String route = '/SelectPlayerScreen';
  final int maxCount;

  SelectPlayerScreen._init(this.maxCount);

  factory SelectPlayerScreen.fromArgs(Map<String, Object> args) {
    return SelectPlayerScreen._init(args['maxCount']);
  }

  static Future push(BuildContext context, {int maxCount}) {
    return Navigator.of(context).pushNamed(
      SelectPlayerScreen.route,
      arguments: {
        'maxCount': maxCount,
      },
    );
  }

  @override
  _SelectPlayerScreenState createState() => _SelectPlayerScreenState();
}

class _SelectPlayerScreenState extends State<SelectPlayerScreen> {
  bool isSelectionMode = false;
  final Set<Student> selectedStudents = Set();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: isSelectionMode ? _selectionAppBar() : _defaultAppBar(),
      body: Stack(
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: isSelectionMode ? 60 : 0),
            child: StudentsWidget(
              selectedStudents: selectedStudents,
              onPressed: _onStudentPressed,
              onLongPressed: _onStudentLoginPressed,
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: isSelectionMode
                ? AppButton(
                    title: 'ADD ${selectedStudents.length} PLAYER',
                    onPressed: _addStudentsPressed,
                  )
                : Container(),
          )
        ],
      ),
    );
  }

  _defaultAppBar() {
    return AppBar(
      brightness: Brightness.light,
      titleSpacing: 0,
      iconTheme: IconThemeData(color: inputHintColor),
      backgroundColor: lightAppBarColor,
      title: Text(
        'ADD PLAYERS',
        style: appBarLightTextStyle,
      ),
    );
  }

  _selectionAppBar() {
    return AppBar(
      titleSpacing: 0,
      leading: IconButton(
        icon: Icon(Icons.close),
        onPressed: _onClosePressed,
      ),
      backgroundColor: appNavigationColor,
      title: Text(
        (widget.maxCount != null)
            ? '${selectedStudents.length.toString()}/${widget.maxCount}'
            : selectedStudents.length.toString(),
        style: appBarDarkTextStyle,
      ),
    );
  }

  _onStudentPressed(Student student) {
    if (isSelectionMode) {
      setState(() {
        if (selectedStudents.contains(student)) {
          selectedStudents.remove(student);
        } else {
          if (widget.maxCount == null || selectedStudents.length < widget.maxCount) selectedStudents.add(student);
        }
        if (selectedStudents.isEmpty) {
          isSelectionMode = false;
        }
      });
    } else {
      _openStudentDetails(student);
    }
  }

  _onStudentLoginPressed(Student student) {
    setState(() {
      isSelectionMode = true;
      selectedStudents.add(student);
    });
  }

  _addStudentsPressed(BuildContext context) {
    Navigator.of(context).pop(selectedStudents);
  }

  _onClosePressed() {
    if (isSelectionMode) {
      setState(() {
        selectedStudents.clear();
        isSelectionMode = false;
      });
    } else {
      Navigator.of(context).pop();
    }
  }

  _openStudentDetails(Student student) async {
    final result = await StudentDetailsScreen.push(
      context,
      student,
      mode: StudentDetailsMode.selection,
    );
    if (result == null) return;
    Navigator.of(context).pop(Set<Student>()..add(result));
  }
}
