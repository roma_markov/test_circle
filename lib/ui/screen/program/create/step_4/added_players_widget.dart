import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

import 'add_player/screen.dart';
import 'list_item.dart';

class AddedPlayersWidget extends StatefulWidget {
  final List<Student> addedPlayers;
  final int maxCount;
  final ValueChanged<List<Student>> onPlayersChanged;

  const AddedPlayersWidget({
    Key key,
    @required this.addedPlayers,
    this.onPlayersChanged,
    this.maxCount,
  }) : super(key: key);

  @override
  _AddedPlayersWidgetState createState() => _AddedPlayersWidgetState();
}

class _AddedPlayersWidgetState extends State<AddedPlayersWidget> {
  final List<Student> selectedPlayers = [];
  final buttonPadding = const EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 8);

  @override
  void initState() {
    super.initState();
    selectedPlayers.addAll(widget.addedPlayers ?? []);
  }

  @override
  Widget build(BuildContext context) {
    return selectedPlayers.isEmpty ? _emptyListWidget() : _listWidget();
  }

  _emptyListWidget() {
    return SliverList(
      delegate: SliverChildListDelegate([
        SizedBox(height: 24),
        Padding(
          padding: const EdgeInsets.only(left: 16, right: 16, bottom: 24),
          child: Text('Add players\nto this program.',
              style: TextStyle(
                fontFamily: fontMerriweatherBold,
                fontSize: 20,
                color: appBlackColor.withOpacity(0.2),
              )),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 16, right: 16, bottom: 24),
          child: Text(
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
              style: TextStyle(
                fontFamily: fontRobotoRegular,
                fontSize: 14,
                color: appBlackColor.withOpacity(0.2),
              )),
        ),
        Padding(
          padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 19),
          child: _addButton(),
        ),
      ]),
    );
  }

  _listWidget() {
    return SliverList(
      delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
        if (index == 0) return Padding(padding: buttonPadding, child: _addButton());
        final lesson = selectedPlayers[index - 1];
        return StudentAddedListItem(
          student: lesson,
          onPressed: (_) {},
          onRemovePressed: _onRemovePlayerPressed,
        );
      }, childCount: selectedPlayers.length + 1),
    );
  }

  _addButton() {
    return AppButtonDashed(
      onPressed: _onAddPlayersPressed,
      iconData: Icons.add,
      enabled: (widget.maxCount == null || selectedPlayers.length < widget.maxCount),
      title: 'ADD PLAYERS',
    );
  }

  _onRemovePlayerPressed(Student student) {
    selectedPlayers.remove(student);
    _notifyPlayersChanged();
  }

  _onAddPlayersPressed(BuildContext context) async {
    final maxCount = widget.maxCount != null ? widget.maxCount - selectedPlayers.length : null;
    final newSelectedPlayers = await SelectPlayerScreen.push(
      context,
      maxCount: maxCount,
    );
    if (newSelectedPlayers == null) return;

    final set = selectedPlayers.toSet();
    set.addAll(newSelectedPlayers);
    selectedPlayers.clear();
    selectedPlayers.addAll(set);

    _notifyPlayersChanged();
  }

  _notifyPlayersChanged() {
    setState(() {});
    if (widget.onPlayersChanged != null) {
      widget.onPlayersChanged(selectedPlayers);
    }
  }
}
