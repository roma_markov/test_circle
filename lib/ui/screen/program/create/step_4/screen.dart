import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/general/create_program_general.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_5/screen.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

import 'added_players_widget.dart';

class ProgramCreateAddPlayersScreen extends StatefulWidget {
  static const String route = '/ProgramCreateAddPlayersScreen';

  final Program program;

  ProgramCreateAddPlayersScreen._init(this.program);

  factory ProgramCreateAddPlayersScreen.fromArgs(Map<String, Object> args) {
    return ProgramCreateAddPlayersScreen._init(args['program']);
  }

  static Future push(BuildContext context, Program program) {
    return Navigator.of(context).pushNamed(
      ProgramCreateAddPlayersScreen.route,
      arguments: {'program': program},
    );
  }

  @override
  _ProgramCreateAddPlayersScreenState createState() => _ProgramCreateAddPlayersScreenState();
}

class _ProgramCreateAddPlayersScreenState extends State<ProgramCreateAddPlayersScreen> with CreateProgramMixin {
  @override
  void initState() {
    super.initState();
    program = widget.program;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appWhiteColor,
      appBar: buildAppBar(4, 'ADD PLAYERS', programName: program.name),
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 60),
            child: CustomScrollView(
              slivers: <Widget>[
                AddedPlayersWidget(
                  addedPlayers: program.players ?? [],
                  maxCount: program.capacity,
                  onPlayersChanged: _onPlayersChanged,
                ),
              ],
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: _bottomButtons(),
          )
        ],
      ),
    );
  }

  _bottomButtons() {
    return Container(
      height: 60,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: AppButtonGrey(
              title: '< LESSONS',
              enabled: true,
              onPressed: _onBackPressed,
            ),
          ),
          Expanded(
            flex: 1,
            child: AppButton(
              title: 'VIEW SUMMARY >',
              enabled: true,
              onPressed: _onSummaryPressed,
            ),
          ),
        ],
      ),
    );
  }

  _onPlayersChanged(List<Student> players) {
    setState(() => program.players = players);
  }

  _onSummaryPressed(BuildContext context) {
    ProgramCreateSummaryScreen.push(context, program);
  }

  _onBackPressed(BuildContext context) {
    Navigator.of(context).pop();
  }
}
