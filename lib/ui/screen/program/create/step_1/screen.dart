import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/bloc/bloc.dart';
import 'package:lesson_scheduling_pga/logic/models/place.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/general/create_program_general.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_2/screen.dart';

import 'basic_info_widget.dart';

enum ProgramCreateInfoMode {
  create,
  edit,
}

class ProgramCreateInfoScreen extends StatefulWidget {
  static const String route = '/ProgramCreateInfoScreen';

  final Program program;
  final ProgramCreateInfoMode mode;

  ProgramCreateInfoScreen._init(this.program, this.mode);

  factory ProgramCreateInfoScreen.fromArgs(Map<String, Object> args) {
    return ProgramCreateInfoScreen._init(args['program'], args['mode']);
  }

  static Future push(
    BuildContext context, {
    Program program,
    ProgramCreateInfoMode mode = ProgramCreateInfoMode.create,
  }) {
    return Navigator.of(context).pushNamed(
      ProgramCreateInfoScreen.route,
      arguments: {
        'program': program,
        'mode': mode,
      },
    );
  }

  @override
  _ProgramCreateInfoScreenState createState() => _ProgramCreateInfoScreenState();
}

class _ProgramCreateInfoScreenState extends State<ProgramCreateInfoScreen> with CreateProgramMixin {
  final PerformSaveNotifier performSave = PerformSaveNotifier();

  bool get isEditing => widget.mode == ProgramCreateInfoMode.edit;

  @override
  void initState() {
    super.initState();
    program = widget.program;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appWhiteColor,
      appBar: buildAppBar(1, isEditing ? 'PROGRAM INFO' : 'CREATE NEW PROGRAM', saveFunction: () {
        performSave.performSave();
      }),
      body: ProgramCreateInfoWidget(
        program: program,
        buttonTitle: isEditing ? 'SAVE CHANGES' : 'SAVE AND PROCEED',
        onButtonPressed: _onSavePressed,
        performSave: performSave,
      ),
    );
  }

  _createProgram(String name, String goal, int duration, int capacity, Place place) {
    return program == null
        ? Program(name, goal, duration, capacity, [], place)
        : Program.from(program, name, goal, duration, capacity, place);
  }

  _onSavePressed(
    bool createEnabled,
    bool manual,
    String name,
    String goal,
    int duration,
    int capacity,
    Place place,
  ) async {
    if (isEditing) {
      final bloc = Provider.of<BLoC>(context).program;
      await bloc.editProgramInfo(program.id, name, goal, duration, capacity, place);
      return;
    }
    final newProgram = _createProgram(name, goal, duration, capacity, place);
    if (manual) {
      program = newProgram;
      ProgramCreateScheduleStartScreen.push(context, newProgram);
    } else if (createEnabled) {
      program = newProgram;
      saveProgram();
    } else {
      close();
    }
  }
}

class PerformSaveNotifier extends ChangeNotifier {
  void performSave() {
    notifyListeners();
  }
}
