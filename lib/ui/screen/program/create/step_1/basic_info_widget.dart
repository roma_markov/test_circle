import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lesson_scheduling_pga/constants/const.dart';
import 'package:lesson_scheduling_pga/l10n/localization.dart';
import 'package:lesson_scheduling_pga/logic/models/place.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/screen/place/screen.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_text_field.dart';
import 'package:lesson_scheduling_pga/ui/shared/dropdown.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

class ProgramCreateInfoWidget extends StatefulWidget {
  final Program program;
  final String buttonTitle;
  final Function onButtonPressed;
  final ChangeNotifier performSave;

  const ProgramCreateInfoWidget({
    Key key,
    @required this.program,
    @required this.buttonTitle,
    @required this.onButtonPressed,
    @required this.performSave,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ProgramCreateInfoWidgetState();
}

class _ProgramCreateInfoWidgetState extends State<ProgramCreateInfoWidget> {
  var createEnabled = false;
  var durationSelected = 0;
  final nameController = TextEditingController();
  final goalController = TextEditingController();
  final capacityController = TextEditingController();
  Place selectedPlace;

  bool get isEditing => widget.program != null;

  @override
  void initState() {
    super.initState();
    widget?.performSave?.addListener(_onSavePressed);
    if (isEditing) {
      _fillInfoField(widget.program);
      _onFormChanged();
    }
  }

  @override
  dispose() {
    super.dispose();
    widget?.performSave?.removeListener(_onSavePressed);
  }

  _fillInfoField(Program program) {
    nameController.text = program.name;
    goalController.text = program.goal;
    capacityController.text = program.capacity.toString();
    durationSelected = program.durationDays;
    selectedPlace = program.place;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Form(
          onChanged: _onFormChanged,
          child: ListView(
            padding: EdgeInsets.only(left: 16, right: 16, top: 20, bottom: 84),
            children: <Widget>[
              AppTextField(
                controller: nameController,
                textInputAction: TextInputAction.next,
                textCapitalization: TextCapitalization.sentences,
                onFieldSubmitted: (term) => FocusScope.of(context).nextFocus(),
                hint: 'Program name',
              ),
              SizedBox(height: 12),
              AppTextField(
                controller: goalController,
                textCapitalization: TextCapitalization.sentences,
                hint: 'Goal',
              ),
              SizedBox(height: 12),
              Dropdown(
                items: weeks,
                defaultValue: weeks.firstWhere((value) => value == durationSelected, orElse: () => null),
                builder: (context, value) =>
                    (value == year) ? '1 Year' : AppLocalizations.of(context).weeks(value ~/ 7),
                hint: 'Duration',
                callback: _onDurationSelected,
              ),
              SizedBox(height: 12),
              AppTextField(
                controller: capacityController,
                formatter: WhitelistingTextInputFormatter.digitsOnly,
                keyboardType: TextInputType.numberWithOptions(decimal: true, signed: true),
                hint: 'Capacity',
                textInputAction: TextInputAction.done,
                onFieldSubmitted: (term) => FocusScope.of(context).unfocus(),
              ),
              SizedBox(height: 12),
              _placesFieldWidget(
                selectedPlace == null ? 'Where to meet?' : selectedPlace.name,
                _onPlacePressed,
                selectedPlace == null,
              ),
            ],
          ),
        ),
        Positioned(
          bottom: 0,
          left: 0,
          right: 0,
          child: AppButton(
            title: widget.buttonTitle,
            enabled: createEnabled,
            onPressed: (_) => _onSavePressed(manual: true),
          ),
        )
      ],
    );
  }

  _placesFieldWidget(String title, VoidCallback onPressed, bool isHint) {
    return InkWell(
      onTap: onPressed,
      child: Stack(
        fit: StackFit.passthrough,
        children: <Widget>[
          Container(
            child: Text(
              title,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: isHint ? hintTextStyle() : fieldTextStyle(),
            ),
            padding: const EdgeInsets.only(left: 50, right: 50, top: 24, bottom: 24),
            alignment: Alignment.centerLeft,
          ),
          Positioned.fill(
            child: Container(
                padding: const EdgeInsets.only(left: 24),
                decoration: BoxDecoration(
                    border: Border.all(
                  color: dividerColor,
                  width: 1,
                ))),
          ),
          Positioned(
            left: 16,
            top: 0,
            bottom: 0,
            child: const Icon(
              Icons.location_on,
              color: inputHintColor,
            ),
          ),
          Positioned(
            right: 24,
            top: 0,
            bottom: 0,
            child: const Icon(Icons.arrow_drop_down),
          )
        ],
      ),
    );
  }

  _onFormChanged() {
    var newValue = nameController.text.isNotEmpty &&
        goalController.text.isNotEmpty &&
        capacityController.text.isNotEmpty &&
        durationSelected > 0;
    if (createEnabled != newValue) setState(() => createEnabled = newValue);
  }

  _onDurationSelected(int position) {
    durationSelected = weeks[position];
    _onFormChanged();
  }

  _onPlacePressed() async {
    final place = await PlacesScreen.push(context);
    if (place != null) setState(() => selectedPlace = place);
  }

  _onSavePressed({bool manual = false}) {
    widget.onButtonPressed(
      createEnabled,
      manual,
      nameController.text,
      goalController.text,
      durationSelected,
      int.tryParse(capacityController.text) ?? 0,
      selectedPlace,
    );
  }
}
