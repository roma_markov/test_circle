import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/bloc/bloc.dart';
import 'package:lesson_scheduling_pga/logic/bloc/modules/private_lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/schedule_lesson/same_time_item.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/block_provider.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/calendar.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/calendar_decoration.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/event_list.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';
import 'package:lesson_scheduling_pga/utils/date_utils.dart';

import 'date_time_item.dart';
import 'mode_button.dart';

enum ScheduleLessonTimeMode {
  sameTime,
  customTime,
}

enum ScheduleLessonDayMode {
  specific,
  weekly,
}

class ScheduleProgramLessonScreen extends StatefulWidget {
  static const String route = '/ScheduleProgramLessonScreen';
  final Lesson lesson;
  final Program program;

  ScheduleProgramLessonScreen._init(this.lesson, this.program);

  factory ScheduleProgramLessonScreen.fromArgs(Map<String, Object> args) {
    return ScheduleProgramLessonScreen._init(args['lesson'], args['program']);
  }

  static Future push(
    BuildContext context,
    Program program,
    Lesson lesson,
  ) {
    return Navigator.of(context).pushNamed(
      ScheduleProgramLessonScreen.route,
      arguments: {'lesson': lesson, 'program': program},
    );
  }

  @override
  _ScheduleProgramLessonScreenState createState() => _ScheduleProgramLessonScreenState();
}

final _kTimeButtonsHeight = 100.0;
final _kDayButtonsHeight = 60.0;
final _kCustomItemHeight = 168.0;

class _ScheduleProgramLessonScreenState extends State<ScheduleProgramLessonScreen> {
  List<ScheduleItem> _listItems = [];
  List<DateTime> selectedLessonDates = [];
  Set<DateTime> invalidLessonDates = Set();
  List<DatePair> selectedRanges = [];
  TimeOfDay selectedTime = TimeOfDay.now();
  Map<DateTime, TimeOfDay> selectedTimesMap = Map();
  DateTime pressedDateTime;
  ScheduleLessonTimeMode timeMode;
  ScheduleLessonDayMode dayMode = ScheduleLessonDayMode.specific;
  final scrollController = ScrollController();
  EventList cachedEventList;
  StreamSubscription subscription;

  @override
  void initState() {
    super.initState();
    selectedLessonDates.addAll(
      widget.program.lessonsSchedule[widget.lesson].map((schedule) => schedule.date).toList(),
    );
    if (selectedLessonDates.isNotEmpty) {
      selectedLessonDates.forEach((date) {
        selectedTimesMap[date] = selectedTime;
      });
    }
    final selectedStartDates = widget.program.startDates;
    selectedStartDates.forEach((start) {
      final startDay = DateTime(start.year, start.month, start.day);
      selectedRanges.add(DatePair(startDay, startDay.add(Duration(days: widget.program.durationDays - 1))));
    });
    _changeTimeMode(ScheduleLessonTimeMode.sameTime);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _validateSelected();
    });
  }

  @override
  dispose() {
    subscription?.cancel();
    subscription = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appWhiteColor,
      appBar: _appBar(),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned.fill(bottom: 60, child: _calendar()),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: AppButton(
              title: selectedLessonDates.length > 0 && invalidLessonDates.isEmpty
                  ? 'SCHEDULE ${selectedLessonDates.length} DAYS'
                  : 'SET LESSON SCHEDULE',
              enabled: selectedLessonDates.length > 0 && invalidLessonDates.isEmpty,
              onPressed: _onSetPressed,
            ),
          )
        ],
      ),
    );
  }

  _appBar() {
    return AppBar(
      titleSpacing: 0,
      brightness: Brightness.light,
      leading: IconButton(icon: Icon(Icons.close), onPressed: _onClosePressed),
      iconTheme: IconThemeData(color: inputHintColor),
      backgroundColor: lightAppBarColor,
      title: Text(
        'SCHEDULE: ${widget.lesson.name.toUpperCase()}',
        style: appBarLightTextStyle,
      ),
    );
  }

  _calendar() {
    final bloc = Provider.of<BLoC>(context);
    return Container(
      child: StreamBuilder<EventList>(
        initialData: EventList(),
        stream: bloc.eventsStream(),
        builder: (context, snapshot) {
          final eventList = snapshot.data;
          return CustomScrollView(
            controller: scrollController,
            slivers: <Widget>[
              SliverToBoxAdapter(child: _dayModeButtons()),
              _calendarSliver(eventList),
              SliverToBoxAdapter(child: SectionHeader(text: 'LESSON TIME', textColor: inputHintColor)),
              SliverToBoxAdapter(child: _timeModeButtons()),
              SliverList(
                delegate: SliverChildBuilderDelegate((context, position) {
                  final item = _listItems[position];
                  switch (item.runtimeType) {
                    case SameTimeItem:
                      return SameTimeItemWidget(
                        selectedTime: selectedTime,
                        onPressed: _onTimePressed,
                      );
                    case CustomTimeItem:
                      final date = (item as CustomTimeItem).date;
                      return DateTimeItemWidget(
                        dateTime: date,
                        selectedTime: selectedTimesMap[date],
                        isValid: !invalidLessonDates.contains(date),
                        onPressed: _onCustomTimePressed,
                      );
                  }
                  return Container();
                }, childCount: _listItems.length),
              ),
            ],
          );
        },
      ),
    );
  }

  _calendarSliver(EventList eventList) {
    return SliverToBoxAdapter(
      child: Container(
          height: Calendar.calendarHeight(context, 6),
          child: Calendar(
            onDayPressed: _onDayPressed,
            decoratorBuilder: _buildDecoration,
            eventList: eventList,
            selectedDayButtonColor: colorById(widget.program.id).withOpacity(0.5),
            weekFormat: false,
            selectedDateTimes: selectedLessonDates,
          )),
    );
  }

  _dayModeButtons() {
    return Container(
      height: _kDayButtonsHeight,
      color: appBackgroundColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ModeButton(
            titleText: 'SPECIFIC DATES',
            isActive: dayMode == ScheduleLessonDayMode.specific,
            onPressed: () => _changeDayMode(ScheduleLessonDayMode.specific),
          ),
          SizedBox(width: 16),
          ModeButton(
            titleText: 'WEEKLY DATES',
            isActive: dayMode == ScheduleLessonDayMode.weekly,
            onPressed: () => _changeDayMode(ScheduleLessonDayMode.weekly),
          ),
        ],
      ),
    );
  }

  _timeModeButtons() {
    return Container(
      height: _kTimeButtonsHeight,
      width: 100,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ModeButton(
            titleText: 'SAME TIME',
            isActive: timeMode == ScheduleLessonTimeMode.sameTime,
            onPressed: () => _changeTimeMode(ScheduleLessonTimeMode.sameTime),
          ),
          SizedBox(width: 16),
          ModeButton(
            titleText: 'CUSTOM TIME',
            isActive: timeMode == ScheduleLessonTimeMode.customTime,
            onPressed: () => _changeTimeMode(ScheduleLessonTimeMode.customTime),
          ),
        ],
      ),
    );
  }

  _changeTimeMode(ScheduleLessonTimeMode newMode) {
    if (newMode != timeMode) {
      setState(() {
        timeMode = newMode;
        _updateItems();
      });
    }
  }

  _changeDayMode(ScheduleLessonDayMode newMode) {
    if (newMode != dayMode) {
      setState(() {
        dayMode = newMode;
        selectedLessonDates.clear();
        selectedTimesMap.clear();
        invalidLessonDates.clear();
        _updateItems();
      });
    }
  }

  _updateItems() {
    _listItems.clear();
    if (timeMode == ScheduleLessonTimeMode.sameTime) {
      _listItems.add(SameTimeItem());
    } else {
      final days = selectedTimesMap.keys.toList();
      days.sort((r, l) => r.compareTo(l));
      _listItems.addAll(days.map((date) => CustomTimeItem(date)));
    }
  }

  _onTimePressed() async {
    final result = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );
    if (result == null) return;
    selectedTimesMap.keys.forEach((time) => selectedTimesMap[time] = result);
    _validateSelected();
    setState(() => selectedTime = result);
  }

  _onCustomTimePressed(DateTime dateTime) async {
    final result = await ChooseTimeScreen.push(
      context,
      widget.lesson,
      dateTime,
      invalidLessonDates.contains(dateTime) ? null : selectedTimesMap[dateTime],
      false,
    );
    if (result == null) return;

    setState(() {
      selectedTimesMap[dateTime] = result;
      _validateSelected();
    });
  }

  _onSetPressed(BuildContext context) {
    if (timeMode == ScheduleLessonTimeMode.sameTime) {
      widget.program.lessonsSchedule[widget.lesson] =
          selectedLessonDates.map((date) => Schedule(date, selectedTime, widget.lesson)).toList();
    } else {
      widget.program.lessonsSchedule[widget.lesson] =
          selectedLessonDates.map((date) => Schedule(date, selectedTimesMap[date], widget.lesson)).toList();
    }
    Navigator.of(context).pop();
  }

  _onClosePressed() {
    Navigator.of(context).pop();
  }

  _onDayPressed(DateTime day) {
    if (DateUtils.isInRanges(selectedRanges, day)) {
      if (selectedLessonDates.contains(day)) {
        (dayMode == ScheduleLessonDayMode.specific) ? _removeDaySpecificMode(day) : _removeDayWeeklyMode(day);
      } else {
        (dayMode == ScheduleLessonDayMode.specific) ? _addDaySpecificMode(day) : _addDayWeeklyMode(day);
        _validateSelected();
      }
      _updateItems();
      setState(() {});
    }
  }

  _addDaySpecificMode(DateTime day) {
    selectedTimesMap[day] = selectedTime;
    selectedLessonDates.add(day);
  }

  _removeDaySpecificMode(DateTime day) {
    selectedTimesMap.remove(day);
    selectedLessonDates.remove(day);
    invalidLessonDates.remove(day);
  }

  _addDayWeeklyMode(DateTime day) {
    final end = selectedRanges.last.end.add(Duration(hours: 24));
    DateTime date = day;
    while (date.isBefore(end)) {
      if (DateUtils.isInRanges(selectedRanges, date)) {
        selectedTimesMap[date] = selectedTime;
        selectedLessonDates.add(date);
      }
      date = DateUtils.addDays(date, 7);
    }
  }

  _removeDayWeeklyMode(DateTime day) {
    selectedLessonDates.removeWhere((item) => item.weekday == day.weekday);
    selectedTimesMap.removeWhere((dateTime, time) => dateTime.weekday == day.weekday);
    invalidLessonDates.removeWhere((item) => item.weekday == day.weekday);
  }

  _validateSelected() {
    if (cachedEventList != null) {
      selectedLessonDates.forEach((date) {
        _validateDate(cachedEventList, date);
      });
    } else {
      final bloc = Provider.of<BLoC>(context);
      subscription = bloc.eventsStream().listen((list) {
        cachedEventList = list;
        _validateSelected();
        subscription.cancel();
        subscription = null;
      });
    }
  }

  _validateDate(EventList events, DateTime date) {
    if (date == null || selectedTimesMap == null || selectedTimesMap[date] == null) return;
    final isValid =
        PrivateLessonBLoC.validateDateTime(events, widget.program.id, widget.lesson, date, selectedTimesMap[date]);
    if (isValid) {
      invalidLessonDates.remove(date);
    } else {
      invalidLessonDates.add(date);
      _scrollToInvalidDate(date);
    }
  }

  _scrollToInvalidDate(DateTime date) {
    _changeTimeMode(ScheduleLessonTimeMode.customTime);
    final days = selectedTimesMap.keys.toList()..sort();
    final indexOfDay = days.indexOf(date);
    double scrollTo = Calendar.calendarHeight(context, 6);
    scrollTo += kSectionHeight;
    scrollTo += _kTimeButtonsHeight;
    scrollTo += _kDayButtonsHeight;
    scrollTo += (indexOfDay * _kCustomItemHeight);
    scrollController.animateTo(scrollTo, duration: Duration(milliseconds: 500), curve: Curves.linear);
  }

  Widget _buildDecoration(BuildContext context, DateTime day, EventList eventList) {
    return CalendarDecoration.buildDecoration(
      context,
      day,
      eventList,
      activePrograms: [widget.program],
      style: CalendarDecorationStyle.grayed,
      additionalBuilder: (context) {
        return _buildCurrentProgramIndicators(day);
      },
    );
  }

  _buildCurrentProgramIndicators(DateTime day) {
    final show = DateUtils.isInRanges(selectedRanges, day);
    final isInvalid = invalidLessonDates.contains(day);
    return Container(
      constraints: BoxConstraints.tightForFinite(),
      decoration: BoxDecoration(
          color: (show ? lightGrayColor.withOpacity(0.2) : Colors.transparent),
          border: Border.all(
            width: 2,
            color: isInvalid ? appRedColor.withOpacity(0.8) : Colors.transparent,
          )),
    );
  }
}

abstract class ScheduleItem {}

class SameTimeItem extends ScheduleItem {}

class CustomTimeItem extends ScheduleItem {
  final DateTime date;

  CustomTimeItem(this.date);
}
