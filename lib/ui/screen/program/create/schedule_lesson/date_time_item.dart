import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';

import 'date_time_button.dart';

class DateTimeItemWidget extends StatelessWidget {
  final DateTime dateTime;
  final TimeOfDay selectedTime;
  final Function(DateTime) onPressed;
  final bool isValid;

  const DateTimeItemWidget({
    Key key,
    @required this.onPressed,
    @required this.dateTime,
    @required this.isValid,
    @required this.selectedTime,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 170,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Divider(color: lightGrayColor, height: 1),
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 16, right: 16),
            height: 60,
            child: Text(
              DateFormat.MMMMd().format(dateTime).toUpperCase(),
              style: TextStyle(
                fontFamily: fontRobotoBold,
                fontSize: 14,
                color: appBlackColor,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 16, right: 16),
            alignment: Alignment.centerLeft,
            height: 34,
            child: RichText(
              text: TextSpan(
                text: 'START TIME',
                style: lessonLabelTextStyle,
                children: isValid ? [] : [TextSpan(text: '  (this time is already taken)', style: warningTextStyle)],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 16, right: 16, bottom: 20),
            child: DateTimeButton(
              text: selectedTime.format(context),
              icon: Icons.access_time,
              isValid: isValid,
              onPressed: () => onPressed(dateTime),
            ),
          ),
        ],
      ),
    );
  }
}
