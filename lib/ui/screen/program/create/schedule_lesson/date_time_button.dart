import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';

class DateTimeButton extends StatelessWidget {
  final String text;
  final IconData icon;
  final bool isValid;
  final VoidCallback onPressed;

  const DateTimeButton({
    Key key,
    @required this.text,
    @required this.icon,
    @required this.isValid,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: lightGrayColor,
              width: 1.0,
            ),
          ),
        ),
        padding: EdgeInsets.only(top: 12, bottom: 12),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(child: Text(text, style: isValid ? lessonTextStyle : lessonWarningTextStyle)),
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Icon(
                icon,
                color: inputHintColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
