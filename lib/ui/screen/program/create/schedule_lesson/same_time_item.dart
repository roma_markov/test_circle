import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';

import 'date_time_button.dart';

class SameTimeItemWidget extends StatelessWidget {
  final TimeOfDay selectedTime;
  final VoidCallback onPressed;

  const SameTimeItemWidget({
    Key key,
    @required this.onPressed,
    @required this.selectedTime,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 16, right: 16, top: 24, bottom: 14),
          child: Text('START TIME', style: lessonLabelTextStyle),
        ),
        Padding(
          padding: EdgeInsets.only(left: 16, right: 16, bottom: 24),
          child: DateTimeButton(
            text: selectedTime.format(context),
            icon: Icons.access_time,
            isValid: true,
            onPressed: onPressed,
          ),
        ),
      ],
    );
  }
}
