import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';

class ModeButton extends StatelessWidget {
  final String titleText;
  final bool isActive;
  final VoidCallback onPressed;

  const ModeButton({
    Key key,
    @required this.titleText,
    @required this.isActive,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.all(Radius.circular(36)),
      onTap: () => onPressed(),
      child: Container(
        height: 44,
        padding: EdgeInsets.only(left: 24, right: 24, top: 12, bottom: 14),
        decoration: BoxDecoration(
          color: isActive ? appBlackColor : lightGrayColor,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.all(Radius.circular(36)),
        ),
        alignment: Alignment.center,
        child: Text(
          titleText,
          style: isActive ? switchButtonTextStyle(appWhiteColor) : switchButtonTextStyle(appBlackColor),
        ),
      ),
    );
  }
}
