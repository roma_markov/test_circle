import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

class PlayerStatusListItem extends StatelessWidget {
  final Student student;
  final bool isConfirmed;

  const PlayerStatusListItem({
    Key key,
    @required this.student,
    this.isConfirmed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return contentWidget(context);
  }

  Widget contentWidget(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 8, bottom: 8, left: 16, right: 16),
      height: 136,
      child: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Container(
              decoration: BoxDecoration(
                color: appWhiteColor,
                border: Border.all(color: Color(0xFFE5E5E5)),
              ),
            ),
          ),
          Positioned(
            left: 24,
            right: 0,
            top: 27,
            child: Text(
              '${student.firstName} ${student.lastName}',
              style: lessonItemNameStyle,
            ),
          ),
          Positioned(
            left: 24,
            right: 0,
            top: 52,
            child: Text(
              student.email,
              style: lessonItemGoalStyle,
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              padding: EdgeInsets.only(left: 24, right: 24, bottom: 8),
              height: 48,
              child: Row(
                children: [
                  Icon(
                    isConfirmed ? Icons.event_available : Icons.event_busy,
                    color: isConfirmed ? approvedColor : appRedColor,
                  ),
                  SizedBox(width: 12),
                  Text(
                    isConfirmed ? 'CONFIRMED' : 'NOT YET CONFIRMED',
                    style: isConfirmed ? approvedTextStyle : warningTextStyle,
                  )
                ],
              ),
            ),
          ),
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () => {},
                child: Container(),
              ),
            ),
          )
        ],
      ),
    );
  }
}
