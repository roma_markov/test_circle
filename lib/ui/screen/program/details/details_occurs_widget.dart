import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

class DetailsOccursWidget extends StatelessWidget {
  final List<DateTime> selectedStartDates;
  final int durationDays;

  const DetailsOccursWidget({
    Key key,
    @required this.selectedStartDates,
    @required this.durationDays,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverList(
        delegate: SliverChildListDelegate(
      [
        _label('PROGRAM OCCURS ON'),
        _periodItems(),
        SizedBox(height: 16),
      ],
    ));
  }

  _periodItems() {
    final result = <Widget>[];
    selectedStartDates.forEach((start) {
      final end = start.add(Duration(days: durationDays));
      result.add(Padding(
        padding: EdgeInsets.only(left: 32, right: 32, top: 8, bottom: 8),
        child: Text(
          '${DateFormat.MMMMd().format(start)} - ${DateFormat.MMMMd().format(end)}',
          style: fieldTextStyle(),
        ),
      ));
    });
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: result,
    );
  }

  _label(String text) {
    return Padding(
      padding: EdgeInsets.only(left: 32, right: 32, top: 24, bottom: 14),
      child: Text(text, style: lessonLabelTextStyle),
    );
  }
}
