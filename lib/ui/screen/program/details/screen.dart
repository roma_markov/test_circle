import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/l10n/localization.dart';
import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/details/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/edit/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_1/screen.dart';
import 'package:lesson_scheduling_pga/ui/shared/bottom_dialog_item.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/calendar.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/calendar_decoration.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/event_list.dart';
import 'package:lesson_scheduling_pga/ui/shared/sliver_section_header.dart';
import 'package:lesson_scheduling_pga/utils/date_utils.dart';

import '../../../index.dart';
import '../../../shared/style.dart';
import 'details_added_lesson_widget.dart';
import 'details_added_players_widget.dart';
import 'details_occurs_widget.dart';
import 'general_info_widget.dart';

enum ProgramDetailsMode { none, selection }

class ProgramDetailsScreen extends StatefulWidget {
  static const String route = '/ProgramDetailsScreen';
  final int programId;
  final ProgramDetailsMode mode;

  ProgramDetailsScreen._init(this.programId, this.mode);

  factory ProgramDetailsScreen.fromArgs(Map<String, Object> args) {
    return ProgramDetailsScreen._init(
      args['programId'],
      args['selectMode'] ?? ProgramDetailsMode.none,
    );
  }

  static Future push(
    BuildContext context,
    int programId, {
    ProgramDetailsMode mode = ProgramDetailsMode.none,
  }) {
    return Navigator.of(context).pushNamed(
      ProgramDetailsScreen.route,
      arguments: {
        'programId': programId,
        'selectMode': mode,
      },
    );
  }

  @override
  _ProgramDetailsScreenState createState() => _ProgramDetailsScreenState();
}

class _ProgramDetailsScreenState extends State<ProgramDetailsScreen> {
  Program program;
  List<DatePair> selectedRanges = [];

  _buildDays(Program program) {
    selectedRanges.clear();
    final selectedStartDates = program.startDates;
    selectedStartDates.forEach((start) {
      selectedRanges.add(DatePair(start, start.add(Duration(days: program.durationDays - 1))));
    });
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<BLoC>(context);
    return StreamBuilder<bool>(
        initialData: false,
        stream: provider.program.progressStream,
        builder: (context, snapshot) {
          return IgnorePointer(
            ignoring: snapshot.data,
            child: _content(context, snapshot.data),
          );
        });
  }

  _content(BuildContext context, bool progress) {
    final bloc = Provider.of<BLoC>(context).program;
    bloc.program(widget.programId);
    return StreamBuilder<Program>(
        stream: bloc.programDetailsStream,
        builder: (context, snapshot) {
          program = snapshot.data ?? program;
          if (program == null)
            return Container(
              color: appWhiteColor,
            );
          _buildDays(program);
          return Scaffold(
            backgroundColor: appWhiteColor,
            appBar: _appBar(),
            body: Stack(
              children: <Widget>[
                Positioned.fill(
                  child: CustomScrollView(slivers: _slivers()),
                ),
                Positioned(
                  left: 0,
                  right: 0,
                  top: 0,
                  height: 5,
                  child: progress ? LinearProgressIndicator() : Container(),
                ),
              ],
            ),
          );
        });
  }

  _appBar() {
    return AppBar(
      titleSpacing: 0,
      brightness: Brightness.light,
      actions: widget.mode == ProgramDetailsMode.none
          ? <Widget>[
              IconButton(
                icon: Icon(Icons.more_vert),
                onPressed: _onMorePressed,
              )
            ]
          : null,
      iconTheme: IconThemeData(color: inputHintColor),
      backgroundColor: lightAppBarColor,
      title: Text(
        'PROGRAM DETAILS',
        style: appBarLightTextStyle,
      ),
    );
  }

  _slivers() {
    final result = <Widget>[
      SliverList(
        delegate: SliverChildListDelegate([
          ProgramGeneralInfoWidget(program: program),
        ]),
      ),
    ];

    var widgets = _addOccurs(program);
    if (widgets != null) result.addAll(widgets);

    widgets = _addLessons(program);
    if (widgets != null) result.addAll(widgets);

    widgets = _addPlayers(program);
    if (widgets != null) result.addAll(widgets);

    return result;
  }

  _calendarSliver() {
    final bloc = Provider.of<BLoC>(context);
    return SliverList(
      delegate: SliverChildListDelegate.fixed([
        StreamBuilder<EventList>(
            initialData: EventList(),
            stream: bloc.eventsStream(),
            builder: (context, snapshot) {
              final eventList = snapshot.data;
              return Container(
                  height: Calendar.calendarHeight(context, 6),
                  child: Calendar(
                    decoratorBuilder: _buildDecoration,
                    eventList: eventList,
                    weekFormat: false,
                  ));
            }),
      ]),
    );
  }

  Widget _buildDecoration(BuildContext context, DateTime day, EventList eventList) {
    return CalendarDecoration.buildDecoration(
      context,
      day,
      eventList,
      activePrograms: [program],
      style: CalendarDecorationStyle.grayed,
      additionalBuilder: (context) {
        return _buildCurrentProgramIndicators(day);
      },
    );
  }

  _buildCurrentProgramIndicators(DateTime date) {
    final show = DateUtils.isInRanges(selectedRanges, date);
    return Container(
      constraints: BoxConstraints.tightForFinite(),
      color: show ? lightGrayColor.withOpacity(0.2) : Colors.transparent,
    );
  }

  _addOccurs(Program program) {
    if (program.startDates == null || program.startDates.isEmpty) return null;
    return <Widget>[
      SliverSectionHeader(text: 'SCHEDULING INFORMATION', textColor: inputHintColor),
      DetailsOccursWidget(
        selectedStartDates: program.startDates ?? [],
        durationDays: program.durationDays,
      ),
      _calendarSliver(),
    ];
  }

  _addLessons(Program program) {
    if (program.startDates == null || program.lessonsSchedule.isEmpty) return null;
    return <Widget>[
      SliverSectionHeader(
        text: AppLocalizations.of(context).lessons(program.lessonsSchedule.length).toUpperCase(),
        textColor: inputHintColor,
      ),
      DetailsAddedLessonsWidget(
        addedLessons: program.lessonsSchedule ?? Map<Lesson, List<Schedule>>(),
        onPressed: _onLessonPressed,
      ),
    ];
  }

  _addPlayers(Program program) {
    if (program.players == null || program.players.isEmpty) return null;
    return <Widget>[
      SliverSectionHeader(
        text: 'PLAYERS (${program.players.length}/${program.capacity})',
        textColor: inputHintColor,
      ),
      DetailsAddedPlayersWidget(
        addedPlayers: program.players,
        invitationRecord: program.invitationRecords,
      ),
    ];
  }

  _onMorePressed() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return SizedBox(
              height: (3 * kBottomItemHeight) + kBottomDialogPadding,
              child: Column(children: [
                Padding(padding: EdgeInsets.only(top: kBottomDialogPadding / 2.0)),
                _bottomDialogItem("EDIT PROGRAM", Icons.edit, _onEditPressed),
                _bottomDialogItem("DELETE PROGRAM", Icons.delete, _onDeletePressed),
                _bottomDialogItem("CREATE NEW SIMILAR", Icons.event_note, _onCreateNewPressed),
              ]));
        });
  }

  _onEditPressed() {
    Navigator.of(context).pop();
    ProgramEditSummaryScreen.push(context, program);
  }

  _onDeletePressed() async {
    Navigator.of(context).pop();
    final provider = Provider.of<BLoC>(context);
    final result = await provider.program.deleteProgram(widget.programId);
    if (result.isSuccess) {
      await provider.privateLesson.requestPrivateLessons();
      Navigator.of(context).pop();
    }
  }

  _onCreateNewPressed() {
    Navigator.of(context).pop();
    ProgramCreateInfoScreen.push(context, program: program);
  }

  _bottomDialogItem(String title, IconData icon, VoidCallback onPressed) {
    return InkWell(
      onTap: () => onPressed(),
      child: Container(
        height: 56,
        child: Row(
          children: <Widget>[
            Container(
              width: 72,
              child: Icon(
                icon,
                color: inputHintColor,
              ),
            ),
            Text(
              title,
              style: lightButtonTextStyle,
            )
          ],
        ),
      ),
    );
  }

  _onLessonPressed(Lesson item) {
    LessonDetailsScreen.push(
      context,
      item.id,
      schedules: program.lessonsSchedule[item],
      mode: LessonDetailsMode.scheduled,
    );
  }
}
