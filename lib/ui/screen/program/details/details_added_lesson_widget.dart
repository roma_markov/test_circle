import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/list/list_item.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_3/list_item.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

class DetailsAddedLessonsWidget extends StatelessWidget {
  final Map<Lesson, List<Schedule>> addedLessons;
  final LessonAddedPressed onPressed;

  const DetailsAddedLessonsWidget({
    Key key,
    @required this.addedLessons,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _listWidget();
  }

  _listWidget() {
    final lessons = addedLessons.keys.toList();
    return SliverList(
      delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
        final lesson = lessons[index];
        return LessonListItem(
          lesson: lesson,
          schedules: addedLessons[lessons] ?? [],
          indicatorColor: colorById(lesson.id),
          onPressed: onPressed,
        );
      }, childCount: addedLessons.length),
    );
  }
}
