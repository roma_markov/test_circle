import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/logic/models/invitation_record.dart';
import 'package:lesson_scheduling_pga/logic/models/student.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/details/player_status_item.dart';

class DetailsAddedPlayersWidget extends StatelessWidget {
  final List<Student> addedPlayers;
  final List<InvitationRecord> invitationRecord;

  const DetailsAddedPlayersWidget({
    Key key,
    @required this.addedPlayers,
    @required this.invitationRecord,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _listWidget();
  }

  _listWidget() {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          final player = addedPlayers[index];
          final record = invitationRecord?.firstWhere((record) => record.playerId == player.id, orElse: () => null);
          return PlayerStatusListItem(
            student: player,
            isConfirmed: record?.isConfirmed ?? false,
          );
        },
        childCount: addedPlayers.length,
      ),
    );
  }
}
