import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/constants/const.dart';
import 'package:lesson_scheduling_pga/l10n/localization.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

class ProgramGeneralInfoWidget extends StatelessWidget {
  final Program program;

  const ProgramGeneralInfoWidget({
    Key key,
    @required this.program,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.only(top: 32, left: 32, right: 32, bottom: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(program.name, style: lessonNameTextStyle),
          _label(AppLocalizations.of(context).goal.toUpperCase()),
          Text(program.goal, style: lessonTextStyle),
          _label(AppLocalizations.of(context).duration.toUpperCase()),
          Text(
            (program.durationDays == year) ? '1 Year' : AppLocalizations.of(context).weeks(program.durationDays ~/ 7),
            style: lessonTextStyle,
          ),
          _label(AppLocalizations.of(context).capacity.toUpperCase()),
          Text(AppLocalizations.of(context).students(program.capacity), style: lessonTextStyle),
          program.place != null ? _label(AppLocalizations.of(context).location.toUpperCase()) : Container(),
          program.place != null ? Text(program.place.name, style: lessonTextStyle) : Container(),
        ],
      ),
    );
  }

  _label(String text) {
    return Padding(
      padding: const EdgeInsets.only(top: 24, bottom: 14),
      child: Text(text, style: lessonLabelTextStyle),
    );
  }
}
