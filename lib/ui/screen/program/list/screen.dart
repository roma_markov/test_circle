import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/details/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/list/programs_widget.dart';

class ProgramsScreen extends StatefulWidget {
  static const String route = '/ProgramsScreen';

  ProgramsScreen._init();

  factory ProgramsScreen.fromArgs(Map<String, Object> args) {
    return ProgramsScreen._init();
  }

  static Future push(BuildContext context) {
    return Navigator.of(context).pushNamed(ProgramsScreen.route, arguments: {});
  }

  @override
  _ProgramsScreenState createState() => _ProgramsScreenState();
}

class _ProgramsScreenState extends State<ProgramsScreen> {
  @override
  Widget build(BuildContext context) {
    return ProgramsWidget(onPressed: _onProgramPressed);
  }

  _onProgramPressed(Program program) {
    ProgramDetailsScreen.push(context, program.id);
  }
}
