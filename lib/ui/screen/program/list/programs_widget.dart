import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/logic/bloc/bloc.dart';
import 'package:lesson_scheduling_pga/logic/bloc/modules/program.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/shared/block_provider.dart';
import 'package:lesson_scheduling_pga/ui/shared/search_widget.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

import 'list_item.dart';

class ProgramsWidget extends StatefulWidget {
  final ProgramPressed onPressed;

  const ProgramsWidget({
    Key key,
    @required this.onPressed,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ProgramsWidgetState();
  }
}

class ProgramsWidgetState extends State<ProgramsWidget> {
  final searchController = TextEditingController();
  ProgramBLoC programBLoC;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      programBLoC.requestPrograms();
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    programBLoC = Provider.of<BLoC>(context).program;
  }

  @override
  dispose() {
    programBLoC.changeSearchFilter('');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Program>>(
        initialData: <Program>[],
        stream: programBLoC.programsStream,
        builder: (context, snapshot) {
          final list = snapshot.data;
          return Stack(
            children: [
              _listViewWidget(list),
              _progressIndicator(),
            ],
          );
        });
  }

  _listViewWidget(List<Program> list) {
    return RefreshIndicator(
      child: ListView.builder(
          itemCount: list.length + 1,
          itemBuilder: (context, position) {
            if (position == 0) return _searchWidget(context);
            return _listItemWidget(list[position - 1]);
          }),
      onRefresh: () => _refreshPrograms(true),
    );
  }

  _listItemWidget(Program program) {
    return ProgramListItem(
      program: program,
      indicatorColor: colorById(program.id),
      onPressed: widget.onPressed,
    );
  }

  _progressIndicator() {
    return StreamBuilder<bool>(
        initialData: true,
        stream: programBLoC.progressStream,
        builder: (context, snapshot) {
          return snapshot.data ? LinearProgressIndicator() : Container();
        });
  }

  _searchWidget(BuildContext context) {
    return SearchWidget(
      hint: 'Search for a program',
      searchController: searchController,
      onChanged: _changeSearch,
    );
  }

  //Actions

  _changeSearch(String value) {
    programBLoC.changeSearchFilter(value);
  }

  _refreshPrograms(bool force) async {
    programBLoC.requestPrograms(force: force);
  }
}
