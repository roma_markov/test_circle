import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/l10n/localization.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';

import '../../../index.dart';

typedef ProgramPressed = void Function(Program item);

class ProgramListItem extends StatelessWidget {
  final Color indicatorColor;
  final Program program;
  final ProgramPressed onPressed;

  const ProgramListItem({
    Key key,
    @required this.program,
    @required this.onPressed,
    @required this.indicatorColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int totalHours = 0;
    program.lessonsSchedule.forEach((lesson, schedules) {
      totalHours += lesson.durationHours * schedules.length;
    });
    return Container(
      height: 80,
      child: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Container(
              color: appWhiteColor,
            ),
          ),
          Positioned(
            left: 0,
            top: 0,
            bottom: 0,
            child: Container(
              width: 4,
              color: indicatorColor,
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              height: 1,
              color: Color(0xFFE5E5E5),
            ),
          ),
          Positioned(
            left: 24,
            right: 0,
            top: 24,
            child: Text(
              program.name,
              style: lessonItemNameStyle,
            ),
          ),
          Positioned(
            left: 24,
            right: 24,
            top: 47,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                  child: Text(
                    AppLocalizations.of(context).lessons(program.lessonsSchedule.keys?.length ?? 0),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: lessonItemGoalStyle,
                  ),
                ),
                Flexible(
                  child: Text(
                    "    |    ${AppLocalizations.of(context).hours(totalHours)}",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: lessonItemGoalStyle,
                  ),
                ),
                Flexible(
                  child: Text(
                    "    |    ${AppLocalizations.of(context).students(program.players?.length ?? 0)}",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: lessonItemGoalStyle,
                  ),
                ),
              ],
            ),
          ),
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                highlightColor: indicatorColor.withOpacity(0.2),
                onTap: () => onPressed(program),
                child: Container(),
              ),
            ),
          )
        ],
      ),
    );
  }
}
