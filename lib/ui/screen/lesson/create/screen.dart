import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_tags/tag.dart';
import 'package:lesson_scheduling_pga/l10n/localization.dart';
import 'package:lesson_scheduling_pga/logic/bloc/modules/lesson.dart';
import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/equipment.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/tag.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_text_field.dart';
import 'package:lesson_scheduling_pga/ui/shared/dialogs.dart';
import 'package:lesson_scheduling_pga/ui/shared/dropdown.dart';

enum LessonCreateMode {
  create,
  edit,
}

class LessonCreateScreen extends StatefulWidget {
  static const String route = '/LessinCreateScreen';
  final Lesson lesson;
  final LessonCreateMode mode;

  LessonCreateScreen._init(this.lesson, this.mode);

  factory LessonCreateScreen.fromArgs(Map<String, Object> args) {
    return LessonCreateScreen._init(
      args["lesson"],
      args["mode"],
    );
  }

  static Future push(
    BuildContext context, {
    Lesson lesson,
    LessonCreateMode mode = LessonCreateMode.create,
  }) {
    return Navigator.of(context).pushNamed(
      LessonCreateScreen.route,
      arguments: {
        'lesson': lesson,
        'mode': mode,
      },
    );
  }

  @override
  _LessonCreateScreenState createState() => _LessonCreateScreenState();
}

class _LessonCreateScreenState extends State<LessonCreateScreen> {
  var createEnabled = false;
  var fieldsEnabled = true;
  var durationSelected = 0;
  List<Equipment> selectedEquipments = [];
  List<Tag> selectedTags = [];
  final nameController = TextEditingController();
  final goalController = TextEditingController();
  final noteController = TextEditingController();

  bool get isEditing => widget.mode == LessonCreateMode.edit;
  LessonBLoC lessonBLoC;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    lessonBLoC = Provider.of<BLoC>(context).lesson;
  }

  @override
  void initState() {
    super.initState();
    if (widget.lesson != null) {
      _fillInfoField(widget.lesson);
      _onFormChanged();
    }
  }

  _fillInfoField(Lesson lesson) {
    nameController.text = lesson.name;
    goalController.text = lesson.goal;
    noteController.text = lesson.note;
    durationSelected = lesson.durationHours;
    selectedEquipments = lesson.equipment ?? [];
    selectedTags = lesson.tags ?? [];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appWhiteColor,
      appBar: _appBar(),
      body: Stack(
        children: [
          _form(),
          _saveButton(),
          _progressIndicator(),
        ],
      ),
    );
  }

  _appBar() {
    return AppBar(
      titleSpacing: 0,
      brightness: Brightness.light,
      leading: IconButton(
        icon: Icon(Icons.close),
        onPressed: _onClosePressed,
      ),
      iconTheme: IconThemeData(color: inputHintColor),
      backgroundColor: lightAppBarColor,
      title: Text(
        isEditing ? AppLocalizations.of(context).lessonEditTitle : AppLocalizations.of(context).lessonCreateTitle,
        style: appBarLightTextStyle,
      ),
    );
  }

  _form() {
    return Form(
      onChanged: _onFormChanged,
      child: ListView(
        padding: const EdgeInsets.only(left: 16, right: 16, top: 20, bottom: 84),
        children: <Widget>[
          AppTextField(
            controller: nameController,
            textInputAction: TextInputAction.next,
            isEnabled: fieldsEnabled,
            textCapitalization: TextCapitalization.sentences,
            onFieldSubmitted: (term) => FocusScope.of(context).nextFocus(),
            hint: AppLocalizations.of(context).lessonNameHint,
          ),
          const SizedBox(height: 12),
          AppTextField(
            controller: goalController,
            isEnabled: fieldsEnabled,
            textCapitalization: TextCapitalization.sentences,
            hint: AppLocalizations.of(context).goal,
          ),
          const SizedBox(height: 12),
          Dropdown(
            items: _hours,
            isEnabled: fieldsEnabled,
            defaultValue: _hours.firstWhere((value) => value == durationSelected, orElse: () => null),
            builder: (context, value) => AppLocalizations.of(context).hours(value),
            hint: AppLocalizations.of(context).lessonDurationHint,
            callback: _onDurationSelected,
          ),
          const SizedBox(height: 12),
          AppTextField(
            controller: noteController,
            hint: AppLocalizations.of(context).note,
            isEnabled: fieldsEnabled,
            textCapitalization: TextCapitalization.sentences,
            textInputAction: TextInputAction.done,
            onFieldSubmitted: (term) => FocusScope.of(context).unfocus(),
            isMultiline: true,
          ),
          const SizedBox(height: 12),
          _listAttributeWidget(
            selectedEquipments.isEmpty ? AppLocalizations.of(context).equipment : '',
            () => _showEquipmentsDialog(context),
            selectedEquipments.map((item) => item.name).toList(),
          ),
          const SizedBox(height: 12),
          _listAttributeWidget(
            selectedTags.isEmpty ? 'Tags' : '',
            () => _showTagsDialog(context),
            selectedTags.map((item) => item.name).toList(),
          ),
        ],
      ),
    );
  }

  _saveButton() {
    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: AppButton(
        title: isEditing ? AppLocalizations.of(context).saveChanges : AppLocalizations.of(context).lessonCreate,
        enabled: createEnabled,
        onPressed: _onSavePressed,
      ),
    );
  }

  _progressIndicator() {
    return StreamBuilder<bool>(
        initialData: true,
        stream: lessonBLoC.progressStream,
        builder: (context, snapshot) {
          return snapshot.data ? LinearProgressIndicator() : Container();
        });
  }

  _onFormChanged() {
    var newValue = nameController.text.isNotEmpty && goalController.text.isNotEmpty && durationSelected > 0;
    if (createEnabled != newValue) setState(() => createEnabled = newValue);
  }

  _onDurationSelected(int position) {
    durationSelected = _hours[position];
    _onFormChanged();
  }

  _onSavePressed(BuildContext context) async {
    _disableFields(true);
    final result = (isEditing)
        ? await lessonBLoC.editLesson(
            widget.lesson.id,
            nameController.text,
            goalController.text,
            durationSelected,
            noteController.text,
            selectedEquipments,
            selectedTags,
          )
        : await lessonBLoC.createLesson(
            nameController.text,
            goalController.text,
            durationSelected,
            noteController.text,
            selectedEquipments,
            selectedTags,
          );
    if (result.isSuccess) {
      Navigator.of(context).pop();
    } else {
      _disableFields(false);
    }
  }

  _disableFields(bool disable) {
    setState(() {
      fieldsEnabled = !disable;
      createEnabled = !disable;
    });
  }

  _onClosePressed() {
    Navigator.of(context).pop();
  }

  _listAttributeWidget(String title, VoidCallback onPressed, List<String> names) {
    return InkWell(
      onTap: onPressed,
      child: Stack(
        fit: StackFit.passthrough,
        children: <Widget>[
          Container(
            child: Text(
              title,
              style: hintTextStyle(),
            ),
            padding: const EdgeInsets.all(24),
            alignment: Alignment.centerLeft,
          ),
          _attributes(names),
          Positioned.fill(
            child: Container(
                padding: const EdgeInsets.only(left: 24),
                decoration: BoxDecoration(
                    border: Border.all(
                  color: dividerColor,
                  width: 1,
                ))),
          ),
          Positioned(
            right: 24,
            top: 0,
            bottom: 0,
            child: const Icon(Icons.arrow_drop_down),
          )
        ],
      ),
    );
  }

  _attributes(List<String> names) {
    return Padding(
      padding: const EdgeInsets.all(24.0),
      child: Tags(
        alignment: WrapAlignment.start,
        itemCount: names?.length ?? 0,
        itemBuilder: (int index) {
          return ItemTags(
            pressEnabled: false,
            elevation: 0,
            textActiveColor: Colors.black,
            padding: const EdgeInsets.only(left: 18, right: 18, top: 8, bottom: 8),
            textStyle: TextStyle(
              color: Colors.black,
              fontSize: 14,
              fontFamily: fontRobotoRegular,
            ),
            activeColor: const Color(0xFFDFDFDF),
            color: const Color(0xFFDFDFDF),
            active: true,
            title: names[index],
            index: index,
          );
        },
      ),
    );
  }

  _showEquipmentsDialog(BuildContext context) async {
    final bloc = Provider.of<BLoC>(context).lesson;
    await showDialog(
      context: context,
      builder: (context) {
        return StreamBuilder<List<Equipment>>(
            stream: bloc.equipmentStream,
            initialData: <Equipment>[],
            builder: (context, snapshot) {
              final List<Equipment> items = snapshot.data;
              return SelectListDialog(
                items: items,
                title: 'EQUIPMENT',
                selectedItems: selectedEquipments,
                completion: (list) {
                  setState(() {
                    selectedEquipments = list;
                  });
                },
              );
            });
      },
    );
  }

  _showTagsDialog(BuildContext context) async {
    final bloc = Provider.of<BLoC>(context).lesson;
    await showDialog(
      context: context,
      builder: (context) {
        return StreamBuilder<List<Tag>>(
            stream: bloc.tagsStream,
            initialData: <Tag>[],
            builder: (context, snapshot) {
              final List<Tag> items = snapshot.data;
              return SelectListDialog(
                items: items,
                title: 'TAGS',
                selectedItems: selectedTags,
                completion: (list) {
                  setState(() {
                    selectedTags = list;
                  });
                },
              );
            });
      },
    );
  }
}

final _hours = List.generate(4, (index) => index + 1);
