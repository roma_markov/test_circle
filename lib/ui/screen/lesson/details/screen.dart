import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_tags/tag.dart';
import 'package:intl/intl.dart';
import 'package:lesson_scheduling_pga/l10n/localization.dart';
import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:lesson_scheduling_pga/ui/screen/add_event/schedule_private_lesson/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/create/screen.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/bottom_dialog_item.dart';
import 'package:lesson_scheduling_pga/ui/shared/dialogs.dart';
import 'package:lesson_scheduling_pga/ui/shared/sliver_section_header.dart';

enum LessonDetailsMode {
  none,
  selection,
  schedule,
  scheduled,
}

class LessonDetailsScreen extends StatefulWidget {
  static const String route = '/LessonDetailsScreen';
  final int lessonId;
  final List<Schedule> schedules;
  final LessonDetailsMode mode;

  LessonDetailsScreen._init(this.lessonId, this.mode, this.schedules);

  factory LessonDetailsScreen.fromArgs(Map<String, Object> args) {
    return LessonDetailsScreen._init(
      args["lessonId"],
      args["selectMode"] ?? LessonDetailsMode.none,
      args["schedules"],
    );
  }

  static Future push(
    BuildContext context,
    int lessonId, {
    LessonDetailsMode mode = LessonDetailsMode.none,
    List<Schedule> schedules,
  }) {
    return Navigator.of(context).pushNamed(
      LessonDetailsScreen.route,
      arguments: {
        'lessonId': lessonId,
        'selectMode': mode,
        'schedules': schedules,
      },
    );
  }

  @override
  _LessonDetailsScreenState createState() => _LessonDetailsScreenState();
}

class _LessonDetailsScreenState extends State<LessonDetailsScreen> {
  Lesson lesson;

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<BLoC>(context);
    return StreamBuilder<bool>(
        initialData: false,
        stream: provider.lesson.progressStream,
        builder: (context, snapshot) {
          return IgnorePointer(
            ignoring: snapshot.data,
            child: _content(context, snapshot.data),
          );
        });
  }

  _content(BuildContext context, bool progress) {
    final bloc = Provider.of<BLoC>(context).lesson;
    bloc.lesson(widget.lessonId);
    return StreamBuilder<Lesson>(
        stream: bloc.lessonDetailsStream,
        builder: (context, snapshot) {
          if (snapshot.data == null) return Container();
          lesson = snapshot.data;
          return Scaffold(
            backgroundColor: appWhiteColor,
            appBar: _appBar(),
            body: Stack(
              children: <Widget>[
                Positioned.fill(
                  bottom: (widget.mode == LessonDetailsMode.schedule) ? 60 : 0,
                  child: CustomScrollView(
                    slivers: <Widget>[
                      _baseInfoSliver(lesson),
                      _scheduleSliver(),
                    ],
                  ),
                ),
                widget.mode == LessonDetailsMode.scheduled
                    ? Container()
                    : Positioned(
                        left: 0,
                        right: 0,
                        bottom: 0,
                        child: _selectButton(),
                      ),
                Positioned(
                  left: 0,
                  right: 0,
                  top: 0,
                  height: 5,
                  child: progress ? LinearProgressIndicator() : Container(),
                ),
              ],
            ),
          );
        });
  }

  _baseInfoSliver(Lesson lesson) {
    return SliverList(
      delegate: SliverChildListDelegate.fixed(
        <Widget>[
          SizedBox(height: 40),
          _value(lesson.name, lessonNameTextStyle),
          _label(AppLocalizations.of(context).duration.toUpperCase()),
          _value(AppLocalizations.of(context).hours(lesson.durationHours)),
          _label(AppLocalizations.of(context).goal.toUpperCase()),
          _value(lesson.goal),
          _label(AppLocalizations.of(context).note.toUpperCase()),
          _value(lesson.note ?? ''),
          _label(AppLocalizations.of(context).equipment.toUpperCase()),
          _equipment(lesson),
          _label(AppLocalizations.of(context).tags.toUpperCase()),
          _tags(lesson),
          _editButton(),
          SizedBox(height: 40),
        ],
      ),
    );
  }

  _scheduleSliver() {
    List<dynamic> items = [];
    if (widget.schedules != null) {
      final map = groupBy(widget.schedules, (Schedule schedule) => schedule.start.month);
      final year = DateTime.now().year;
      final formatter = DateFormat.MMMM();
      map.forEach((month, list) {
        items.add(formatter.format(DateTime(year, month)));
        items.addAll(list);
      });
    }
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (context, position) {
          final item = items[position];
          return (item is String)
              ? SectionHeader(
                  text: item.toUpperCase(),
                  textColor: inputHintColor,
                )
              : _scheduleItem(item as Schedule);
        },
        childCount: items.length,
      ),
    );
  }

  _scheduleItem(Schedule schedule) {
    final day = DateFormat.MMMMd();
    final time = DateFormat.jm();
    return Container(
      height: 85,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: inputHintColor, width: 0.5),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _label(day.format(schedule.start).toUpperCase()),
          _value('${time.format(schedule.start)} - ${time.format(schedule.end)}'),
        ],
      ),
    );
  }

  _editButton() {
    return widget.mode == LessonDetailsMode.schedule
        ? Padding(
            padding: const EdgeInsets.only(top: 60, left: 32, right: 32),
            child: AppButtonGrey(
              title: AppLocalizations.of(context).edit.toUpperCase(),
              iconData: Icons.edit,
              color: inputHintColor,
              textColor: appWhiteColor,
              enabled: true,
              onPressed: _onEditPressed,
            ),
          )
        : Container();
  }

  _appBar() {
    return AppBar(
      titleSpacing: 0,
      brightness: Brightness.light,
      actions: widget.mode == LessonDetailsMode.none
          ? <Widget>[
              IconButton(
                icon: Icon(Icons.more_vert),
                onPressed: _onMorePressed,
              )
            ]
          : null,
      iconTheme: IconThemeData(color: inputHintColor),
      backgroundColor: lightAppBarColor,
      title: Text(
        AppLocalizations.of(context).lessonInfo,
        style: appBarLightTextStyle,
      ),
    );
  }

  _selectButton() {
    return AppButton(
      title: _bottomButtonTitle(),
      onPressed: _onSelectPressed,
    );
  }

  _bottomButtonTitle() {
    switch (widget.mode) {
      case LessonDetailsMode.none:
        return AppLocalizations.of(context).lessonSchedulePrivate;
      case LessonDetailsMode.schedule:
        return AppLocalizations.of(context).lessonScheduleThis;
      case LessonDetailsMode.selection:
        return AppLocalizations.of(context).lessonAddPrivate;
      case LessonDetailsMode.scheduled:
        return null;
    }
  }

  _label(String text) {
    return Padding(
      padding: const EdgeInsets.only(top: 24, bottom: 14, left: 32, right: 32),
      child: Text(text, style: lessonLabelTextStyle),
    );
  }

  _value(String text, [TextStyle textStyle = lessonTextStyle]) {
    return Padding(
      padding: const EdgeInsets.only(left: 32, right: 32),
      child: Text(text, style: textStyle),
    );
  }

  _equipment(Lesson lesson) {
    return Padding(
      padding: const EdgeInsets.only(left: 32, right: 32),
      child: Tags(
        alignment: WrapAlignment.start,
        itemCount: lesson.equipment?.length ?? 0,
        itemBuilder: (int index) {
          return ItemTags(
            pressEnabled: false,
            elevation: 0,
            textActiveColor: Colors.black,
            padding: EdgeInsets.only(left: 18, right: 18, top: 8, bottom: 8),
            textStyle: TextStyle(
              color: Colors.black,
              fontSize: 14,
              fontFamily: fontRobotoRegular,
            ),
            activeColor: Color(0xFFDFDFDF),
            color: Color(0xFFDFDFDF),
            active: true,
            title: lesson.equipment[index].name,
            index: index,
          );
        },
      ),
    );
  }

  _tags(Lesson lesson) {
    return Padding(
      padding: const EdgeInsets.only(left: 32, right: 32),
      child: Tags(
        alignment: WrapAlignment.start,
        itemCount: lesson.tags?.length ?? 0,
        itemBuilder: (int index) {
          return ItemTags(
            pressEnabled: false,
            elevation: 0,
            textActiveColor: Colors.black,
            padding: EdgeInsets.only(left: 18, right: 18, top: 8, bottom: 8),
            textStyle: TextStyle(
              color: Colors.black,
              fontSize: 14,
              fontFamily: fontRobotoRegular,
            ),
            activeColor: Color(0xFFDFDFDF),
            color: Color(0xFFDFDFDF),
            active: true,
            title: lesson.tags[index].name,
            index: index,
          );
        },
      ),
    );
  }

  _onMorePressed() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return SizedBox(
              height: (4 * kBottomItemHeight) + kBottomDialogPadding,
              child: Column(children: [
                Padding(padding: EdgeInsets.only(top: kBottomDialogPadding / 2.0)),
                BottomDialogItem(AppLocalizations.of(context).lessonEdit, Icons.edit, () {
                  Navigator.of(context).pop();
                  _onEditPressed(context);
                }),
                BottomDialogItem(AppLocalizations.of(context).lessonDelete, Icons.delete, _onDeletePressed),
                BottomDialogItem(AppLocalizations.of(context).lessonSchedulePrivate, Icons.calendar_today, () {
                  Navigator.of(context).pop();
                  SchedulePrivateLessonScreen.push(context, widget.lessonId);
                }),
                BottomDialogItem(AppLocalizations.of(context).lessonSimilar, Icons.event_note, () {
                  Navigator.of(context).pop();
                  _onCreateNewPressed();
                }),
              ]));
        });
  }

  _onEditPressed(BuildContext context) {
    LessonCreateScreen.push(
      context,
      lesson: lesson,
      mode: LessonCreateMode.edit,
    );
  }

  _onDeletePressed() async {
    Navigator.of(context).pop();
    await showConfirmDialog(
      context,
      'Lesson will be removed from all programs and provate lessons. Are you sure?',
      confirmationCallback: () async {
        final bloc = Provider.of<BLoC>(context);
        final status = await bloc.lesson.deleteLesson(widget.lessonId);
        if (status.isSuccess) {
          await bloc.program.requestPrograms(force: true);
          await bloc.privateLesson.requestPrivateLessons(force: true);
          Navigator.of(context).pop();
        }
      },
    );
  }

  _onCreateNewPressed() {
    LessonCreateScreen.push(context, lesson: lesson);
  }

  _onSelectPressed(BuildContext context) {
    if (widget.mode == LessonDetailsMode.none) {
      SchedulePrivateLessonScreen.push(context, widget.lessonId);
    } else {
      Navigator.of(context).pop(lesson);
    }
  }
}
