import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/l10n/localization.dart';
import 'package:lesson_scheduling_pga/logic/bloc/bloc.dart';
import 'package:lesson_scheduling_pga/logic/bloc/modules/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/tag.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/list/list_item.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/list/tags_widget.dart';
import 'package:lesson_scheduling_pga/ui/shared/block_provider.dart';
import 'package:lesson_scheduling_pga/ui/shared/search_widget.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

class LessonsWidget extends StatefulWidget {
  final LessonPressed onPressed;
  final LessonPressed onLongPressed;
  final Set<Lesson> selectedLessons;

  const LessonsWidget({
    Key key,
    this.onPressed,
    this.onLongPressed,
    this.selectedLessons,
  }) : super(key: key);

  @override
  _LessonsWidgetState createState() => _LessonsWidgetState();
}

class _LessonsWidgetState extends State<LessonsWidget> {
  final searchController = TextEditingController();
  final selectedTags = Set<Tag>();
  LessonBLoC lessonBLoC;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    lessonBLoC = Provider.of<BLoC>(context).lesson;
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      lessonBLoC.requestLessons();
    });
  }

  @override
  dispose() {
    lessonBLoC.changeSearchFilter('');
    lessonBLoC.changeTagsFilter(null);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Lesson>>(
        initialData: <Lesson>[],
        stream: lessonBLoC.lessonsStream,
        builder: (context, snapshot) {
          final list = snapshot.data;
          return Stack(
            children: [
              _listViewWidget(list),
              _progressIndicator(),
            ],
          );
        });
  }

  _listViewWidget(List<Lesson> list) {
    return RefreshIndicator(
      child: ListView.builder(
          itemCount: list.length + 2,
          itemBuilder: (context, position) {
            if (position == 0) return _searchWidget(context);
            if (position == 1)
              return TagsWidget(
                selectedTags: selectedTags,
                onSelectedTagChanged: _onTagsChanged,
              );
            return _listItemWidget(list[position - 2]);
          }),
      onRefresh: () => _refreshLessons(),
    );
  }

  _listItemWidget(Lesson lesson) {
    return LessonListItem(
      lesson: lesson,
      indicatorColor: colorById(lesson.id),
      onPressed: widget.onPressed,
      onLongPressed: widget.onLongPressed,
      isSelected: widget.selectedLessons?.contains(lesson) ?? false,
    );
  }

  _progressIndicator() {
    return StreamBuilder<bool>(
        initialData: true,
        stream: lessonBLoC.progressStream,
        builder: (context, snapshot) {
          return snapshot.data ? LinearProgressIndicator() : Container();
        });
  }

  _searchWidget(BuildContext context) {
    return SearchWidget(
      hint: AppLocalizations.of(context).lessonSearchHint,
      searchController: searchController,
      onChanged: _changeSearch,
    );
  }

  //Actions

  _changeSearch(String value) {
    lessonBLoC.changeSearchFilter(value);
  }

  _refreshLessons() async {
    await lessonBLoC.requestLessons(force: true);
  }

  _onTagsChanged(Tag value) {
    if (selectedTags.contains(value)) {
      selectedTags.remove(value);
    } else {
      selectedTags.add(value);
    }
    lessonBLoC.changeTagsFilter(selectedTags);
  }
}
