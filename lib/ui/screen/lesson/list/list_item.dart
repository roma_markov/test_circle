import 'package:flutter/material.dart';
import 'package:flutter_tags/tag.dart';
import 'package:lesson_scheduling_pga/l10n/localization.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';

typedef LessonPressed = void Function(Lesson item);

class LessonListItem extends StatelessWidget {
  final Color indicatorColor;
  final Lesson lesson;
  final LessonPressed onPressed;
  final LessonPressed onLongPressed;
  final bool isSelected;
  final List<Schedule> schedules;

  const LessonListItem({
    Key key,
    @required this.lesson,
    @required this.onPressed,
    @required this.indicatorColor,
    this.onLongPressed,
    this.isSelected = false,
    this.schedules,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
      color: appWhiteColor,
      height: 96,
      child: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            bottom: 0,
            width: 4,
            child: Container(color: indicatorColor),
          ),
          Positioned(
            left: 24,
            right: 0,
            top: 13,
            child: Text(
              lesson.name,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: lessonItemNameStyle,
            ),
          ),
          Positioned(
            left: 24,
            right: 24,
            top: 38,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                  child: Text(
                    lesson.goal,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: lessonItemGoalStyle,
                  ),
                ),
                Text(
                  '    |    ${AppLocalizations.of(context).hours(lesson.durationHours)}',
                  style: lessonItemGoalStyle,
                )
              ],
            ),
          ),
          Positioned(
            left: 24,
            right: 24,
            top: 61,
            child: _tags(),
          ),
          Positioned.fill(
            child: Material(
              color: isSelected ? indicatorColor.withOpacity(0.2) : Colors.transparent,
              child: InkWell(
                highlightColor: indicatorColor.withOpacity(0.2),
                onTap: () => onPressed(lesson),
                onLongPress: onLongPressed != null ? () => onLongPressed(lesson) : null,
                child: Container(
                  decoration: BoxDecoration(
                    border: schedules == null
                        ? Border(
                            bottom: BorderSide(
                              color: Color(0xFFE5E5E5),
                              width: 1,
                            ),
                          )
                        : Border.all(
                            color: Color(0xFFE5E5E5),
                            width: 1,
                          ),
                  ),
                ),
              ),
            ),
          ),
          (onLongPressed != null)
              ? Positioned(
                  right: 0,
                  top: 0,
                  bottom: 0,
                  width: 60,
                  child: Checkbox(
                    activeColor: inputHintColor,
                    onChanged: (bool value) => isSelected ? onPressed(lesson) : onLongPressed(lesson),
                    value: isSelected,
                  ),
                )
              : Container(),
        ],
      ),
    );
  }

  _tags() {
    return Tags(
      alignment: WrapAlignment.start,
      itemCount: lesson.tags.length ?? 0,
      itemBuilder: (int index) {
        final tag = lesson.tags[index];
        return ItemTags(
          elevation: 0,
          padding: EdgeInsets.only(left: 12, right: 12, top: 4, bottom: 4),
          textStyle: TextStyle(
            fontSize: 12,
            fontFamily: fontRobotoRegular,
          ),
          border: Border.all(color: Colors.transparent),
          color: Color(0xFFDFDFDF),
          textColor: Colors.black,
          active: false,
          title: tag.name,
          index: index,
        );
      },
    );
  }
}
