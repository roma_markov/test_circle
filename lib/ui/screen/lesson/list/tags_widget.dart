import 'package:flutter/material.dart';
import 'package:flutter_tags/tag.dart';
import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/tag.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

class TagsWidget extends StatelessWidget {
  final Set<Tag> selectedTags;
  final Function(Tag) onSelectedTagChanged;

  const TagsWidget({
    Key key,
    @required this.selectedTags,
    @required this.onSelectedTagChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<BLoC>(context).lesson;
    return Container(
      alignment: Alignment.topLeft,
      height: 46,
      child: StreamBuilder<List<Tag>>(
          stream: bloc.tagsStream,
          initialData: [],
          builder: (context, snapshot) {
            final tags = snapshot.data;
            return Tags(
              horizontalScroll: true,
              heightHorizontalScroll: 40,
              alignment: WrapAlignment.start,
              itemCount: tags?.length ?? 0,
              itemBuilder: (int index) {
                final tag = tags[index];
                final isSelected = selectedTags.contains(tag);
                return ItemTags(
                  elevation: 0,
                  padding: EdgeInsets.only(left: 18, right: 18, top: 10, bottom: 10),
                  textStyle: TextStyle(
                    fontSize: 14,
                    fontFamily: fontRobotoRegular,
                  ),
                  color: Color(0xFFFFFFFF),
                  textColor: Colors.black,
                  activeColor: Color(0xFF000000),
                  textActiveColor: Colors.white,
                  border: Border.all(color: isSelected ? Color(0xFF000000) : Color(0xFFDDDDDD), width: 0.5),
                  onPressed: (tag) => onSelectedTagChanged(tags[tag.index]),
                  active: isSelected,
                  title: tag.name,
                  index: index,
                );
              },
            );
          }),
    );
  }
}
