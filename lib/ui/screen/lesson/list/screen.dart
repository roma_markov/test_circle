import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/details/screen.dart';

import 'lessons_widget.dart';

class LessonsScreen extends StatelessWidget {
  static const String route = '/LessonsScrenn';

  LessonsScreen._init();

  factory LessonsScreen.fromArgs(Map<String, Object> args) {
    return LessonsScreen._init();
  }

  static Future push(BuildContext context) {
    return Navigator.of(context).pushNamed(LessonsScreen.route, arguments: {});
  }

  @override
  Widget build(BuildContext context) {
    return LessonsWidget(
      onPressed: (lesson) => _onLessonPressed(context, lesson),
    );
  }

  _onLessonPressed(BuildContext context, Lesson lesson) {
    LessonDetailsScreen.push(context, lesson.id);
  }
}
