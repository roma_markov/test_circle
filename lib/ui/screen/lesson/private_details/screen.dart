import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_tags/tag.dart';
import 'package:intl/intl.dart';
import 'package:lesson_scheduling_pga/l10n/localization.dart';
import 'package:lesson_scheduling_pga/logic/bloc/bloc.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/place.dart';
import 'package:lesson_scheduling_pga/logic/models/private_lesson.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/details/details_added_players_widget.dart';
import 'package:lesson_scheduling_pga/ui/shared/block_provider.dart';
import 'package:lesson_scheduling_pga/ui/shared/bottom_dialog_item.dart';
import 'package:lesson_scheduling_pga/ui/shared/sliver_section_header.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

class ScheduledPrivateLessonDetails extends StatefulWidget {
  static const String route = '/ScheduledPrivateLessonDetails';
  final PrivateLesson event;

  ScheduledPrivateLessonDetails._init(this.event);

  factory ScheduledPrivateLessonDetails.fromArgs(Map<String, Object> args) {
    return ScheduledPrivateLessonDetails._init(args['event']);
  }

  static Future push(BuildContext context, PrivateLesson event) {
    return Navigator.of(context).pushNamed(
      ScheduledPrivateLessonDetails.route,
      arguments: {'event': event},
    );
  }

  @override
  _ScheduledPrivateLessonDetailsState createState() => _ScheduledPrivateLessonDetailsState();
}

class _ScheduledPrivateLessonDetailsState extends State<ScheduledPrivateLessonDetails> {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<BLoC>(context);
    return StreamBuilder<bool>(
        initialData: false,
        stream: provider.privateLesson.progressStream,
        builder: (context, snapshot) {
          return IgnorePointer(
            ignoring: snapshot.data,
            child: _content(context, snapshot.data),
          );
        });
  }

  _content(BuildContext context, bool progress) {
    return Scaffold(
      backgroundColor: appWhiteColor,
      appBar: _appBar(),
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            child: CustomScrollView(
              slivers: <Widget>[
                _baseInfoSliver(widget.event.lesson),
                SliverSectionHeader(
                  text: AppLocalizations.of(context).players(widget.event.players.length).toUpperCase(),
                  backgroundColor: appWhiteColor,
                ),
                DetailsAddedPlayersWidget(
                  addedPlayers: widget.event.players,
                  invitationRecord: widget.event.invitationRecords,
                ),
              ],
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            top: 0,
            height: 5,
            child: progress ? LinearProgressIndicator() : Container(),
          ),
        ],
      ),
    );
  }

  _appBar() {
    return AppBar(
      brightness: Brightness.light,
      titleSpacing: 0,
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.more_vert),
          onPressed: _onMorePressed,
        ),
      ],
      bottom: PreferredSize(
        preferredSize: Size(MediaQuery.of(context).size.width, 50),
        child: Container(
          padding: EdgeInsets.only(left: 32, bottom: 12),
          width: MediaQuery.of(context).size.width,
          height: 50,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    width: 8,
                    height: 8,
                    decoration: BoxDecoration(
                      color: appRedColor,
                      shape: BoxShape.circle,
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: Text(
                        AppLocalizations.of(context).lessonPrivate,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.left,
                        style: lessonItemNameStyle,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      iconTheme: IconThemeData(color: inputHintColor),
      backgroundColor: lightAppBarColor,
      title: Container(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).lessonInfo,
          style: appBarLightTextStyle,
        ),
      ),
    );
  }

  _baseInfoSliver(Lesson lesson) {
    return SliverList(
      delegate: SliverChildListDelegate.fixed(
        <Widget>[
          SizedBox(height: 32),
          _value(lesson.name, lessonNameTextStyle),
          _value(
            '${DateFormat('EEEE, MMMM d').format(widget.event.start)}\n'
            '${DateFormat.jm().format(widget.event.start)} - ${DateFormat.jm().format(widget.event.end)}',
          ),
          _label(AppLocalizations.of(context).hours(lesson.durationHours).toUpperCase()),
          widget.event.place == null ? Container() : _place(widget.event.place),
          SizedBox(height: 8),
          Divider(color: lightGrayColor, height: 1),
          _label(AppLocalizations.of(context).goal.toUpperCase()),
          _value(lesson.goal),
          _label(AppLocalizations.of(context).note.toUpperCase()),
          _value(lesson.note ?? ''),
          _label(AppLocalizations.of(context).equipment.toUpperCase()),
          _equipment(lesson),
          SizedBox(height: 32),
          Divider(color: lightGrayColor, height: 1),
        ],
      ),
    );
  }

  _label(String text) {
    return Padding(
      padding: const EdgeInsets.only(top: 24, bottom: 14, left: 32, right: 32),
      child: Text(text, style: lessonLabelTextStyle),
    );
  }

  _value(String text, [TextStyle textStyle = lessonTextStyle]) {
    return Padding(
      padding: const EdgeInsets.only(left: 32, right: 32),
      child: Text(text, style: textStyle),
    );
  }

  _place(Place place) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 14, left: 30, right: 32),
      child: Row(
        children: <Widget>[
          Icon(
            Icons.location_on,
            size: 12,
            color: inputHintColor,
          ),
          SizedBox(width: 3),
          Expanded(
            child: Text(
              place.name.toUpperCase(),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: lessonLabelTextStyle,
            ),
          )
        ],
      ),
    );
  }

  _equipment(Lesson lesson) {
    return Padding(
      padding: const EdgeInsets.only(left: 32, right: 32),
      child: Tags(
        alignment: WrapAlignment.start,
        itemCount: lesson.equipment?.length ?? 0,
        itemBuilder: (int index) {
          return ItemTags(
            pressEnabled: false,
            elevation: 0,
            textActiveColor: Colors.black,
            padding: EdgeInsets.only(left: 18, right: 18, top: 8, bottom: 8),
            textStyle: TextStyle(
              color: Colors.black,
              fontSize: 14,
              fontFamily: fontRobotoRegular,
            ),
            activeColor: Color(0xFFDFDFDF),
            color: Color(0xFFDFDFDF),
            active: true,
            title: lesson.equipment[index].name,
            index: index,
          );
        },
      ),
    );
  }

  _onMorePressed() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return SizedBox(
              height: kBottomItemHeight + kBottomDialogPadding,
              child: Column(children: [
                Padding(padding: EdgeInsets.only(top: kBottomDialogPadding / 2.0)),
                BottomDialogItem(
                    AppLocalizations.of(context).lessonDelete.toUpperCase(), Icons.delete, _onDeletePressed),
              ]));
        });
  }

  _onDeletePressed() async {
    Navigator.of(context).pop();
    final bloc = Provider.of<BLoC>(context).privateLesson;
    final status = await bloc.deletePrivateLesson(widget.event.id);
    if (status.isSuccess) {
      Navigator.of(context).pop();
    }
  }
}
