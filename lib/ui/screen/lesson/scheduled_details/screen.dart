import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_tags/tag.dart';
import 'package:intl/intl.dart';
import 'package:lesson_scheduling_pga/l10n/localization.dart';
import 'package:lesson_scheduling_pga/logic/models/lesson.dart';
import 'package:lesson_scheduling_pga/logic/models/place.dart';
import 'package:lesson_scheduling_pga/logic/models/program.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/details/details_added_players_widget.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/details/screen.dart';
import 'package:lesson_scheduling_pga/ui/shared/sliver_section_header.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

class ScheduledLessonDetails extends StatefulWidget {
  static const String route = '/ScheduledLessonDetails';
  final Schedule event;

  ScheduledLessonDetails._init(this.event);

  factory ScheduledLessonDetails.fromArgs(Map<String, Object> args) {
    return ScheduledLessonDetails._init(args['event']);
  }

  static Future push(BuildContext context, Schedule event) {
    return Navigator.of(context).pushNamed(
      ScheduledLessonDetails.route,
      arguments: {'event': event},
    );
  }

  @override
  _ScheduledLessonDetailsState createState() => _ScheduledLessonDetailsState();
}

class _ScheduledLessonDetailsState extends State<ScheduledLessonDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appWhiteColor,
      appBar: _appBar(),
      body: CustomScrollView(
        slivers: <Widget>[
          _baseInfoSliver(widget.event.lesson),
          SliverSectionHeader(
            text: AppLocalizations.of(context).players(widget.event.program.players.length).toUpperCase(),
            backgroundColor: appWhiteColor,
          ),
          DetailsAddedPlayersWidget(
            addedPlayers: widget.event.program.players,
            invitationRecord: widget.event.program.invitationRecords,
          ),
        ],
      ),
    );
  }

  _appBar() {
    final schedules = widget.event.program.lessonsSchedule[widget.event.lesson];
    int lessonCount = schedules.length;
    int lessonNumber = schedules.indexOf(widget.event) + 1;
    return AppBar(
      titleSpacing: 0,
      brightness: Brightness.light,
      bottom: PreferredSize(
        preferredSize: Size(MediaQuery.of(context).size.width, 50),
        child: InkWell(
          onTap: _onProgramDetailsPressed,
          child: Container(
            padding: EdgeInsets.only(left: 32, bottom: 12),
            width: MediaQuery.of(context).size.width,
            height: 50,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            width: 8,
                            height: 8,
                            decoration: BoxDecoration(
                              color: colorById(widget.event.program.id),
                              shape: BoxShape.circle,
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 16.0),
                              child: Text(
                                widget.event.program.name,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.left,
                                style: lessonItemNameStyle,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 24.0),
                        child: Text(
                          'SESSION ($lessonNumber/$lessonCount)',
                          textAlign: TextAlign.left,
                          style: appBarLightSubTextStyle,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 60,
                  width: 60,
                  child: Icon(
                    Icons.trending_flat,
                    color: inputHintColor,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      iconTheme: IconThemeData(color: inputHintColor),
      backgroundColor: lightAppBarColor,
      title: Container(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context).lessonInfo,
          style: appBarLightTextStyle,
        ),
      ),
    );
  }

  _baseInfoSliver(Lesson lesson) {
    return SliverList(
      delegate: SliverChildListDelegate.fixed(
        <Widget>[
          SizedBox(height: 32),
          _value(lesson.name, lessonNameTextStyle),
          _value(
            '${DateFormat('EEEE, MMMM d').format(widget.event.date)}\n'
            '${DateFormat.jm().format(widget.event.start)} - ${DateFormat.jm().format(widget.event.end)}',
          ),
          _label(AppLocalizations.of(context).hours(lesson.durationHours).toUpperCase()),
          widget.event.program.place == null ? Container() : _place(widget.event.program.place),
          SizedBox(height: 8),
          Divider(color: lightGrayColor, height: 1),
          _label(AppLocalizations.of(context).goal.toUpperCase()),
          _value(lesson.goal),
          _label(AppLocalizations.of(context).note.toUpperCase()),
          _value(lesson.note ?? ''),
          _label(AppLocalizations.of(context).equipment.toUpperCase()),
          _equipment(lesson),
          SizedBox(height: 32),
          Divider(color: lightGrayColor, height: 1),
        ],
      ),
    );
  }

  _label(String text) {
    return Padding(
      padding: const EdgeInsets.only(top: 24, bottom: 14, left: 32, right: 32),
      child: Text(text, style: lessonLabelTextStyle),
    );
  }

  _place(Place place) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 14, left: 30, right: 32),
      child: Row(
        children: <Widget>[
          Icon(
            Icons.location_on,
            size: 12,
            color: inputHintColor,
          ),
          SizedBox(width: 3),
          Expanded(
            child: Text(
              place.name.toUpperCase(),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: lessonLabelTextStyle,
            ),
          )
        ],
      ),
    );
  }

  _value(String text, [TextStyle textStyle = lessonTextStyle]) {
    return Padding(
      padding: const EdgeInsets.only(left: 32, right: 32),
      child: Text(text, style: textStyle),
    );
  }

  _equipment(Lesson lesson) {
    return Padding(
      padding: const EdgeInsets.only(left: 32, right: 32),
      child: Tags(
        alignment: WrapAlignment.start,
        itemCount: lesson.equipment?.length ?? 0,
        itemBuilder: (int index) {
          return ItemTags(
            pressEnabled: false,
            elevation: 0,
            textActiveColor: Colors.black,
            padding: EdgeInsets.only(left: 18, right: 18, top: 8, bottom: 8),
            textStyle: TextStyle(
              color: Colors.black,
              fontSize: 14,
              fontFamily: fontRobotoRegular,
            ),
            activeColor: Color(0xFFDFDFDF),
            color: Color(0xFFDFDFDF),
            active: true,
            title: lesson.equipment[index].name,
            index: index,
          );
        },
      ),
    );
  }

  _onProgramDetailsPressed() {
    ProgramDetailsScreen.push(context, widget.event.program.id);
  }
}
