import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/bloc/bloc.dart';
import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/ui/screen/home/screen.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_text_field.dart';
import 'package:lesson_scheduling_pga/ui/shared/dialogs.dart';
import 'package:lesson_scheduling_pga/utils/validation_utils.dart';

import '../../index.dart';
import 'create_user.dart';

class LoginScreen extends StatefulWidget {
  static const String route = '/login';

  @override
  State<StatefulWidget> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var loginEnabled = false;
  final emailController = TextEditingController();
  final passController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<BLoC>(context);
    return Stack(
      children: <Widget>[
        Scaffold(
          appBar: AppBar(
            title: Text(
              'LOGIN',
              style: appBarDarkTextStyle,
            ),
          ),
          resizeToAvoidBottomInset: true,
          body: StreamBuilder<bool>(
            stream: provider.auth.progressStream,
            initialData: false,
            builder: (context, snapshot) => _buildForm(context, snapshot.data),
          ),
        ),
      ],
    );
  }

  _onLogin(BuildContext context) async {
    FocusScope.of(context).unfocus();
    if (!formKey.currentState.validate()) return;
    setState(() => loginEnabled = false);
    var provider = Provider.of<BLoC>(context);
    var status = await provider.auth.login(email: emailController.text, password: passController.text);
    if (status.isSuccess) {
      Navigator.of(context).pushReplacementNamed(HomeScreen.route);
    } else if (status.errorMessage != null) {
      setState(() => loginEnabled = true);
      showInfoDialog(context, status.errorMessage);
    } else {
      setState(() => loginEnabled = true);
    }
  }

  _onForgetPass(BuildContext context) async {
    final result = await CreateUserScreen.push(context);
    if (result == true) {
      Navigator.of(context).pushReplacementNamed(HomeScreen.route);
    }
  }

  _onFormChanged() {
    var newValue = emailController.text.isNotEmpty && passController.text.isNotEmpty;
    if (loginEnabled != newValue) setState(() => loginEnabled = newValue);
  }

  _buildForm(BuildContext context, bool showProgress) {
    return Form(
      key: formKey,
      onChanged: _onFormChanged,
      child: Container(
        height: MediaQuery.of(context).size.height,
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Positioned(
              top: 20,
              left: 0,
              right: 0,
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 16),
                child: Stack(
                  children: [
                    AnimatedOpacity(
                      duration: const Duration(milliseconds: 200),
                      opacity: showProgress ? 1.0 : 0.0,
                      child: Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(top: 24),
                        child: CircularProgressIndicator(),
                      ),
                    ),
                    AnimatedOpacity(
                      duration: const Duration(milliseconds: 200),
                      opacity: showProgress ? 0.0 : 1.0,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          AppTextField(
                            controller: emailController,
                            keyboardType: TextInputType.emailAddress,
                            textInputAction: TextInputAction.next,
                            onFieldSubmitted: (term) => FocusScope.of(context).nextFocus(),
                            validator: emailValidatorMethod(emailController),
                            hint: 'Email',
                          ),
                          Padding(padding: const EdgeInsets.only(bottom: 12)),
                          AppTextField(
                            controller: passController,
                            isPassword: true,
                            hint: 'Password',
                          ),
                          InkWell(
                            child: Container(
                              padding: EdgeInsets.all(16),
                              child: Text(
                                'Create Coach'.toUpperCase(),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontFamily: fontRobotoBold,
                                  fontSize: 14,
                                  color: appBlackColor,
                                ),
                              ),
                            ),
                            onTap: () => _onForgetPass(context),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: AppButton(
                title: 'LOGIN',
                enabled: loginEnabled,
                onPressed: _onLogin,
              ),
            )
          ],
        ),
      ),
    );
  }

  FormFieldValidator<String> emailValidatorMethod(TextEditingController emailController) {
    return (value) {
      if (emailController.text.isEmpty) return null;
      if (!ValidationUtils.verifyEmail(emailController.text)) {
        return 'Incorrect email format';
      }
      return null;
    };
  }
}
