import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/bloc/bloc.dart';
import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_button.dart';
import 'package:lesson_scheduling_pga/ui/shared/app_text_field.dart';
import 'package:lesson_scheduling_pga/ui/shared/dialogs.dart';
import 'package:lesson_scheduling_pga/utils/validation_utils.dart';

import '../../index.dart';

class CreateUserScreen extends StatefulWidget {
  static const String route = '/CreateUserScreen';

  CreateUserScreen._init();

  factory CreateUserScreen.fromArgs() {
    return CreateUserScreen._init();
  }

  static Future push(BuildContext context) {
    return Navigator.of(context).pushNamed(CreateUserScreen.route);
  }

  @override
  State<StatefulWidget> createState() => _CreateUserScreenState();
}

class _CreateUserScreenState extends State<CreateUserScreen> {
  var createEnabled = false;
  final emailController = TextEditingController();
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final passController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<BLoC>(context);
    return Stack(
      children: <Widget>[
        Scaffold(
          appBar: AppBar(
            title: Text(
              'COACH REGISTRATION',
              style: appBarDarkTextStyle,
            ),
          ),
          resizeToAvoidBottomInset: true,
          body: StreamBuilder<bool>(
            stream: provider.auth.progressStream,
            initialData: false,
            builder: (context, snapshot) => _buildForm(context, snapshot.data),
          ),
        ),
      ],
    );
  }

  _onCreate(BuildContext context) async {
    FocusScope.of(context).unfocus();
    if (!formKey.currentState.validate()) return;
    setState(() => createEnabled = false);
    var provider = Provider.of<BLoC>(context);
    final status = await provider.auth.createCoach(
      email: emailController.text,
      firstName: firstNameController.text,
      lastName: lastNameController.text,
      password: passController.text,
    );
    if (status.isSuccess) {
      await provider.auth.login(
        email: emailController.text,
        password: passController.text,
      );
      Navigator.of(context).pop(true);
    } else if (status.errorMessage != null) {
      setState(() => createEnabled = true);
      showInfoDialog(context, status.errorMessage);
    } else {
      setState(() => createEnabled = true);
    }
  }

  _onFormChanged() {
    final newValue = emailController.text.isNotEmpty &&
        firstNameController.text.isNotEmpty &&
        lastNameController.text.isNotEmpty &&
        passController.text.isNotEmpty;
    if (createEnabled != newValue) setState(() => createEnabled = newValue);
  }

  _buildForm(BuildContext context, bool showProgress) {
    return Form(
      key: formKey,
      onChanged: _onFormChanged,
      child: Container(
        height: MediaQuery.of(context).size.height,
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Positioned.fill(
              child: SingleChildScrollView(
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 16),
                  padding: EdgeInsets.only(top: 20, bottom: 60),
                  child: Stack(
                    children: [
                      AnimatedOpacity(
                        duration: const Duration(milliseconds: 200),
                        opacity: showProgress ? 1.0 : 0.0,
                        child: Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(top: 24),
                          child: CircularProgressIndicator(),
                        ),
                      ),
                      AnimatedOpacity(
                        duration: const Duration(milliseconds: 200),
                        opacity: showProgress ? 0.0 : 1.0,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            AppTextField(
                              controller: emailController,
                              keyboardType: TextInputType.emailAddress,
                              textInputAction: TextInputAction.next,
                              onFieldSubmitted: (term) => FocusScope.of(context).nextFocus(),
                              validator: emailValidatorMethod(emailController),
                              hint: 'Email',
                            ),
                            Padding(padding: const EdgeInsets.only(bottom: 12)),
                            AppTextField(
                              controller: firstNameController,
                              textInputAction: TextInputAction.next,
                              textCapitalization: TextCapitalization.sentences,
                              onFieldSubmitted: (term) => FocusScope.of(context).nextFocus(),
                              hint: 'First Name',
                            ),
                            Padding(padding: const EdgeInsets.only(bottom: 12)),
                            AppTextField(
                              controller: lastNameController,
                              textInputAction: TextInputAction.next,
                              textCapitalization: TextCapitalization.sentences,
                              onFieldSubmitted: (term) => FocusScope.of(context).nextFocus(),
                              hint: 'Last Name',
                            ),
                            Padding(padding: const EdgeInsets.only(bottom: 12)),
                            AppTextField(
                              controller: passController,
                              textInputAction: TextInputAction.done,
                              isPassword: true,
                              hint: 'Password',
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: AppButton(
                title: 'CREATE',
                enabled: createEnabled,
                onPressed: _onCreate,
              ),
            )
          ],
        ),
      ),
    );
  }

  FormFieldValidator<String> emailValidatorMethod(TextEditingController emailController) {
    return (value) {
      if (emailController.text.isEmpty) return null;
      if (!ValidationUtils.verifyEmail(emailController.text)) {
        return 'Incorrect email format';
      }
      return null;
    };
  }
}
