import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';

typedef ItemPressed = void Function(TabItem item);

class AppDrawerItem extends StatelessWidget {
  final TabItem item;
  final ItemPressed onItemPressed;

  const AppDrawerItem({
    Key key,
    @required this.item,
    @required this.onItemPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      key: null,
      child: Container(
        height: 56,
        padding: const EdgeInsets.only(left: 24),
        child: Row(
          children: [
            Icon(
              item.icon,
              color: drawerItemTextColor,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16),
              child: Text(
                item.title.toUpperCase(),
                style: drawerItemTextStyle,
              ),
            )
          ],
        ),
      ),
      onTap: () => onItemPressed(item),
    );
  }
}

class TabItem {
  final String title;
  final IconData icon;
  final IconData actionIcon;
  final Function(BuildContext context) action;

  TabItem(this.title, this.icon, this.actionIcon, this.action);
}
