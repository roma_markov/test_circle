import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';

class AppDrawerHeader extends StatelessWidget {
  final String userFullName;

  const AppDrawerHeader({Key key, @required this.userFullName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DrawerHeader(
      margin: EdgeInsets.zero,
      padding: const EdgeInsets.all(24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
            height: 52,
            alignment: Alignment.topLeft,
            child: Image.asset(
              'assets/images/pga-logo-white.png',
              height: 22,
              width: 76,
            ),
          ),
          SizedBox(
            height: 52,
            child: Text(
              userFullName,
              style: drawerHeaderTextStyle,
            ),
          ),
        ],
      ),
      decoration: const BoxDecoration(color: appNavigationColor),
    );
  }
}
