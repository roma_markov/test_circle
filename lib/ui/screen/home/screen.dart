import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/logic/bloc/bloc.dart';
import 'package:lesson_scheduling_pga/ui/index.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/create/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/lesson/list/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/create/step_1/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/program/list/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/schedule/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/settings/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/student/create/screen.dart';
import 'package:lesson_scheduling_pga/ui/screen/student/list/screen.dart';
import 'package:lesson_scheduling_pga/ui/shared/calendar/calendar.dart';

import 'drawer_header.dart';
import 'drawer_item.dart';

class HomeScreen extends StatefulWidget {
  static const String route = '/home';

  @override
  State<StatefulWidget> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  JumpTodayController _todayController;
  PageController _controller;
  TabItem _selectedItem;

  @override
  void initState() {
    _controller = PageController();
    _todayController = JumpTodayController();
    _selectedItem = _tabs[0];
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<BLoC>(context);
    return Scaffold(
        appBar: AppBar(
          titleSpacing: 0,
          title: Text(
            _selectedItem.title.toUpperCase(),
            style: appBarDarkTextStyle,
          ),
          actions: [
            IconButton(
              icon: Icon(_selectedItem.actionIcon),
              onPressed: () => _actionPressed(_selectedItem),
            ),
          ],
        ),
        body: PageView(
          controller: _controller,
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            ScheduleScreen.fromArgs(_todayController),
            ProgramsScreen.fromArgs({}),
            LessonsScreen.fromArgs({}),
            StudentsScreen.fromArgs({}),
            SettingsScreen.fromArgs({})
          ],
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              AppDrawerHeader(userFullName: '${bloc.storage.firstName()}\n${bloc.storage.lastName()}'),
              AppDrawerItem(item: _tabs[0], onItemPressed: _itemPressed),
              Divider(color: appNavigationColor.withOpacity(0.4), height: 1),
              AppDrawerItem(item: _tabs[1], onItemPressed: _itemPressed),
              AppDrawerItem(item: _tabs[2], onItemPressed: _itemPressed),
              AppDrawerItem(item: _tabs[3], onItemPressed: _itemPressed),
              Divider(color: appNavigationColor.withOpacity(0.4), height: 1),
              AppDrawerItem(item: _tabs[4], onItemPressed: _itemPressed),
            ],
          ),
        ));
  }

  _itemPressed(TabItem item) {
    Navigator.of(context).pop();
    setState(() => _selectedItem = item);
    _controller.jumpToPage(_tabs.indexOf(item));
  }

  _actionPressed(TabItem item) {
    if (item.action != null) item.action(context);
    if (item.title == 'My schedule') {
      _todayController.jumpToToday();
    }
  }

  static List<TabItem> _tabs = [
    TabItem('My schedule', Icons.calendar_today, Icons.today, null),
    TabItem('Programs', Icons.event_note, Icons.add, (context) => ProgramCreateInfoScreen.push(context)),
    TabItem('Lessons', Icons.school, Icons.add, (context) => LessonCreateScreen.push(context)),
    TabItem('Students', Icons.face, Icons.add, (context) => StudentCreateScreen.push(context)),
    TabItem('Settings', Icons.settings, Icons.input, (context) {
      Provider.of<BLoC>(context).logout(() {
        Navigator.of(context).pushReplacementNamed(LoginScreen.route);
      });
    })
  ];
}
