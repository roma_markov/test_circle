import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:lesson_scheduling_pga/logic/models/place.dart';
import 'package:lesson_scheduling_pga/ui/screen/place/screen.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

typedef PlacePressed = void Function(Place item);

class PlaceListItem extends StatelessWidget {
  final Place place;
  final PlacePressed onPressed;
  final PlacePressed onRemovePressed;

  const PlaceListItem({
    Key key,
    @required this.place,
    @required this.onPressed,
    @required this.onRemovePressed,
  }) : super(key: key);

  _contentWidget(BuildContext context) {
    return Material(
      color: appWhiteColor,
      child: InkWell(
        onTap: () => onPressed(place),
        child: Container(
          padding: const EdgeInsets.only(left: 22, right: 22),
          height: kPlaceItemHeight,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Text(
                  place.name,
                  overflow: TextOverflow.ellipsis,
                  style: appBarLightTextStyle,
                  maxLines: 1,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Slidable(
        child: _contentWidget(context),
        actionExtentRatio: 0.25,
        actionPane: SlidableStrechActionPane(),
        closeOnScroll: true,
        secondaryActions: <Widget>[
          Container(
            color: appRedColor,
            child: IconSlideAction(
              caption: 'Delete',
              color: Colors.transparent,
              icon: Icons.delete,
              onTap: () => onRemovePressed(place),
            ),
          ),
        ],
      ),
    );
  }
}
