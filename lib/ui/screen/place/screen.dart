import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/logic/models/place.dart';
import 'package:lesson_scheduling_pga/logic/repositories/base.dart';
import 'package:lesson_scheduling_pga/ui/shared/add_widget.dart';
import 'package:lesson_scheduling_pga/ui/shared/block_provider.dart';
import 'package:lesson_scheduling_pga/ui/shared/style.dart';

import 'list_item.dart';

double kPlaceItemHeight = 56;

class PlacesScreen extends StatefulWidget {
  static const String route = '/PlacesScreen';

  PlacesScreen._init();

  factory PlacesScreen.fromArgs({Map<String, Object> args}) {
    return PlacesScreen._init();
  }

  static Future push(BuildContext context) {
    return Navigator.of(context).pushNamed(
      PlacesScreen.route,
      arguments: Map<String, dynamic>(),
    );
  }

  @override
  _PlacesScreenState createState() => _PlacesScreenState();
}

class _PlacesScreenState extends State<PlacesScreen> {
  final searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final bloc = Provider.of<BLoC>(context).place;
      bloc.requestPlaces();
    });
  }

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<BLoC>(context).place;
    return Scaffold(
      backgroundColor: appWhiteColor,
      appBar: _appBar(),
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Column(
              children: <Widget>[
                _searchWidget(context),
                Expanded(
                  child: StreamBuilder<List<Place>>(
                      stream: bloc.placesStream,
                      initialData: [],
                      builder: (context, snapshot) {
                        final places = snapshot.data;
                        return ListView.builder(
                          padding: EdgeInsets.zero,
                          itemCount: places.length,
                          itemBuilder: (context, position) => PlaceListItem(
                            place: places[position],
                            onPressed: _onPlacePressed,
                            onRemovePressed: _onPlaceDeletePressed,
                          ),
                        );
                      }),
                ),
              ],
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            top: 0,
            height: 5,
            child: _progressIndicator(),
          ),
        ],
      ),
    );
  }

  _progressIndicator() {
    final bloc = Provider.of<BLoC>(context).place;
    return StreamBuilder<bool>(
        initialData: true,
        stream: bloc.progressStream,
        builder: (context, snapshot) {
          return snapshot.data ? LinearProgressIndicator() : Container();
        });
  }

  _appBar() {
    return AppBar(
      titleSpacing: 0,
      brightness: Brightness.light,
      leading: IconButton(
        icon: Icon(Icons.close),
        onPressed: _onClosePressed,
      ),
      iconTheme: IconThemeData(color: inputHintColor),
      backgroundColor: lightAppBarColor,
      title: Container(
        alignment: Alignment.centerLeft,
        child: Text(
          'WHERE TO MEET',
          style: appBarLightTextStyle,
        ),
      ),
    );
  }

  _searchWidget(BuildContext context) {
    final bloc = Provider.of<BLoC>(context).place;
    return StreamBuilder<bool>(
        initialData: false,
        stream: bloc.progressStream,
        builder: (context, snapshot) {
          return AddWidget(
            hint: 'Add a place',
            searchController: searchController,
            enabled: !snapshot.data,
            onChanged: _changeSearch,
            onCreatePressed: _onAddPlacePressed,
          );
        });

  }

  _onPlaceDeletePressed(Place place) async {
    final bloc = Provider.of<BLoC>(context).place;
    final result = await bloc.deletePlace(place.id);
    if (result.isSuccess) {}
  }

  @override
  void dispose() {
    super.dispose();
  }

  _onClosePressed() {
    _changeSearch('');
    Navigator.pop(context);
  }

  _onPlacePressed(Place item) {
    _changeSearch('');
    Navigator.of(context).pop(item);
  }

  _changeSearch(String value) {
    final bloc = Provider.of<BLoC>(context).place;
    bloc.changeSearchFilter(value);
  }

  _onAddPlacePressed() async {
    if (searchController.text.isEmpty) return;
    final bloc = Provider.of<BLoC>(context).place;
    final samePlace = await bloc.placeByMessage(searchController.text);
    if (samePlace != null) {
      _changeSearch('');
      Navigator.of(context).pop(samePlace);
      return;
    }

    final result = await bloc.createPlace(searchController.text);
    if (result is Status) {
      _changeSearch('');
      Navigator.of(context).pop(result.data);
    }
  }

}
