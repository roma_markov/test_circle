import 'package:flutter/material.dart';
import 'package:lesson_scheduling_pga/logic/index.dart';
import 'package:lesson_scheduling_pga/ui/screen/home/screen.dart';

import '../../index.dart';

class SplashScreen extends StatelessWidget {

  static const String route = '/';

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(seconds: 1), () => _pushFirstScreen(context));
    return Container(
      color: appBackgroundColor,
    );
  }

  _pushFirstScreen(BuildContext context) {
    final provider = Provider.of<BLoC>(context);
    provider.storage.storageLoaded.listen((_) {
      var token = provider.storage.token();
      Navigator.of(context).pushReplacementNamed(
        token != null ? HomeScreen.route : LoginScreen.route,
      );
    });
  }
}
