class ValidationUtils {
  static bool verifyEmail(String email) {
    if (email == null || email.isEmpty) return false;
    return RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email);
  }
}
